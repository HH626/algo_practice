import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 18352 특정거리의 도시찾기
 */
public class Main_특정거리의도시찾기 {

	static int N, M, K, X; 
	static List[] map;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk; 
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());	//도시갯수
		M = Integer.parseInt(tk.nextToken());	//도로갯수
		K = Integer.parseInt(tk.nextToken());	//최단거리
		X = Integer.parseInt(tk.nextToken());	//출발점
		
		map = new List[N+1];
		
		int from, to;
		for(int i=0; i<M; i++) {
			tk = new StringTokenizer(br.readLine());
			from = Integer.parseInt(tk.nextToken());
			to = Integer.parseInt(tk.nextToken());
			if(map[from]==null) {
				map[from] = new LinkedList<Integer>();
			}
			map[from].add(to);
			
		}
		
		///
		List<Integer> list = bfs(X);
		
		Collections.sort(list);
		for(int l: list) {
			System.out.println(l);
		}

	}
	
	public static List<Integer> bfs(int start) {
		Queue<int[]> que = new LinkedList<int[]>();
		boolean visited[] = new boolean[N+1];
		int arr[], from, to, cnt;
		List<Integer> answer = new LinkedList<Integer>();
		
		visited[start] = true;
		que.offer(new int[] {start,0});
		
		while(!que.isEmpty()) {
			arr = que.poll();
			from = arr[0];
			cnt = arr[1];
			
			//거리가 K인 점 answer에 추가
			if(cnt == K) {
				answer.add(from);
			}else if(cnt>K) {
				break;
			}
			
			if(map[from]==null)continue;
			//from과 연결되어있는 점 큐에 추가
			for(int i=0, size=map[from].size(); i<size; i++) {
				to = (int) map[from].get(i);
				if(!visited[to]) {
					visited[to] = true;
					que.offer(new int[] {to,cnt+1});
				}
			}
		}//while
		
		if(answer.size()<1) {
			answer.add(-1);
		}
		return answer;
	}


}
