import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 백준 2668 숫자고르기
 * 완탐으로 풂
 * 분류:dfs
 */
public class Main_숫자고르기 {

	static int N, arr[], answer[];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		arr = new int[N+1];
		answer = new int[N+1];
		int cnt=0;
		for(int i=1; i<=N; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		for(int i=1; i<=N; i++) {
			if(answer[i]>0) continue;
			cnt += makeCircle(i);
		}
		
		System.out.println(cnt);
		for(int i=1; i<=N; i++) {
			if(answer[i]>0) {
				System.out.println(i);
			}
				
		}
		
	}
	
	private static int makeCircle(int root) {
		List<Integer> tempArr = new LinkedList<>();
		int[] temp = new int[N+1];
		int cur = root;
		temp[root] = root;
		tempArr.add(root);
		while(true) {
			cur = arr[cur];
			if(temp[cur]>0) { //루프가 생기면
				if(cur == root) { //돌아서 루트랑 만나면

					int size = tempArr.size();
					for(int i=0; i<size; i++) {
						answer[tempArr.get(i)] = root;
					}
					return size;
				}else {
					return 0;
				}
			}else{
				temp[cur]=root;
				tempArr.add(cur);
			}
			
		}
	}//makeCircle


}
