import java.util.PriorityQueue;
import java.util.Queue;

/**
 * 프로그래머스 힙 더맵게
 */
public class Programmers_더맵게 {

	public static void main(String[] args) {
		System.out.println(solution(new int[]{1,2,3,9,10,12},7)); 	//7
		System.out.println(solution(new int[]{10},7));				//0
		System.out.println(solution(new int[]{2},7));				//-1
		System.out.println(solution(new int[]{1,2},7));				//-1
		System.out.println(solution(new int[]{4,2},7));				//1
	}
	
    public static int solution(int[] scoville, int K) {
    	Queue<Integer> pq = new PriorityQueue<Integer>();
    	
    	for(int i=0, len = scoville.length; i<len; i++) {
    		pq.offer(scoville[i]);
    	}
    	
    	int n,m, cnt=0;
    	while(!pq.isEmpty()) {
    		if(pq.size()==1) {
    			n = pq.poll();//가장 안맵
    			if(n<K){
    				cnt=-1;
    			}
    			break;
    		}
    		n = pq.poll();//가장 안맵
    		if(n>=K) break;
    		m = pq.poll();//두번째로 안맵
    		pq.offer(n+m*2);
    		cnt++;
    		
    		
    	}
        int answer = cnt;
        return answer;
    }

}
