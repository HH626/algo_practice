import java.util.Scanner;
/**
 * 백준 9461 파도반 수열
 * 재귀
 * 메모이제이션
 */
public class Main_파도반수열 {

	static long memo[];
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int tc=0; tc<T; tc++) {
			long answer;
			int n = sc.nextInt();
			memo = new long[n+1];

			answer = P(n);
			System.out.println(answer);
		}

	}
	
	static long P(int n) {
		if(n>5) {
			if(memo[n-1]!=0 && memo[n-5]!=0) {
				return memo[n-1]+memo[n-5];
			}else {
				return memo[n]=P(n-1)+P(n-5);
			}
		}else if(n<=5 && n>3) {
			memo[n]=2;
			return 2;
		}else if(n>0){
			memo[n] = 1;
			return 1;
		}else {
			return 0;
		}
	}

}
