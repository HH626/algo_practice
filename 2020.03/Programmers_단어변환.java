import java.util.LinkedList;
import java.util.Queue;

/**
 * 프로그래머스 DFS/BFS 단어변환
 * DFS
 */
public class Programmers_단어변환 {

	public static void main(String[] args) {
		System.out.println(solution("hit","cog",new String[]{"hot", "dot", "dog", "lot", "log", "cog"}));
	}
	
	static int answer;
	public static int solution(String begin, String target, String[] words) {
		boolean visited[] = new boolean[words.length];
		int Length = begin.length();
		answer = Integer.MAX_VALUE;
		dfs(begin, target, words, visited, Length, 0);	
		if(answer==Integer.MAX_VALUE) {
			answer = 0;
		}
		return answer;
	}
	
	private static void dfs(String begin, String target, String[] words, boolean[] visited, int Length, int cnt) {
		if(begin.equals(target)) {
			if(cnt<answer) {
				answer = cnt;
			}
			return;
		}
		
		String curWord, compareWord;
		for(int i=0; i<Length; i++) {
			//단어에서 알파벳 하나씩 빼보기
			curWord = begin.substring(0, i)+ "."+begin.substring(i+1, Length);
			//하나뺀 알파벳이 같은거 찾기
			for(int n=0; n<words.length; n++) {
				if(visited[n]) continue; // 지나온 단어면 스킵
				compareWord = words[n].substring(0, i)+ "."+words[n].substring(i+1, Length);
				if(compareWord.equals(curWord)) {
					visited[n] = true;
					dfs(words[n], target, words, visited, Length, cnt+1);
					visited[n] = false;
				}
			}//단어비교
		}
		
	}//dfs

}
