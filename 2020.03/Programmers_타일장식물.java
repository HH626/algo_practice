import java.util.PriorityQueue;
import java.util.Queue;

/**
 * 프로그래머스 DP 타일 장식물
 */
public class Programmers_타일장식물 {

	public static void main(String[] args) {
		System.out.println(solution(6));
	}
	
	public static long solution(int N) {
        long answer = 0;
        int sum;
        long[] arr = new long[N+2];
        arr[1] = 1;
        arr[2] = 1;
        for(int i=3; i<=N; i++) {
        	arr[i] = arr[i-1] + arr[i-2];
        }
        
        return 4*arr[N]+2*arr[N-1];
    }

}
