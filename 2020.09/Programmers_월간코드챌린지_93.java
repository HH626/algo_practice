import java.util.*;

public class Programmers_월간코드챌린지_93 {

	public static void main(String[] args) {
		Solution s = new Solution();

	}

}


class Solution {
    public int solution(int[] a) {
        int N = a.length;
        
         //가장 작은값의 인덱스 찾기
        int minIdx = 0;
        for(int i=1; i<N; i++){
            if(a[i]<a[minIdx]){
                minIdx = i;
            }
        }
        
        int cnt=1;
        //가장 작은 값보다 왼쪽
        int left = a[0];
        if(minIdx != 0) cnt++;
        for(int i=1; i<minIdx; i++){
            if(a[i]<left){
                cnt++;
                left = a[i];
            }
        }
        
        //가장 작은 값보다 오른쪽
        int right = a[N-1];
        if(minIdx != N-1) cnt++;
        for(int i=N-2; i>minIdx; i--){
            if(a[i]<right){
                cnt++;
                right = a[i];
            }
        }
        
        int answer = cnt;
        return answer;
    }
}