import java.util.*;

public class Programmers_월간코드챌린지_92 {

	public static void main(String[] args) {
		Solution s = new Solution();

	}

}

class Solution {
    public int[] solution(int n) {
        int[][] tri = new int[n][];
        for(int i=0; i<n; i++){
            tri[i] = new int[i+1];
        }
        
        int cnt =1;
        int r=-1, c=0;
        int N = (n+1)*n/2;
        System.out.println(N);
        
        while(cnt<=N){
            //아래
            while(true){
                r++;
                if(r>=n || tri[r][c] != 0) break;
                tri[r][c] = cnt;
                cnt++;
            }
            r--;
            //오른쪽
            while(true){
                c++;
                if(c>=n ||tri[r][c] != 0 ) break;
                tri[r][c] = cnt;           
                cnt++;
            }
            c--;
            //위
            while(true){
                r--;
                c--;
                if(r<0 || tri[r][c] != 0) break;
                tri[r][c] = cnt;
                cnt++;
            }
            r++;
            c++;
        }
    
        int[] answer = new int[N];
        int idx = 0;
        for(int i=0; i<n; i++){
            for(int j=0; j<i+1; j++){
                answer[idx] = tri[i][j];
                idx++;
            }
        }
            
    
        return answer;
    }
}