import java.util.*;

public class Programmers_월간코드챌린지_91 {

	public static void main(String[] args) {
		Solution s = new Solution();

	}

}

class Solution {
    public int[] solution(int[] numbers) {
        int N = numbers.length;
        HashSet<Integer> hs = new HashSet<>();
        for(int i=0; i<N-1; i++){
            int a = numbers[i];
            for(int j=i+1; j<N; j++){
                int b = numbers[j];
                System.out.println(a+b);
                hs.add(a+b);
            }
        }
        
        int M = hs.size();
        int[] answer = new int[M];
        
        Iterator itr = hs.iterator();
        int cnt=0;
        while(itr.hasNext()){
            answer[cnt] = (int)itr.next();
            cnt++;
        }
        
        Arrays.sort(answer);
            
        return answer;
    }
}
