import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;
/**
 * 백준 19235 모노미노도미노
 */
public class Main_모노미노도미노{

	static int[][] map;
	static int R = 9;
	static int C = 9;
	static int result;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		map = new int[10][10];
		result = 0;
		
		int N = Integer.parseInt(br.readLine());
		for(int i=0; i<N; i++) {
			tk = new StringTokenizer(br.readLine());
			int t = Integer.parseInt(tk.nextToken());
			int r = Integer.parseInt(tk.nextToken());
			int c = Integer.parseInt(tk.nextToken());
			
			green(t, r, c);
			blue(t, r, c);
			
			greenRemove();
			blueRemove();
			
			greenWhite();
			blueWhite();

//			print();
			
		}//for
		
		int cnt = count();
		
		System.out.println(result);
		System.out.println(cnt);
		
	}//main
	
	public static void green(int t, int r, int c) {
		int nr = r;
		int nc = c;
		switch(t) {
		case 1:
			while(nr<=R-1 && map[nr+1][nc] == 0) {
				nr=nr+1;
			}
			map[nr][nc] = 1;
			break;
		case 2:
			while(nr<=R-1 && map[nr+1][nc] == 0 && map[nr+1][nc+1] == 0) {
				nr=nr+1;
			}
			map[nr][nc] = 2;
			map[nr][nc+1] = 2;
			break;
		case 3:
			while(nr<=R-2 && map[nr+2][nc] == 0) {
				nr=nr+1;
			}
			map[nr][nc] = 3;
			map[nr+1][nc] = 3;
			break;
		}
	}//green
	
	public static void blue(int t, int r, int c) {
		int nr = r;
		int nc = c;
		switch(t) {
		case 1:
			while(nc<=C-1 && map[nr][nc+1] == 0) {
				nc = nc+1;
			}
			map[nr][nc] = 1;
			break;
		case 2:
			while(nc<=C-2 && map[nr][nc+2] == 0) {
				nc = nc+1;
			}
			map[nr][nc] = 2;
			map[nr][nc+1] = 2;
			break;
		case 3:
			while(nc<=C-1 && map[nr][nc+1] == 0 && map[nr+1][nc+1] == 0) {
				nc = nc+1;
			}
			map[nr][nc] = 3;
			map[nr+1][nc] = 3;
			break;
		}
	}//blue
	
	public static void greenRemove() {
		
		while(true) {
			boolean endflag = true;	//더이상 떨어지고 제거될 불럭이 없으면 true; 
			//green 아래 블럭부터 확인
			for(int i=9; i>3; i--) {
				boolean lineflag = true; // 제거될 라인이 생기면 true;
				for(int j=0; j<4; j++) {
					if(map[i][j] == 0) {
						lineflag = false;
						break;
					}
				}
				//i번째 라인 제거하기
				if(lineflag) {
					for(int j=0; j<4; j++) {
						map[i][j] = 0;
					}
					result++ ;
					endflag = false;
				}
			}
			
			if(endflag) break;
			
			greenDrop();
		}
		
	}
	
	public static void blueRemove() {
		
		while(true) {
			boolean endflag = true;	
			
			for(int j=9; j>3; j--) {
				boolean lineflag = true; 
				for(int i=0; i<4; i++) {
					if(map[i][j] == 0) {
						lineflag = false;
						break;
					}
				}

				if(lineflag) {
					for(int i=0; i<4; i++) {
						map[i][j] = 0;
					}
					result++;
					endflag = false;
				}
			}
			
			if(endflag) break;
			
			blueDrop();
		}
		
	}

	public static void greenDrop() {
		//빈공간으로 블럭 떨어뜨리기
		for(int i=R-1; i>3; i--) {
			for(int j=0; j<4; j++) {
				int t = map[i][j];
				if(t == 1 || t == 3) {
					int nr = i+1; 
					while(nr<=R && map[nr][j]==0) {
						nr += 1;
					}
					map[i][j] = 0;
					map[nr-1][j] = t;
				}else if(t == 2) {
					int nr = i+1; 
					while(nr<=R && map[nr][j]==0 && map[nr][j+1]==0) {
						nr += 1;
					}
					map[i][j] = 0;
					map[i][j+1] = 0;
					map[nr-1][j] = t;
					map[nr-1][j+1] = t;
					
					j++;
				}
			}
		}
	}//greenDrop	
	
	public static void blueDrop() {

		for(int j=C-1; j>3; j--) {
			for(int i=0; i<4; i++) {
				int t = map[i][j];
				if(t == 1 || t == 2) {
					int nc = j+1; 
					while(nc<=C && map[i][nc]==0) {
						nc += 1;
					}
					map[i][j] = 0;
					map[i][nc-1] = t;
				}else if(t == 3) {
					int nc = j+1; 
					while(nc<=C && map[i][nc]==0 && map[i+1][nc]==0) {
						nc += 1;
					}
					map[i][j] = 0;
					map[i+1][j] = 0;
					map[i][nc-1] = t;
					map[i+1][nc-1] = t;
					
					i++;
				}
			}
		}
	}//blueDrop
	
	public static void greenWhite(){
		int cnt = 0;
		for(int i=4; i<=5; i++) {
			for(int j=0; j<4; j++) {
				if(map[i][j] != 0) {
					cnt++;
					break;
				}
			}
		}
		
		while(cnt>0) {
			cnt--;
			for(int j=0; j<4; j++) {
				map[R-cnt][j] = 0;
			}
		}
		
		greenDrop();
	}//greenWhite
	
	public static void blueWhite(){
		int cnt = 0;
		for(int i=4; i<=5; i++) {
			for(int j=0; j<4; j++) {
				if(map[j][i] != 0) {
					cnt++;
					break;
				}
			}
		}
		
		while(cnt>0) {
			cnt--;
			for(int j=0; j<4; j++) {
				map[j][C-cnt] = 0;
			}
		}
		
		blueDrop();
	}//blueWhite
	
	public static int count() {
		int cnt = 0;
		for(int i=0; i<=R; i++) {
			for(int j=0; j<=C; j++) {
				if(map[i][j] != 0) cnt++;
			}
		}
		return cnt;
	}
	
	public static void print() {
		for (int[] a : map) {
			System.out.println(Arrays.toString(a));
		}
		System.out.println("-------------------------");
	}


}
