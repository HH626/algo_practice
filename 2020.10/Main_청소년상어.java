import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;



/**
 * 백준 19236 청소년 상어
 */
public class Main_청소년상어 {

	static int MAX;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		int[][] map = new int[4][4];
		Fish[] arr = new Fish[17];
		MAX = 0;
		
		for(int i=0; i<4; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=0; j<4; j++) {
				int num = Integer.parseInt(tk.nextToken());
				int dir = Integer.parseInt(tk.nextToken());
				arr[num] = new Fish(num,i,j,dir);
				map[i][j] = num;
			}
		}//in
		
		//0번 물고기는 상어
		int sum = map[0][0];
		int r = arr[sum].r;
		int c = arr[sum].c;
		int dir = arr[sum].dir;
		arr[0] = new Fish(0,r,c,dir);
		map[0][0] = 0;
		
		

		
		
		//out
		System.out.println(MAX);
		
	}//main
	
	public static int[] dR = {-1,-1,-1,0,1,1,1,0,-1};
	public static int[] dC = {1,0,-1,-1,-1,0,1,1,1};
	public static void go(int[][] preMap, Fish[] preArr, int idx, int sum) {
		//1 map복사
		int[][] map = copy(preMap);
		Fish[] arr = preArr.clone();
		
		//2상어가 먹음
		Fish shark = arr[0];
		map[shark.r][shark.c] = -1;
		
		Fish fish = arr[idx];
		map[fish.r][fish.c] = 0;

		int r, c, dir;
		shark.r = r = fish.r;
		shark.c = c = fish.c;
		shark.dir = dir = fish.dir;
		arr[idx] = null;
		
		
		//2 물고기 이동
		fishMove(map, arr);
		
		
		//3 상어 이동
		boolean flag = false;
		for(int i=1; i<4; i++) {
			int nr = r + dR[dir]*i;
			int nc = c + dC[dir]*i;
			
			if(nr<0 || nc<0|| nr>3 || nc>3) break;	//범위넘음
			if(map[nr][nc] < 0) continue;			//물고기 없음
			
			go(map, arr, map[nr][nc], sum+map[nr][nc]);
			flag = true;
			
		}
		
		//4 만약 상어이동 못하면 물고기 번호합 체크
		if(!flag) {
			MAX = Math.max(MAX, sum);
			System.out.println(sum);
		}
	}
	
	public static int[][] copy(int[][] map) {
		int[][] copiedMap = new int[4][4];
		
		for(int i=0; i<4; i++) {
			for(int j=0; j<4; j++) {
				copiedMap[i][j] = map[i][j];
			}
		}
		
		return copiedMap;
	}
	public static void fishMove(int[][] map, Fish[] arr) {
		for(int idx=1; idx<=16; idx++) {
			if(arr[idx]==null) continue;
			
			int r = arr[idx].r;
			int c = arr[idx].c;
			int dir = arr[idx].dir;
			//회전하기
			for(int i=0; i<8; i++) {
				int nd = (dir+i)%8;
				int nr = r+dR[nd];
				int nc = c+dC[nd];
				
				if(nr<0 || nc<0|| nr>3 || nc>3) continue;	//범위넘음
				
				if(map[nr][nc] == 0) continue;			//상어
				
				//map swap
				int tmp = map[r][c];
				map[r][c] = map[nr][nc];
				map[nr][nc] = tmp;
				
				//fishArr swap
				int idx2 = map[nr][nc];
				if(idx2<0) {
					arr[idx] = new Fish(idx, nr, nc, nd);
				}else {
					arr[idx].r = arr[idx2].r;
					arr[idx].c = arr[idx2].c;
					arr[idx].dir = nd;
					arr[idx2] = new Fish(idx2, r,c, arr[idx2].dir);
				}
				break;
			}//회전
		}
	}//fishmove
	
	static class Fish{
		int num;
		int r,c;
		int dir;
		Fish (int num, int r, int c, int dir){
			this.num = num;
			this.r = r;
			this.c = c;
			this.dir = dir;
		}
		@Override
		public String toString() {
			return "Fish [num=" + num + ", dir=" + dir + "]";
		}
		
	}//Fish


}
