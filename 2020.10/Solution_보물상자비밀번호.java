import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;
/**
 * SWEA 5658 [모의]보물상자 비밀번호
 */
public class Solution_보물상자비밀번호 {
	static int N, K, n;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc<= T; tc++) {
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());
			K = Integer.parseInt(tk.nextToken());
			String str = br.readLine();
			
			n = N/4;
			HashSet<String> hs = new HashSet<String>();
			
			for(int i=0; i<n; i++) {

				for(int j=0; j<N;) {
					//1. 단어나누기
					String num = str.substring(j, j+n);
					//2. set에 넣기
					hs.add(num);
					
					j = j+n;
				}
				
				
				//3. rotate
				str = str.substring(N-1,N) + str.substring(0, N-1);

			}//for
			
			ArrayList<String> list = new ArrayList<>();
			
			Iterator<String> iter = hs.iterator();
			while(iter.hasNext()) {
				list.add((String)iter.next());
			}
			
			//4. 정렬하기
			Collections.sort(list);
			int size = list.size();
			
			//5. K번째 수 10진수로 바꾸기
			int answer = decimal(list.get(size-K));
			
			System.out.println("#"+tc+" "+answer);
		}//tc
	}
	
	public static int decimal(String num) {
		int result=0;
		
		int size = num.length();
		for(int i=0; i<size; i++) {
			int cur = toInteger(num.charAt(i));
			result += Math.pow(16, size-i-1) * cur;
		}
		
		return result;
	}
	
	public static int toInteger(char ch) {
		switch(ch) {
		case 'A':
			return 10;
		case 'B':
			return 11;
		case 'C':
			return 12;
		case 'D':
			return 13;
		case 'E':
			return 14;
		case 'F':
			return 15;
		default :
			return ch-'0';	
		}
		
	}

}
