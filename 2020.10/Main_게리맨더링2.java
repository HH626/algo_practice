import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;


/**
 * 백준 17779 게리맨더링2
 */
public class Main_게리맨더링2 {

	static int N, map[][], total, min;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		N = Integer.parseInt(br.readLine());
		map = new int[N+1][N+1];
		min = 999999999;
		total = 0;
		
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				total += map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}//in
		
		//1. 선거구 나누기
		//1-1 r,c정하기 (x=r, y=c);
		for(int r = 1; r <=N; r++) {
			for(int c = 1; c<=N; c++) {
				border(r, c);
			}
		}
		
		
		//out
		System.out.println(min);
	}
	
	public static void border(int r, int c) {
		//1-2 d1,d2가능한 범위로
		for(int d = 1; d< N; d++) {
			for(int e = 1; e<N; e++) {
				if(r+d+e<=N && 1<=c-d && c+e<=N) {
					int result = count(r,c,d,e);
//					System.out.println(result);
					//4. 3의 최솟값
					min = Math.min(result, min);
				}
			}
		}
	}
	
	//2. 인구수 구하기
	public static int count(int r, int c, int d, int e) {
//		System.out.println(r+" "+c+" "+d+" "+e);
		
		int[] arr = new int[6];
		//1번선거구
		for(int i=1; i<r+d; i++) {
			for(int j=1; j<=c; j++) {
				arr[1] += map[i][j];
			}
		}
		for(int i=0; i<d; i++) {
			for(int j=c-i; j<=c; j++) {
//				System.out.println(map[r+i][j]);
				arr[1] -= map[r+i][j];
			}
		}
		
		//2
		for(int i=1; i<=r+e; i++) {
			for(int j=c+1; j<=N; j++) {
				arr[2] += map[i][j];
			}
		}
		for(int i=1; i<=e; i++) {
			for(int j=c+1; j<=c+i; j++) {
				arr[2] -= map[r+i][j];
			}
		}
		
		//3
		for(int i = r+d; i<=N; i++) {
			for(int j=1; j<c-d+e; j++) {
				arr[3] += map[i][j];
			}
		}
		for(int i=0; i<e; i++) {
			for(int j=c-d+i; j<c-d+e; j++) {
				arr[3] -= map[r+d+i][j];
			}
		}
		
		//
		for(int i=r+e+1; i<=N; i++) {
			for(int j=c-d+e; j<=N; j++) {
				arr[4] += map[i][j];
			}
		}
		for(int i=1; i<=d; i++) {
			for(int j=c-d+e; j<=c+e-i; j++) {
				arr[4] -= map[r+e+i][j];
			}
		}
		//5번
		arr[5] = total - arr[1]- arr[2]- arr[3]- arr[4];
		
//		System.out.println(Arrays.toString(arr));
		Arrays.sort(arr);
		
		//3. 최대-최소
		return arr[5] - arr[1];
	}

}
