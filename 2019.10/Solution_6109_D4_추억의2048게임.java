import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * swea 6109 D4 추억의 2048게임
 */
public class Solution_6109_D4_추억의2048게임 {

	static int N, map[][];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());
			String dir = tk.nextToken();
			map = new int[N][N];
			
			for(int i=0; i<N; i++) {
				tk = new StringTokenizer(br.readLine());
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(tk.nextToken());
				}
			}

			move(dir.charAt(0));
			
			//output
			System.out.println("#" + t );
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					System.out.print(map[i][j]+" ");
				}
				System.out.println();
			}
		} //tc
	}
	private static void move(char dir) {
		switch(dir) {
		case 'u':	//up
			up();
		
			for(int j=0; j<N; j++) {
				for(int i=0; i<N-1; i++) {
					if(map[i][j] == map[i+1][j]) {
						map[i][j] = map[i][j] + map[i+1][j];
						map[i+1][j] = 0;
					}
				}
			}
		
			up();	
			break;
		case 'd':	//down
			down();
			for(int j=0; j<N; j++) {
				for(int i=N-1; i>0; i--) {
					if(map[i][j] == map[i-1][j]) {
						map[i][j] = map[i][j] + map[i-1][j];
						map[i-1][j] = 0;
					}
				}
			}
			down();
			break;
		case 'l':	//left
			left();
			
			for(int i=0; i<N; i++) {
				for(int j=0; j<N-1; j++) {
					if(map[i][j] == map[i][j+1]) {
						map[i][j] = map[i][j] + map[i][j+1];
						map[i][j+1] = 0;
					}
				}
			}
		
			left();	
			
			break;
		case 'r':	//right
			right();
			
			for(int i=0; i<N; i++) {
				for(int j=N-1; j>0; j--) {
					if(map[i][j] == map[i][j-1]) {
						map[i][j] = map[i][j] + map[i][j-1];
						map[i][j-1] = 0;
					}
				}
			}
		
			right();	
			break;
		}
	}
	private static void up() {
		int cnt;
		
		for(int j=0; j<N; j++) {
			cnt=0;
			for(int i=0; i<N; i++) {
				if(map[i][j] == 0) {
					cnt++;
				}else {
					if(cnt != 0) {
						map[i-cnt][j] = map[i][j];
						map[i][j]=0;
					}
				}
			}
		}
		
	}//up
	
	private static void down() {
		int cnt;
		
		for(int j=0; j<N; j++) {
			cnt=0;
			for(int i=N-1; i>=0; i--) {
				if(map[i][j] == 0) {
					cnt++;
				}else {
					if(cnt != 0) {
						map[i+cnt][j] = map[i][j];
						map[i][j]=0;
					}
				}
			}
		}
		
	}//down
	
	private static void left() {
		int cnt;
		
		for(int i=0; i<N; i++) {
			cnt=0;
			for(int j=0; j<N; j++) {
				if(map[i][j] == 0) {
					cnt++;
				}else {
					if(cnt != 0) {
						map[i][j-cnt] = map[i][j];
						map[i][j]=0;
					}
				}
			}
		}
		
	}//left
	
	private static void right() {
		int cnt;
		
		for(int i=0; i<N; i++) {
			cnt=0;
			for(int j=N-1; j>=0; j--) {
				if(map[i][j] == 0) {
					cnt++;
				}else {
					if(cnt != 0) {
						map[i][j+cnt] = map[i][j];
						map[i][j]=0;
					}
				}
			}
		}
		
	}//right
	
	
	
	public void print() {
		for (int[] string : map) {
			System.out.println(Arrays.toString(string));
		}
		System.out.println();
	}
	

}
