import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * swea 5658 모의 보물상자 비밀번호
 */
public class Solution_5658_보물상자비밀번호_김혜희 {

	static int N,K, M;
	static char[] arr;
	static List<Integer> numList;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			numList = new LinkedList<Integer>();
			
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());
			K = Integer.parseInt(tk.nextToken());
			M = N/4;
			arr = new char[N];
			String line = br.readLine();
			for(int i=0; i<N; i++) {
				arr[i] = line.charAt(i); 
			}
			
			for(int i=0; i<M; i++) {
				rotate();
				addList();
			}
			
			Collections.sort(numList);
//			System.out.println(numList.toString());
			
			for(int i=0; i<numList.size(); i++) {
				for(int j=i+1; j<numList.size(); j++) {

					if(numList.get(i).equals(numList.get(j))) {
						numList.remove(j);
						j--;
					}
				}
			}
			
//			System.out.println(numList.toString());
			
			int answer = numList.get(numList.size()-K);
			//output
			System.out.println("#" + t + " " + answer);
		} //tc
	}//main

	private static void rotate() {
		char temp = arr[N-1];
		for(int i=N-1; i>0; i--) {
			arr[i] = arr[i-1];
		}
		arr[0] = temp;
	}// rotate;
	
	private static void addList() {
		String st;
		int temp;
		for(int i=0; i<N;) {
			st = new String(arr, i, M);
			temp = Integer.parseInt(st,16);			
			numList.add(temp);			

			i=i+M;
		}
		
	}//addlist

}
