import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * swea 2105 모의] 디저트 카페
 *
 */
public class Solution_2105_디저트카페_김혜희 {

	static int N, map[][], sum, MAX, startR, startC;
	static boolean visited[];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			visited = new boolean[101];
			MAX = -1;
			
			N = Integer.parseInt(br.readLine());
			map = new int[N][N];
			for(int i=0; i<N; i++) {
				tk = new StringTokenizer(br.readLine());
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(tk.nextToken());
				}
			}
			
			
			for(int j=1; j<N-1; j++) {
				for(int i=0; i<N-2; i++) {
					visited[map[i][j]] = true;
					startR = i;
					startC = j;
					dfs(i,j,0,1);
					visited[map[i][j]] = false;
				}
			}
			
			
			
			//output
			System.out.println("#" + t + " " + MAX);
		} //tc
	}
	private static void dfs(int r, int c, int state, int sum) {
		if(r-1==startR && c-1==startC) {
			if(sum>MAX) {
				MAX = sum;
			}
			return;
		}
		
		
		
		int curR, curC;
		switch(state) {
		case 0:
		case 1: //1.남서
			curR = r+1;
			curC = c-1;
			if(curR>=0 && curR<N && curC>=0 && curC<N //범위 체크
					&& visited[map[curR][curC]] == false) {
				visited[map[curR][curC]] =true;
				dfs(curR, curC, 1, sum+1);
				visited[map[curR][curC]] = false;
			}
			
			if(state==0) break;
			
		case 2: //2.남동
			curR = r+1;
			curC = c+1;
			if(curR>=0 && curR<N && curC>=0 && curC<N //범위 체크
					&& visited[map[curR][curC]] == false) {
				visited[map[curR][curC]] =true;
				dfs(curR, curC, 2, sum+1);
				visited[map[curR][curC]] = false;
			}
			
			if(state==1) break;
			
		case 3: //3.북동
			curR = r-1;
			curC = c+1;
			if(curR>=0 && curR<N && curC>=0 && curC<N //범위 체크
					&& visited[map[curR][curC]] == false) {
				visited[map[curR][curC]] =true;
				dfs(curR, curC, 3, sum+1);
				visited[map[curR][curC]] = false;
			}
			
			if(state==2) break;
			
		case 4: //4.북서
			curR = r-1;
			curC = c-1;
			if(curR>=0 && curR<N && curC>=0 && curC<N //범위 체크
					&& visited[map[curR][curC]] == false) {
				visited[map[curR][curC]] =true;
				dfs(curR, curC, 4, sum+1);
				visited[map[curR][curC]] = false;
			}
			
			break;
		}//switch
		

		
	}//dfs

}
