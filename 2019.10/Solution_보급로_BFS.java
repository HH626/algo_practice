import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * swea 1249 보급로
 *	bfs
 *	다익스트라
 */
class Point{
	int r, c;
	int val;
	
	Point (int r, int c){
		this.r = r;
		this.c = c;
	}
	
}

public class Solution_보급로_BFS {

	static int[][] map;
	static int N, min;
	static boolean[][] visited;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {//tc
			min = Integer.MAX_VALUE;
			N = Integer.parseInt(br.readLine());
			map = new int[N][N];
			visited = new boolean[N][N];
			String line;
			for(int i=0; i<N; i++) {
				line = br.readLine();
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(line.charAt(j)+"");
				}
			}
			
			min = bfs();
			
			//output
			System.out.println("#" + t + " " + min);
		} //tc
		
		
		
	}//main
	static int[] dirR= {-1,0,1,0};
	static int[] dirC= {0,1,0,-1};
	static int MAX = Integer.MAX_VALUE;
	private static int bfs() {
		Queue<Point> queue = new LinkedList<>();
		int[][] map2 = new int[N][N]; //(r,c)���� ���� �ּڰ� ������ �迭
		for (int[] is : map2) {
			Arrays.fill(is, MAX);
		}
		Point point;
		int r, c, nextR, nextC;
		
		queue.offer(new Point(0,0));
//		visited[0][0]=true;		
		map2[0][0] = 0;
		//int count=0;
		while(!queue.isEmpty()) {
			point = queue.poll();
			r = point.r;
			c = point.c;
			for(int d=0; d<4; d++) {
				nextR = r+dirR[d];
				nextC = c+dirC[d];
				if(nextR>=0 && nextC>=0 && nextR<N && nextC<N ) {
					if(map2[r][c]+map[nextR][nextC]<map2[nextR][nextC]) {
						map2[nextR][nextC]=map2[r][c]+map[nextR][nextC];
						queue.offer(new Point(nextR, nextC));
					}
//					if(visited[nextR][nextC]==false) {
//						visited[nextR][nextC]=true;
//						queue.offer(new Point(nextR, nextC));
						//System.out.println(count++);
//					}
				} 
			}
			
		}//while
		
		
		return map2[N-1][N-1];

	}//bfs

}
