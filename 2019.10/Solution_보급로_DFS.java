import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * swea 1249 보급로
 * DFS memoization
 */
public class Solution_보급로_DFS {

	static int[][] map, map2;
	static int N, min;
	static boolean[][] visited;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {//tc
			min = Integer.MAX_VALUE;
			N = Integer.parseInt(br.readLine());
			map = new int[N][N];
			map2 = new int[N][N];	// ���� ��ġ���� ���µ� �ּ� ��� ����
			for (int[] arr : map2) {
				Arrays.fill(arr, Integer.MAX_VALUE);
			}
			visited = new boolean[N][N];
			String line;
			for(int i=0; i<N; i++) {
				line = br.readLine();
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(line.charAt(j)+"");
				}
			}
			
			dfs(0,0,0);
			
			//output
			System.out.println("#" + t + " " + min);
		} //tc
		
		
		
	}//main
	static int[] dirR= {-1,0,1,0};
	static int[] dirC= {0,1,0,-1};
	
	private static void dfs(int r, int c, int sum) {
		if(sum>min) {
			return;
		}
		if(map2[r][c]>sum) {
			map2[r][c] = sum;
		}else {
			return;
		}
		
		if(r==N-1 && c==N-1) {
			if(sum<min) {
				min = sum;
			}
			return;
		}
		
		int nextR, nextC;
		for(int d=0; d<4; d++) {
			nextR = r+dirR[d];
			nextC = c+dirC[d];
			if(nextR>=0 && nextC>=0 && nextR<N && nextC<N && visited[nextR][nextC]==false) {
				visited[nextR][nextC] = true;
				dfs(nextR, nextC, sum+map[nextR][nextC]);
				visited[nextR][nextC] = false;			
			} 
		}
	}

}
