import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;
/**
 * 백준 1149 RGB거리 
 * dp
 */
public class Main_1149_RGB거리_김혜희 {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int N = Integer.parseInt(br.readLine());
		int[][] map = new int[N][3]; //R,G,B
		for(int i=0; i<N; i++) {
			tk = new StringTokenizer(br.readLine());
			map[i][0] = Integer.parseInt(tk.nextToken());	//R
			map[i][1] = Integer.parseInt(tk.nextToken());	//G
			map[i][2] = Integer.parseInt(tk.nextToken());	//B
		}
		
		int[][] dp = new int[N][3]; 
		dp[0][0] = map[0][0];
		dp[0][1] = map[0][1];
		dp[0][2] = map[0][2];
		
		
		for(int i=1; i<N; i++) {
			//R
			dp[i][0] = map[i][0] + Math.min(dp[i-1][1], dp[i-1][2]);
			//G
			dp[i][1] = map[i][1] + Math.min(dp[i-1][0], dp[i-1][2]);
			//B
			dp[i][2] = map[i][2] + Math.min(dp[i-1][0], dp[i-1][1]);
		}
		
		
		Arrays.sort(dp[N-1]);
		
		System.out.println(dp[N-1][0]);
		
	}//main

}
