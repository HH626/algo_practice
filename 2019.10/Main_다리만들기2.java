import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * 백준 17472 다리만들기2
 *
 */

class Point {
	int r, c;
	
	Point(int r, int c){
		this.r = r;
		this.c = c;
	}
}
public class Main_다리만들기2 {
	
	static int R, C, cnt, answer, min=Integer.MAX_VALUE;
	static int[][] map, adjMat;
	static int[][] visited;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		C = sc.nextInt();
		map = new int[R][C];
		for(int i=0; i<R; i++) {
			for(int j=0; j<C; j++) {
				map[i][j] = sc.nextInt();
			}
		}
		
		//1.섬 번호 붙이기
		cnt=1;
		color();
		
		//2. 섬 인접 행렬 만들기
		adjMat = new int[cnt+1][cnt+1];
		for (int[] a : adjMat) {
			Arrays.fill(a, 10000);
		}
		makeMatrix();
		
		//3.다 연결된거중에 값이 짧은거 찾기
		//3-1. 다 연결 안된섬 거르기;
		boolean flag;
		for(int i=2; i<=cnt; i++) {
			flag = false;
			for(int j=2; j<=cnt; j++) {
				if(adjMat[i][j]<10000 && adjMat[i][j]>1) { //연결된 섬이 있다
					flag = true;
					break;
				}
			}
			if(!flag) { //섬a에 연결된 섬이 하나도 없으면
				//output
				answer = -1;
			}
		}
		
		//3-2. 연결섬 최솟값 찾기
		visited = new int[cnt+1][cnt+1];
		comb(2,2,0);

		System.out.println(answer);
		
	}//main
	
	public static boolean isConnected() {
		Queue<Integer> que = new LinkedList<>();
		int num=0;
		boolean[] vist = new boolean[cnt+1];

		for(int i=2; i<=cnt; i++) {
			for(int j=2; j<=cnt; j++) {
				if(visited[i][j]==1) {
					num = i;
					break;
				}
			}
			if(num!=0) {break;}
		}
		vist[num]=true;
		que.offer(num);
		while(!que.isEmpty()) {
			num = que.poll();
			for(int j=2; j<=cnt; j++) {
				if(visited[num][j]==1 && !vist[j]) {
					vist[j] = true;
					que.offer(j);
				}
				
			}
			
		}//while
		
		for(int i=2; i<=cnt; i++) {
			if(vist[i]==false) {
				return false;
			}
		}
		
		return true;
		
	}//isco
	
	public static void comb(int i, int j, int sum) {
		
		if(j>cnt) {
			i++;
			j=i+1;
		}
		
		
		for(; i<cnt; i++) {
			for(; j<=cnt; j++) {
				if(adjMat[i][j]<10000 && adjMat[i][j]>1) {
					visited[i][j] = visited[j][i] = 1;
//					System.out.println(i+" ij "+j);
					comb(i,j+1, sum+adjMat[i][j]);
					visited[i][j] = visited[j][i] = 0;
					
				}	
			}
			j=i+2;
		}
		
		if(i==cnt) {
			//연결확인
//			for (int[] ks : visited) {
//				System.out.println(Arrays.toString(ks));
//			}
//			System.out.println(sum);
			
//			isConnected();
			
			if(isConnected()) {
				//최솟값
				if(sum !=0 && min>sum) {
					min = sum;
					answer = min;
				}				
			}
			
			return;
		}
		
	}// comb
	
	
	
	public static void makeMatrix() {
		int start, end, len;
		for(int i=0; i<R; i++) {
			for(int j=0; j<C; j++) {
				
				if(map[i][j] == 0) continue;
				
				start = map[i][j];
				len=0;
				for(int k=j+1; k<C; k++) {//오른쪽으로 인접한 섬 있나 확인
					if(map[i][k]==start) {
						break;
					}else if(map[i][k]==0) {
						len++;
					}else {
						end=map[i][k];
						if(len > 1 && len<adjMat[start][end]) { //start섬과 end섬을 잇는 더 짧은 다리 생기면 갱신
							adjMat[start][end] = adjMat[end][start] = len;
						}
						break;
					}
					
				}
				len =0;
				for(int k=i+1; k<R; k++) {//아래로 인접한 섬 있나 확인
					if(map[k][j]==start) {
						break;
					}else if(map[k][j]==0) {
						len++;
					}else {
						end=map[k][j];
						if(len > 1 && len<adjMat[start][end]) { //start섬과 end섬을 잇는 더 짧은 다리 생기면 갱신
							adjMat[start][end] = adjMat[end][start] = len;
						}
						break;
					}
				}
				
			}
		}
		
	}// make adj matrix

	public static void color(){
		Queue<Point> queue = new LinkedList<Point>();
		
		for(int i=0; i<R; i++) {
			for(int j=0; j<C; j++) {
				if(map[i][j] == 1) {
					++cnt;
					map[i][j] = cnt;
					queue.offer(new Point(i,j));
					bfs(queue);
				}
			}
		}
	}//color
	
	static int[] dirR= {-1,0,1,0};
	static int[] dirC= {0,1,0,-1};
	public static void bfs(Queue<Point> queue) {
		Point point;
		int r, c, nextR, nextC;
		
		while(!queue.isEmpty()) {
			point = queue.poll();
			r = point.r;
			c = point.c;
			for(int d=0; d<4; d++) {
				nextR = r+dirR[d];
				nextC = c+dirC[d];
				if(nextR>=0 && nextC>=0 && nextR<R && nextC<C && map[nextR][nextC]==1) {
					map[nextR][nextC] = cnt;
					queue.offer(new Point(nextR,nextC));
				}
			}			
		}
	}//bfs
}
