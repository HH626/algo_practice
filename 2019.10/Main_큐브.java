import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 16939 2x2x2큐브
 */
public class Main_큐브 {

//	static final int top=1, bottom=6, front=2, back=5, left=3, right=4;
	static final int top=0, bottom=2, front=1, back=5, left=3, right=4;
	static int[][] cube = new int[6][5];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk = new StringTokenizer(br.readLine());
		for(int i=0; i<6; i++) {
			cube[i][1] = Integer.parseInt(tk.nextToken());
			cube[i][2] = Integer.parseInt(tk.nextToken());
			cube[i][3] = Integer.parseInt(tk.nextToken());
			cube[i][4] = Integer.parseInt(tk.nextToken());
		}
		
		
		int answer =0;
		if(checkFour(top) && checkFour(bottom)) {
			if(checkTwo(1) && rotate(1)) {
				answer=1;
			}
		}else if(checkFour(front) && checkFour(back)) {
			if(checkTwo(2)&& rotate(2)) {
				answer=1;
			}
			
		}else if(checkFour(left)&& checkFour(right)) {
			if(checkTwo(3)&& rotate(3)) {
				answer=1;
			}
		}else { // 풀수 없음
			answer=0;
		}
		
		System.out.println(answer);
		
	}//main
	
	static boolean rotate(int n) {
		switch(n) {
		case 1: //top-bottom
			//right
			if(cube[front][1] == cube[right][3]
				&& cube[right][1] == cube[back][3]
				&& cube[back][1] == cube[left][3]
				&& cube[left][1] == cube[front][3]) {
				return true;
			}
			//left
			if(cube[front][1] == cube[left][3]
					&& cube[left][1] == cube[back][3]
					&& cube[back][1] == cube[right][3]
					&& cube[right][1] == cube[front][3]) {
				return true;
			}
			
			break;
		case 2: //front-back
			if(cube[top][3]== cube[right][2]
					&& cube[right][1] == cube[bottom][3]
					&& cube[bottom][1] == cube[left][1]
					&& cube[left][2] == cube[top][1]) {
				return true;
			}
			if(cube[top][3] == cube[left][1]
					&&cube[left][2] == cube[bottom][3]
					&&cube[bottom][1] == cube[right][2]
					&&cube[right][1] == cube[top][1]) {
				return true;
			}
		case 3: //left-right
			if(cube[top][2]== cube[back][2]
					&& cube[back][1] == cube[bottom][1]
					&& cube[bottom][2] == cube[front][1]
					&& cube[front][2] == cube[top][1]) {
				return true;
			}
			if(cube[top][2] == cube[front][1]
					&& cube[front][2] == cube[bottom][1]
					&& cube[bottom][2] == cube[back][2]
					&& cube[back][1] == cube[top][1]) {
				return true;
			}
			
		}//switch
		return false;
	}
	
	
	static boolean checkTwo(int n) {
		
		switch(n) {
		case 1: //top-bottom
			int[] arr = {front, right, back, left};
			for(int i=0; i<4; i++) {
				if(cube[arr[i]][1] == cube[arr[i]][2] && cube[arr[i]][3] == cube[arr[i]][4]) {}
				else { return false;}
			}
			break;
		
		case 2:	//front-back
			if(cube[top][1] == cube[top][2] && cube[top][3] == cube[top][4]) {}
			else { return false;}
			if(cube[bottom][1] == cube[bottom][2] && cube[bottom][3] == cube[bottom][4]) {}
			else { return false;}
			if(cube[left][1] == cube[left][3] && cube[left][2] == cube[left][4]) {}
			else { return false;}
			if(cube[right][1] == cube[right][3] && cube[right][2] == cube[right][4]) {}
			else { return false;}
			
			break;
			
		case 3: //left-right
			int[] arr2 = {top, front, bottom, back};
			for(int i=0; i<4; i++) {
				if(cube[arr2[i]][1] == cube[arr2[i]][3] && cube[arr2[i]][2] == cube[arr2[i]][4]) {}
				else { return false;}
			}
			
		}//switch
		
		return true;
		
	}
	
	static boolean checkFour(int side) {
		int color = cube[side][1];
		for(int i=2; i<=4; i++) {
			if(cube[side][i] != color) {
				return false;
			}
		}
		return true;
	}//check
	

}
