import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * swea 5672 D4 올해의 조련사
 *
 */
public class Solution_5672_D4_올해의조련사_김혜희 {

	static int N;
	static char[] arr, newArr;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			
			N=Integer.parseInt(br.readLine());
			arr = new char[N];
			newArr = new char[N];
			
			for(int i=0; i<N; i++) {
				arr[i] = br.readLine().charAt(0);
			}
			
			
			//
			int i=0, j=N-1, n=0;
			int nexti, nextj;
			while(n<N) {
				if(i==j) {
					newArr[n] = arr[i];
					n++;
					break;
				}
				nexti = i;
				nextj = j;
				while (true) {
					if (arr[nexti] < arr[nextj]) {
						newArr[n] = arr[i];
						i++;
						n++;
						break;
					} else if (arr[nexti] > arr[nextj]) {
						newArr[n] = arr[j];
						j--;
						n++;
						break;
					} else {
						nexti++;
						nextj--;
						if (nexti >= nextj) {
							newArr[n] = arr[i];
							i++;
							n++;
							break;
						}
					}
				} // while
				
				
			}//while
			
			
			String answer = new String(newArr);
			
			//output
			System.out.println("#" + t + " " + answer);
		} //tc
		
	}

}
