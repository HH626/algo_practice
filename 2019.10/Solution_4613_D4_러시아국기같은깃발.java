import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * swea 4613 D4 러시아 국기 같은 깃발
 * @author student
 *
 */
public class Solution_4613_D4_러시아국기같은깃발 {

	static int R,C, table[][], MIN;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			tk = new StringTokenizer(br.readLine());
			R = Integer.parseInt(tk.nextToken());
			C = Integer.parseInt(tk.nextToken());
			table = new int[R][3];
			MIN = Integer.MAX_VALUE;
			
			String line;
			char ch;
			int white, blue, red;
			for(int i=0; i<R; i++) {
				line = br.readLine();
				white = blue = red = 0;
				for(int j=0; j<C; j++) {
					ch = line.charAt(j);
					if(ch=='W') { white++; }
					else if(ch=='B') { blue++; }
					else if(ch=='R') { red++; }
				}
				table[i][0]= blue+red;		//흰색으로 칠하기로 했을때 바꿔야하는 천 갯수
				table[i][1]= white+red;		//파란색
				table[i][2]= white+blue;	//빨간색
			}
			
			go(0,1,table[0][0]);
			
			int answer = MIN;
			
			//output
			System.out.println("#" + t + " " + answer);
		} //tc
	}
	
	private static void go(int color, int n, int sum) {
		if(n==R) {
			if(color == 2 && sum<MIN) {
				MIN = sum;
			}
			return;
		}
		
		switch(color) {
		case 0:
			go(0, n+1, sum+table[n][0]);
		case 1:
			go(1, n+1, sum+table[n][1]);
			if(color==0) break;
		case 2:
			go(2, n+1, sum+table[n][2]);
			break;
		}
		
	}//go

}
