import java.util.Scanner;

/**
 * 백준 14002 가장 긴 증가하는 부분 수열4
 */
public class Main_LIS4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int N = sc.nextInt();
		int[] arr = new int[N];
		for(int i=0; i<N; i++) {
			arr[i] = sc.nextInt();
		}

		int[] dp = new int[N];
		int max=1, maxIdx=0;
		for (int i = 0; i < N; i++) {
			dp[i] =1;
			for (int j = 0; j < i; j++) {
				if(arr[j] < arr[i]) {
					if(dp[j]>=dp[i]) {
						dp[i] = dp[j]+1; 
						if(dp[i]>max) {
							max = dp[i];
							maxIdx = i;
						}
					}
				}
			}
		}
		
		int[] nums = new int[max];
		int val = max;
		for(int i=maxIdx; i>=0; i--) {
			if(dp[i]==val) {
				nums[--val] = arr[i];				
			}
		}
		
		System.out.println(max);
		for (int num : nums) {
			System.out.print(num+" ");
		}
	}//main

}
