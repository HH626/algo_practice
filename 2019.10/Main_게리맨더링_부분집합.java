import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 17471 게리맨더링
 *
 */
public class Main_게리맨더링_부분집합 {

	static int N, min=Integer.MAX_VALUE;
	static int mat[][], people[];
	static boolean  arr[];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		//2 ≤ N ≤ 10
		N = Integer.parseInt(br.readLine());
		people = new int[N+1];	//지역 사람 수
		mat = new int[N+1][N+1];
		
		tk = new StringTokenizer(br.readLine());
		for(int i=1; i<=N; i++) {
			people[i] = Integer.parseInt(tk.nextToken());
		}
		
		int n, k;
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			n = Integer.parseInt(tk.nextToken());
			for(int j=0; j<n; j++) {
				k = Integer.parseInt(tk.nextToken());
				mat[i][k] = 1;
				mat[k][i] = 1;
			}
		}
		
		//조합
		
		for(int i=2; i<Math.pow(2, N+1); i++) {		//i: 비트 마스킹 할 거
			for(int j=1; j<=N; j++) {	//j: 마을 번호 1~N
				if((i & 1<<N) != 0) {//부분집합 포함하면
					list1.add(j);
				}else {
					list2.add(j);
				}
			}
			check(list1);
			check(list2);
		}
		dfs(1);

		
		System.out.println(min);

	}//main
	
	private static void dfs(int city) {
		if(city == N+1) {
			//이어진 도시인지 확인
			if(check()) {
				//인구수 차이
				int sum1=0, sum2=0;
				for(int i=1; i<=N; i++) {
					if(arr[i]==true) {
						sum1 += people[i];
					}else {
						sum2 += people[i];
					}
				}
				if(Math.abs(sum1-sum2)<min) {
					min = Math.abs(sum1-sum2);
				}
			}
			return;
		}
		
		//포함
		arr[city]=true;
		dfs(city+1);
		//불포함
		arr[city]=false;
		dfs(city+1);
		
	}//dfs

	private static boolean check() {
		Queue<Integer> queue = new LinkedList<Integer>();
		boolean[] check = new boolean[N+1];
		int t1=0, t2=0;
		for(int i=1; i<=N; i++) {
			if(arr[i]) {
				t1 = i;
				break;
			}
		}
		for(int i=1; i<=N; i++) {
			if(arr[i]==false) {
				t2 = i;
				break;
			}
		}

		queue.offer(t1);
		//1팀 연결
		while(!queue.isEmpty()) {
			t1 = queue.poll();
			for(int j=1; j<=N; j++ ) {
				if(check[j] != true && mat[t1][j]==true && arr[j]==true ) {
					queue.offer(j);
					check[j]=true;
				}
			}
		}
		//2팀연결
		queue.offer(t2);
		while(!queue.isEmpty()) {
			t2 = queue.poll();
			for(int j=1; j<=N; j++ ) {
				if(check[j] != true && mat[t2][j]==true && arr[j]==false) {
					queue.offer(j);
					check[j]=true;
				}
			}
		}
		
		//다 이어져 있는지 확인
		for(int i=1; i<=N; i++) {
			if(check[i]==false) {
				return false;
			}
		}
		
		return true;
		
	}//check

}
