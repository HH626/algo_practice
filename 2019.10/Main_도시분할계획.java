import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

/**
 * 백준 1647 도시분할계획
 * 크루스칼 O(ElogE)
 */

class Node implements Comparable<Node>{
	int v1, v2;
	int weigth;
	
	public Node(int a, int b, int c) {
		this.v1 = a;
		this.v2 = b;
		this.weigth = c;
	}
	
	public int compareTo(Node o) {
		return this.weigth- o.weigth;
	};
}
public class Main_도시분할계획 {

	static int N, M, parent[];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());	//집 
		M = Integer.parseInt(tk.nextToken());	//경로
		
		parent = new int[N+1];
		PriorityQueue<Node> pq = new PriorityQueue<Node>();
		
		int a, b, c; //집1, 집2, 경로 가중치
		for(int i=0; i<M; i++) {
			tk = new StringTokenizer(br.readLine());
			a = Integer.parseInt(tk.nextToken());
			b = Integer.parseInt(tk.nextToken());
			c = Integer.parseInt(tk.nextToken());
			pq.offer(new Node(a,b,c));
		}
		
		//
		
		make();
		
		
		Node node ;
		int i,j, cnt=0, sum=0, max=0;
		boolean[] visited = new boolean[N+1];
		while(cnt<N-1) {
			node = pq.poll();
			i= node.v1;
			j= node.v2;
			if(union(i,j)) {
				sum += node.weigth;
				if(max<node.weigth) {
					max = node.weigth;
				}				
				cnt++;
			}
			
		}//while
		
		System.out.println(sum-max);
		
	}//main
	
	public static void make() {
		Arrays.fill(parent, -1);
	}
	
	public static int find(int a) {
		if(parent[a]==-1) return a;
		return parent[a] = find(parent[a]);
	}
	
	public static boolean union(int a, int b) {
		int aRoot = find(a);
		int bRoot = find(b);
		if(aRoot == bRoot) return false;
		parent[bRoot] = aRoot;
		return true;
	}

}
