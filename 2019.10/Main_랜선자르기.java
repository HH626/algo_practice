import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 2933 랜선자르기
 */
public class Main_랜선자르기{

	static int K, N, arr[];
	static long answer;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk = new StringTokenizer(br.readLine());
		
		K = Integer.parseInt(tk.nextToken());
		N = Integer.parseInt(tk.nextToken());
		arr = new int[K];
		int max =0;
		for(int i=0; i<K; i++) {
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]>max) {
				max = arr[i];
			}
		}
		
		answer = max;
		binarySearch(1, max);
		
		System.out.println(answer);
	}
	
	public static void binarySearch(long left, long right) {
		if(right-left<0) {         
			return;
		}
		
		long mid = (left+right)/2;
		
		int sum=0;
		for(int i=0; i<K;i++) {
			sum += arr[i]/mid;
		}
	
		if(sum<N) { // 렌선을 더 짧게 짤라야해
			binarySearch(left, mid-1);
		}else if(sum>=N) { //랜선 더길게
			answer=mid;
			binarySearch(mid+1, right);
		}
	}

}
