import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 16917 양념 반 후라이드 반
 */
public class Main_양념반후라이드반 {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk = new StringTokenizer(br.readLine());
		int seasoned = Integer.parseInt(tk.nextToken());	//양념치킨 가격
		int fried = Integer.parseInt(tk.nextToken());		//후라이드 가격
		int half = Integer.parseInt(tk.nextToken());		//반반 가격
		int numS = Integer.parseInt(tk.nextToken());		//양념 최소 갯수
		int numF = Integer.parseInt(tk.nextToken());		//후라이드 갯수
		int numH =0 ;
		
		//1. 양념1+후라이드1보다 반반2를 시키는게 더 싼 경우
		if(seasoned + fried > 2*half) {
			numH = Math.min(numS, numF);
			numS = numS-numH;
			numF = numF-numH;
			numH = numH*2;
		}
		//2. 후라이드 1보다 반반2를 시키는게 더 싼 경우
		if(fried > 2*half) {
			numH += numF*2;
			numF = 0;
		}
		//3. 양념1보다 반반2를 시키는게 더 싼 경우
		if(seasoned > 2*half) {
			numH += numS*2;
			numS = 0;
		}
		
		//output
		System.out.println(numS*seasoned + numF*fried + numH*half);
	}//main

}
