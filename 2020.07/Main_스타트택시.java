import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 19238 스타트택시
 */
public class Main_스타트택시 {
	static class Point{
		int r, c;
		int gas;
		int dist;
		Point(int r, int c, int gas, int dist){
			this.r = r;
			this.c = c;
			this.gas = gas;
			this.dist = dist;
		}
		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + ", gas=" + gas + ", dist=" + dist + "]";
		}
		
	}
	static int N, M, map[][];
	static Point taxi, dest[];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());	//맵 크기
		M = Integer.parseInt(tk.nextToken());	//승객수 : ~N^2
		int initGas = Integer.parseInt(tk.nextToken());	//연료
		
		map = new int[N+2][N+2];				// -1:벽, 0:빈칸, 1~:승객의 출발지
		dest = new Point[M+1];					//n번 승객의 목적지
		
		for(int i=0; i<N+2; i++) {
			Arrays.fill(map[i], -1);
			if(i==0 || i==N+1) continue;
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(tk.nextToken()) * (-1);	//벽을 -1로 바꾸기
			}
		}
		
		tk = new StringTokenizer(br.readLine());
		int r = Integer.parseInt(tk.nextToken());
		int c = Integer.parseInt(tk.nextToken());
		taxi = new Point(r,c,initGas,0);
				
		for(int i=1; i<=M; i++) {
			tk = new StringTokenizer(br.readLine());
			r = Integer.parseInt(tk.nextToken());
			c = Integer.parseInt(tk.nextToken());
			map[r][c] = i;								//map에 승객 출발지 위치 표시
			r = Integer.parseInt(tk.nextToken());
			c = Integer.parseInt(tk.nextToken());
			dest[i] = new Point(r,c,-1,-1);
		}
		/////input
		
		int cnt = 0;
		while(cnt>=0 && cnt<M) {
			////1.가장 가까운 승객 태우러 가기
			int passenger = goToPassenger();
			if(passenger<0) {
				cnt=-1;
				break;
			}
			////2.승객의 목적지로 가기
			cnt += goToDestination(passenger);
		}
		
		/////output
		System.out.println(cnt<0? -1 : taxi.gas);
		
	}//main
	
	static int[] dR = {-1,0,0,1};
	static int[] dC = {0,-1,1,0};
	private static int goToPassenger() {
		PriorityQueue<Point> que = new PriorityQueue<Point>(new Comparator<Point>() {
			@Override
			public int compare(Point o1, Point o2) {
				if(o1.dist != o2.dist) return o1.dist-o2.dist;
				else if(o1.r != o2.r) return o1.r-o2.r;
				else return o1.c-o2.c;
			}
		});
		boolean[][] visited = new boolean[N+2][N+2];
		
		visited[taxi.r][taxi.c] = true;
		que.offer(taxi);
		
		while(!que.isEmpty()) {
			Point p = que.poll();
			int r = p.r;
			int c = p.c;
			int gas = p.gas;
			
			//가스가 다 떨어지면
			if(gas<=0) {
				return -1;
			}
			
			//승객을 만나면 태우기
			if(map[r][c] > 0) {
				int passenger = map[r][c];
				map[r][c] = 0;
				taxi = new Point(r,c,gas,0);
				return passenger;
			}
			
			for(int d=0; d<4; d++) {
				int nr = r+dR[d];
				int nc = c+dC[d];
				
				if(visited[nr][nc] || map[nr][nc]<0) continue;
				
				visited[nr][nc] = true;
				que.offer(new Point(nr,nc,gas-1,p.dist+1));
			}
			
		}//while
		
		return -1;
		
	}//goToP
	
	private static int goToDestination(int passenger) {
		Queue<Point> que = new LinkedList<Point>();
		boolean[][] visited = new boolean[N+2][N+2];
		
		Point destination = dest[passenger];
		
		visited[taxi.r][taxi.c] = true;
		que.offer(taxi);
		
		while(!que.isEmpty()) {
			Point p = que.poll();
			int r = p.r;
			int c = p.c;
			int gas = p.gas;
			
			//도착지에 도달함
			if(r==destination.r && c==destination.c) {
				//연료 충전
				gas += 2*p.dist;
				taxi = new Point(r,c,gas,0);
				return 1;
			}
			
			//가스가 다 떨어지면
			if(gas<=0) {
				return -1;
			}
			
			for(int d=0; d<4; d++) {
				int nr = r+dR[d];
				int nc = c+dC[d];
				
				if(visited[nr][nc] || map[nr][nc]<0) continue;
				
				visited[nr][nc] = true;
				que.offer(new Point(nr,nc,gas-1,p.dist+1));
			}
			
		}//while
		
		return -1;
		
	}//goToDestination

}
