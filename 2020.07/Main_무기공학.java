import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 18430 무기공학
 */
public class Main_무기공학 {

	static int R, C, map[][],MAX;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		
		map = new int[R+2][C+2];
		visited = new boolean[R+2][C+2];
		MAX = 0;
		
		for(int i=1; i<=R; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=C; j++) {
				map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		
		dfs(0,0);
		
		System.out.println(MAX);
		
	}//main
	static int[] dR = {1,0,-1,0};
	static int[] dC = {0,-1,0,1};
	static boolean visited[][];
	private static void dfs(int idx, int sum) {

		for(int i=idx; i<R*C; i++) {
			int r = i/C +1;
			int c = i%C +1;
			if(visited[r][c]) continue;
			
			for(int d=0;d<4;d++) {	//부메랑 4개 확인하기
				int nr = r+ dR[d];
				int nc = c+ dC[d];
				int mr = r+ dR[(d+1)%4];
				int mc = c+ dC[(d+1)%4];
				if(map[nr][nc]==0 || map[mr][mc]==0) continue;	//범위 넘어감
				if(visited[r][c] || visited[nr][nc] || visited[mr][mc]) continue;	//이미 부메랑으로 만듦
				visited[r][c] = visited[nr][nc] = visited[mr][mc] = true;
				int newBoo = map[r][c]*2+map[nr][nc]+map[mr][mc];
				MAX = Math.max(sum+newBoo, MAX);
				dfs(i+1,sum+newBoo);
				visited[r][c] = visited[nr][nc] = visited[mr][mc] = false;
			}
			
			
		}
			
	}//dfs
	
}
