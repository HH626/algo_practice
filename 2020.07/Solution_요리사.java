import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * SWEA 4012 [모의] 요리사
 */
public class Solution_요리사 {
	static int N, adjMap[][], MIN;
	static boolean arr[];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			
			N = Integer.parseInt(br.readLine());
			adjMap = new int[N][N];
			MIN = 999999999;
			
			for(int i=0; i<N; i++) {
				tk = new StringTokenizer(br.readLine());
				for(int j=0; j<N; j++) {
					adjMap[i][j] = Integer.parseInt(tk.nextToken());
				}
			}//input
			
			arr = new boolean[N];
			//1. A,B 재료 나누기
			comb(arr, 0, 0);
			
			//output
			System.out.println("#"+tc+" "+MIN);
			
		}//tc
	}//main

	private static void comb(boolean[] arr, int idx ,int cnt) {
		if(cnt>N/2) return;
		
		//나누기 완료
		if(idx>=N) {
			if(cnt==N/2) {
				//2. Sij구하기
				int a = synergy(arr, true);
				int b = synergy(arr, false);
	
				//3. 차 최소값 구하기
				int result = Math.abs(a-b);
				MIN = Math.min(MIN, result);
			}
			return;
		}
		
		arr[idx] = true;
		comb(arr, idx+1, cnt+1);
		arr[idx] = false;
		comb(arr, idx+1, cnt);
	}// comb
	
	private static int synergy(boolean[] arr, boolean flag) {
		int sum=0;
		
		for(int i=0; i<N; i++) {
			if(arr[i] != flag) continue;		//다른 팀일 때 스킵
			for(int j=0; j<N; j++) {
				if(arr[j] != flag) continue; 	//다른 팀일때 스킵
				sum += adjMap[i][j];
			}
		}
		
		return sum;
	}//synergy
}
