import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 16928 뱀과 사다리 게임
 * dfs
 */
public class Main_뱀과사다리게임 {

	public static int MIN;
	public static int[] ledder, snake, memo;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(tk.nextToken());
		int M = Integer.parseInt(tk.nextToken());
		
		ledder = new int[101];
		snake = new int[101];
		memo = new int[101];
		MIN=999999;
		
		for(int i=0; i<N; i++) {
			tk = new StringTokenizer(br.readLine());
			int a = Integer.parseInt(tk.nextToken());
			int b = Integer.parseInt(tk.nextToken());
			ledder[a]=b;
		}
		for(int i=0; i<M; i++) {
			tk = new StringTokenizer(br.readLine());
			int a = Integer.parseInt(tk.nextToken());
			int b = Integer.parseInt(tk.nextToken());
			snake[a]=b;
		}
		
		memo[1]=0;
		dfs(1,0);
		
		System.out.println(MIN);
	}

	private static void dfs(int loc, int cnt) {
		if(cnt>MIN) return;
		
		if(loc==100) {
			if(cnt<MIN) {
				MIN = cnt;
			}
			return;
		}else if(loc>100) {
			return;
		}
		
		for(int i=6; i>0; i--) {
			int nextLoc = loc +i;
			
			if(nextLoc > 100) continue;
			
			if(ledder[nextLoc]>0) {
				nextLoc = ledder[nextLoc];
			}else if(snake[nextLoc]>0) {
				nextLoc = snake[nextLoc];
			}
			
			if(memo[nextLoc]==0 || memo[nextLoc]>cnt+1) {
				memo[nextLoc]=cnt+1;
				dfs(nextLoc,cnt+1);
			}
		}
		
	}//dfs
}