import java.util.Scanner;

/**
 * 백준 15729 방탈출
 */
public class Main_방탈출 {
	static int N, arr[], result[];
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		arr = new int[N];
		result = new int[N];
		for(int i=0; i<N; i++) {
			arr[i] = sc.nextInt();
		}//in
		
		int cnt=0;
		for(int i=0; i<N; i++) {
			if(arr[i] != result [i]) {
				light(i);
				cnt++;
			}
		}
		
		//out
		System.out.println(cnt);
	}//main
	
	private static void light(int idx) {
		for(int i=idx; i<=idx+2; i++) {
			if(i >= N) continue;
			
			if(result[i] == 0) result[i] = 1;
			else if(result[i] == 1) result[i] = 0;
		}
	}

}
