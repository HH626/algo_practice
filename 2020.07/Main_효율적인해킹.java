import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;
/**
 * 백준 1325 효율적인 해킹
 *
 * adjMap -> 시간초과
 * adjList -> 통과
 */
public class Main_효율적인해킹 {

	static int V,E; //V:~10,000 E:~100,000
	static ArrayList<Integer>[] adjList;
	static int[] memo;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		V = Integer.parseInt(tk.nextToken());
		E = Integer.parseInt(tk.nextToken());
		
		adjList = new ArrayList[V+1];

		for(int i=1; i<=V; i++) {
			adjList[i] = new ArrayList<Integer>();
		}
		
		for(int i=0; i<E; i++) {
			tk = new StringTokenizer(br.readLine());
			int a = Integer.parseInt(tk.nextToken());
			int b = Integer.parseInt(tk.nextToken());
			//adjMap[b][a] = true;
			adjList[b].add(a);
		}
		//input
		
		ArrayList<Integer> answers = new ArrayList<Integer>();
		int max = 0;
		for(int i=1; i<=V; i++) {
			int com = numOfCom(i);
			if(com>max) {
				answers = new ArrayList<Integer>();
				answers.add(i);
				max = com;
			}else if(com==max) {
				answers.add(i);
			}
		}
		
		//output
		for (int com : answers) {
			System.out.print(com+" ");
		}
		
	}//main
	private static int numOfCom(int start) {
		Queue<Integer> que = new LinkedList<Integer>();
		boolean[] visited = new boolean[V+1];
		int cnt=0;

		que.offer(start);
		visited[start] = true;
		cnt++;
		while(!que.isEmpty()) {
			int from = que.poll();
			for (int i : adjList[from]) {
				if(!visited[i]) {
					que.offer(i);
					visited[i] = true;
					cnt++;
				}
			}
		}

		return cnt;
		
	}//check

}
