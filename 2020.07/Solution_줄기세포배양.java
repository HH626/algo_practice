import java.util.*;
import java.io.*;
/**
 * SWEA 5653 [모의] 줄기세포 배양
 */
public class Solution_줄기세포배양 {
	static int R, C, K, map[][];
	static LinkedList<Point> list;
	static final boolean INACT = false, ACT=true;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc = 1; tc<=T; tc++) {
			tk = new StringTokenizer(br.readLine());
			R = Integer.parseInt(tk.nextToken());
			C = Integer.parseInt(tk.nextToken());
			K = Integer.parseInt(tk.nextToken());
			
			map = new int [1000][1000];
			list = new LinkedList<Point>();
			
			for(int i=0; i<R; i++) {
				tk = new StringTokenizer(br.readLine());
				for(int j=0; j<C; j++) {
					int el = Integer.parseInt(tk.nextToken());
					map[500+i][500+j] = el;
					if(el !=0) {
						list.add(new Point(500+i,500+j,el,0,INACT));	//r,c,생명력,생성시간,status
					}
				}
			}
			
			
			for(int time=1; time<=K; time++) {
				//1.번식
				propagation(time);
				
				//2.상태 변경
				changeStatus(time);
			}
			
			System.out.println("#"+tc+" "+list.size());
		}//tc
	}//main
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static void propagation(int time) {
		ArrayList<Point> tmp = new ArrayList<Point>();
		
		for(int i=0, size=list.size(); i<size; i++) {
			Point p = list.get(i);
			
			if(p.status != ACT) continue;
			
			//1. 활성 상태이면 번식
			int r = p.r;
			int c = p.c;
			int x = p.x;
			for(int d=0; d<4; d++) {
				int nr = r+dR[d];
				int nc = c+dC[d];
				
				//1) 이미 세포가 있음
				if(map[nr][nc]>0) continue;
				
				//2) 번식할 수 있음
				tmp.add(new Point(nr,nc,x,time,INACT));
			}
		}//while
	
		
		//2.새로 늘어난 세포중에 중복 안되게 list에 넣기
		//생명수치가 높은 순으로 정렬
		Collections.sort(tmp);
		for(int i=0,size = tmp.size(); i<size; i++) {
			Point p = tmp.get(i);
			if(map[p.r][p.c] == 0 ) {
				map[p.r][p.c] = p.x;
				list.offer(p);
			}
		}//////2.
	
		
	}//propagation
	
	
	private static void changeStatus(int time) {
		for(int i=list.size()-1; i>=0; i--) {
			Point p = list.get(i);
			int birth = p.birth;
			int x = p.x;
			//1.죽은 셀 삭제
			if(time-birth >= 2*x) {
				list.remove(i);
			}
			
			//2.활성화
			if(time-birth == x) {
				p.status = ACT;
			}	
			
		}
	}//changeStatus

	
	static class Point implements Comparable<Point>{
		int r, c;
		int x;
		int birth;
		boolean status;
		Point(int r, int c, int x, int birth, boolean status){
			this.r = r;
			this.c = c;
			this.x = x;
			this.birth = birth;
			this.status = status;
		}
		@Override
		public int compareTo(Point o) {
			return o.x-this.x;
		}
		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + ", x=" + x + ", birth=" + birth + ", status=" + status + "]";
		}
		
	}

}
