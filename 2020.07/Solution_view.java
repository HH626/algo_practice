import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * SWEA 1206 [D3] view
 */
public class Solution_view {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		for(int tc=1; tc<=10; tc++) {
			int N = Integer.parseInt(br.readLine());
			int[] arr = new int[N];
			tk = new StringTokenizer(br.readLine());
			for(int i=0; i<N; i++) {
				arr[i] = Integer.parseInt(tk.nextToken());
			}//input
			
			
			int sum =0;
			for(int i=2; i<N-2; i++) {
				int leftMax = Math.max(arr[i-2], arr[i-1]);
				int rightMax = Math.max(arr[i+1], arr[i+2]);
				int max = Math.max(leftMax, rightMax);
				if(arr[i]>max) {
					sum += arr[i]-max;
					i += 2;
				}
			}
			
			//output
			System.out.println("#"+tc+" "+sum);
		}//tc
		
	}

}
