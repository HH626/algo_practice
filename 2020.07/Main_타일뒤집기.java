import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 백준 14711 타일 뒤집기 (Easy)
 */
public class Main_타일뒤집기 {
	static int N;
	static char map[][];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		N = Integer.parseInt(br.readLine());
		map = new char[N][N];
		boolean[][] flag = new boolean[N][N];		//#눌렀을 때 영향 받는 친구들 표시 false:.로바뀜, true:#으로 바뀜
		
		String line = br.readLine();
		for(int j=0; j<N; j++) {
			map[0][j] = line.charAt(j);
		}
		
		for(int i=0; i<N-1; i++) {
			//1.한줄에 #있으면 눌러서 바꿔 놓기
			for(int j=0; j<N; j++) {
				if(map[i][j] == '#') {
					if(j>0) flag[i][j-1] = flag[i][j-1]?false:true;
					if(j<N-1) flag[i][j+1] = flag[i][j+1]?false:true;
					if(i<N-1) flag[i+1][j] = flag[i+1][j]?false:true;
				}
			}
			
			//2.그 다음줄로 와서 윗줄에 뒤집어야 할 거(#) 있으면 바꿀 수 있게 #놓기
			for(int j=0; j<N; j++) {
				if(flag[i][j]) {
					map[i+1][j] = '#';
				}else {
					map[i+1][j] = '.';
				}
			}
		}
		
		
		//sysout
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				System.out.print(map[i][j]);
			}
			System.out.println();
		}
	}//main

}
