import java.util.Scanner;
/**
 * 백준 2635 수 이어가기
 * 
 * N=1일때 케이스 유의하여 풀기
 */
public class Main_수이어가기 {
	static int N, MAX;
	static String answer;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();	//~30,000
		MAX=0;
		answer = "";

		for(int i=N; i>N/2-1; i--) {
			StringBuilder sb = new StringBuilder();
			go(N,i,1,sb.append(N+" "));
		}
		
		System.out.println(MAX);
		System.out.println(answer);
	}//main
	private static void go(int n, int m, int cnt, StringBuilder sb) {
		if(m<0) {
			if(cnt>MAX) {
				MAX = cnt;
				answer = sb.toString();
			}
			return;
		}
		
		go(m,n-m,cnt+1,sb.append(m+" "));
	}//go

}
