import java.util.*;
import java.io.*;

/**
 * SWEA 5656 [모의] 벽돌깨기
 */
public class Solution_벽돌깨기 {
	public static int N,C,R, originMap[][], MIN;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());
			C = Integer.parseInt(tk.nextToken());
			R = Integer.parseInt(tk.nextToken());
			
			originMap = new int[R][C];
			MIN = 999999999;
			
			for(int i=0; i<R; i++) {
				tk = new StringTokenizer(br.readLine());
				for(int j=0; j<C; j++) {
					originMap[i][j] = Integer.parseInt(tk.nextToken());
				}
			}////input
			
			int[][] map = new int[R][C];
			copy(originMap, map);
			////1.공 떨어트릴 자리
			for(int j=0; j<C; j++) {
				permut(j,map,1);
			}
			
			/////output
			System.out.println("#"+tc+" "+MIN);
			
		}//tc
	}//main
	
	private static void permut(int c, int[][] map, int cnt) {
		if(cnt>N) {
			return;
		}
		
		int[][] curMap = new int[R][C];
		copy(map, curMap);
		for(int i=0; i<R; i++) {
			if(map[i][c] !=0) {
				////2.벽 부서짐
				boom(i,c,curMap);
				////3.빈공간 채우기
				curMap = drop(curMap);
				////4.남은 벽돌 수
				int result = count(curMap);
				MIN = Math.min(MIN, result);
				if(result == 0) return;
				break;
			}
			if(i==R-1) return;
		}
		
		for(int j=0; j<C; j++) {
			permut(j,curMap, cnt+1);
		}
	}//permut
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static void boom(int r, int c, int[][] map) {
		Queue<Point> que = new LinkedList<Point>();
		boolean[][] visited = new boolean[R][C];
		
		que.offer(new Point(r,c));
		visited[r][c] = true;
		
		while(!que.isEmpty()) {
			Point p = que.poll();
			r = p.r;
			c = p.c;
			int len = map[r][c];
			
			//지금 벽돌 없애기
			map[r][c] = 0;
			
			//지금 벽돌 영향받는 벽돌 큐에 넣기
			for(int l=1; l<len; l++) {
				for(int d=0; d<4; d++) {
					int nr = r+dR[d]*l;
					int nc = c+dC[d]*l;
					
					if(nr<0 || nr>=R || nc<0 || nc>=C || visited[nr][nc]) continue;
					
					visited[nr][nc] = true;
					que.offer(new Point(nr,nc));
				}
			}
		}//while
	}//boom
	
	private static int[][] drop(int[][] map) {
		int[][] temp = new int[R][C];
		for(int j=0; j<C; j++) {
			int cnt = R;
			for(int i=R-1; i>=0; i--) {
				if(map[i][j] !=0) {
					temp[--cnt][j] = map[i][j];
				}
			}
		}
		
		return temp;
	}//drop
	
	private static int count(int[][] map) {
		int cnt =0;
		
		for(int j=0; j<C; j++) {
			for(int i=R-1; i>=0; i--) {
				if(map[i][j] != 0) cnt++;
				else break;
			}
		}
		
		return cnt;
	}
	
	private static void copy(int[][]origin, int[][]copy) {
		for(int i=0; i<R; i++) {
			for(int j=0; j<C; j++) {
				copy[i][j] = origin[i][j];
			}
		}
	}//copy
	
	static class Point{
		int r,c;
		Point(int r, int c){
			this.r = r;
			this.c = c;
		}
		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + "]";
		}
	}//Point

}
