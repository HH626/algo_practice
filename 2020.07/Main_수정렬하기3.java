import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * 백준 10989 수정렬하기3
 * 
 * input을 담는 arr[]를 만들면 메모리초과
 */
public class Main_수정렬하기3 {
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		
		int[] count = new int[10001];
		int MAX=0;
		int el;
		for(int i=0; i<N; i++) {
			el = Integer.parseInt(br.readLine());
			count[el]++;
			MAX = Math.max(MAX, el);
		}//input

		for(int i=1; i<=MAX; i++) {
			count[i] = count[i-1] + count[i];
		}
		
		StringBuilder sb = new StringBuilder();
		for(int i=1; i<=MAX; i++) {
			while(count[i-1]<count[i]) {
				count[i-1]++;
				sb.append(i+"\n");
			}
		}
		
		System.out.println(sb);

	}//main

}


