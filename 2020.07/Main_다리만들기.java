import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 2146 다리 만들기
 */
public class Main_다리만들기 {
	static class Point{
		int r, c;
		int color;
		Point(int r, int c, int color){
			this.r = r;
			this.c = c;
			this.color = color;
		}
		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + ", color=" + color + "]";
		}
		
	}
	public static int N, map[][], MIN, color;
	public static ArrayList<Point> islandArr;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		N = Integer.parseInt(br.readLine()); //~100
		map = new int[N+2][N+2];
		color = 2;
		islandArr = new ArrayList<Point>();
		
		for(int i = 0 ;i<N+2; i++) {
			Arrays.fill(map[i], -1);
			if(i==0 || i==N+1) continue;
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}//input
		
		//섬 태두리 칠하기
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				if(map[i][j]==1) {
					map[i][j]=color;
					coloring(i,j);
					color++;
				}
			}
		}
		
		MIN = 2000;
		
		//다리 세우기
		int numIsland = islandArr.size();
		for(int i=0; i<numIsland; i++) {
			for(int j=i+1; j<numIsland; j++) {
				Point a = islandArr.get(i);
				Point b = islandArr.get(j);
				if(a.color == b.color) continue;
				MIN = Math.min(MIN, Math.abs(a.r-b.r)+Math.abs(a.c-b.c));
			}
		}
		
		
		//
		System.out.println(MIN-1);
	}//main
	
	static int[] dR = {0,1,0,-1}; //동남서북
	static int[] dC = {1,0,-1,0};
	private static void coloring(int r, int c) {
		
		islandArr.add(new Point(r,c,color));
		for(int d=0; d<4; d++) {
			int nr = r+dR[d];
			int nc = c+dC[d];
			if(map[nr][nc]==1) {
				map[nr][nc] = color;
				coloring(nr, nc);
			}
		}
	}

}
