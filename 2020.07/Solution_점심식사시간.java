import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

/**
 * SWEA 2383 [모의] 점심 식사시간 
 */
public class Solution_점심식사시간 {

	static class Point implements Comparable<Point>{
		int r, c;
		int dist; //계단까지의 거리
		boolean flag;
		
		Point(int r, int c, int dist){
			this.r = r;
			this.c = c;
			this.dist = dist;
		}
		
		@Override
		public int compareTo(Point o) {
			return this.dist-o.dist;
		}

		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + ", dist=" + dist + ", flag=" + flag + "]";
		}
		
	}
	public static int N, map[][], MIN;
	public static ArrayList<Point> stair, people;
	public static PriorityQueue<Point> group1, group2;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			
			N = Integer.parseInt(br.readLine()); //4~10
			map = new int[N][N];	//사람: 1~10, 계단:2
			MIN = 9999999;
			stair = new ArrayList<Point>();
			people = new ArrayList<Point>();

			
			for(int i=0; i<N; i++) {
				tk = new StringTokenizer(br.readLine());
				for(int j=0; j<N; j++) {
					int el = Integer.parseInt(tk.nextToken());
					map[i][j] = el;
					if(el>1) {
						stair.add(new Point(i,j,el));
					}else if(el==1) {
						people.add(new Point(i,j,-1));
					}
				}
			}//input
			
			//1.어느 계단으로 내려갈지 나누기
			grouping(0);
			
			//ouput
			System.out.println("#"+tc+" "+MIN);
			
		}//tc
	}
	private static void grouping(int idx) {
		
		if(idx==people.size()) {
			//그룹 묶기
			group1 = new PriorityQueue<Point>();
			group2 = new PriorityQueue<Point>();
			for (Point p : people) {
				if(p.flag) {
					p.dist = Math.abs(p.r-stair.get(0).r)+ Math.abs(p.c-stair.get(0).c)+1; //계단까지 거리 +입구 대기시간
					group1.offer(p);
				}else {
					p.dist = Math.abs(p.r-stair.get(1).r)+ Math.abs(p.c-stair.get(1).c)+1;
					group2.offer(p);
				}
			}

			//2.계단 내려가기
			int time = goDown();
			
			if(time<MIN) {
				MIN = time;
			}
			
			return;
		}
		
		//idx가 그룹1
		people.get(idx).flag = true;
		grouping(idx+1);
		//idx가 그룹2
		people.get(idx).flag = false;
		grouping(idx+1);
		
	}
	private static int goDown() {
		int time=0;

		int[] capacity = new int[1000];	//n분에 계단을 내려가고 있는 사람의 수
		int lenOfStair = stair.get(0).dist;
		while(!group1.isEmpty()) {
			Point p = group1.poll();
			if(capacity[p.dist+1]<3) {	//계단 내려갈 수 있으면
				int cnt=0;
				while(cnt<lenOfStair) {
					capacity[p.dist+1+cnt]++;
					cnt++;
				}
				time = Math.max(time, p.dist+lenOfStair);
			}else {	//계단이 꽉차 있으면
				//대기시간 1분 늘려준 다음 다시 큐에 넣기
				p.dist++;
				group1.offer(p);
			}
		}//while
		
		capacity = new int[1000];
		lenOfStair = stair.get(1).dist;
		while(!group2.isEmpty()) {
			Point p = group2.poll();
			if(capacity[p.dist+1]<3) {	//계단 내려갈 수 있으면
				int cnt=0;
				while(cnt<lenOfStair) {
					capacity[p.dist+1+cnt]++;
					cnt++;
				}
				time = Math.max(time, p.dist+lenOfStair);
			}else {	//계단이 꽉차 있으면
				p.dist++;
				group2.offer(p);
			}
		}//while
		return time;
	}//go down

}
