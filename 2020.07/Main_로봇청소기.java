import java.util.*;
import java.awt.Point;
import java.io.*;

/**
 * 백준 4991 로봇청소기
 * 
 * 순열 + bfs -> 시간초과
 * 각방사이의 거리 최솟값 구하기 + 순열
 */
public class Main_로봇청소기 {
	static int R,C, N, MIN, adjMap[][], seq[];
	static char map[][];
	static ArrayList<Point> dirtyRoom;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		while(true) {
			tk = new StringTokenizer(br.readLine());
			C = Integer.parseInt(tk.nextToken());
			R = Integer.parseInt(tk.nextToken());
			if(C+R==0) break;	//종료
			
			map = new char[R+2][C+2];
			dirtyRoom = new ArrayList<Point>();		//더러운 방 위치
			N =0;									//더러운 칸 수
			MIN = 9999999;
			Point startPoint = new Point(0,0,0);
			
			for(int i=0; i<R+2; i++) {
				Arrays.fill(map[i], 'x');
				if(i==0 || i==R+1) continue;
				String line = br.readLine();
				for(int j=1; j<=C; j++) {
					char el = line.charAt(j-1);
					if(el == 'o') {
						startPoint = new Point(i,j,0);
						el = '0';
					}else if(el == '*') {
						dirtyRoom.add(new Point(i,j,0));
						el = (char)('1'+N);
						N++;
					}
					map[i][j] = el;
				}
			}//input
			
			dirtyRoom.add(0, startPoint);
			adjMap = new int[N+1][N+1];					//더러운 방 사이의 거리
			boolean flag = true;
			
			////1.장소사이 거리의 최솟값 미리 구하기
			for(int i=0; i<=N; i++) {
				for(int j=0; j<=N; j++) {
					int dist = minDistance(i,j);
					if(dist<0) flag = false;
					adjMap[i][j] = dist;
				}
			}
			
			//방사이가 연결 되어 있지 않으면 -1 출력
			if(!flag) {
				System.out.println(-1);
				continue;
			}
			
			////2.치울 방 순서 정하기
			seq = new int[N];
			for(int i=0; i<N; i++) {
				seq[i] = i+1;
			}
			
			do {
				////3.순서대로 가보기
				int sum = adjMap[0][seq[0]];
				for (int i=0; i<N-1; i++) {
					int from = seq[i];
					int to = seq[i+1];
					sum += adjMap[from][to];
				}
				
				////4.최소값 찾기
				MIN = Math.min(MIN, sum);
			}while(np());

			//output
			System.out.println(MIN);
			
		}//while tc
	}//main
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static int minDistance(int from, int to) {
		Queue<Point> que = new LinkedList<Point>();
		boolean[][] visited = new boolean[R+2][C+2];
		
		Point fp = dirtyRoom.get(from);
		que.offer(fp);
		visited[fp.r][fp.c] = true;
		
		while(!que.isEmpty()) {
			Point p = que.poll();
			int r = p.r;
			int c = p.c;
			int level = p.level;
			
			// 다음 치워야 할 차례의 칸에 도착함
			if(map[r][c] == (char)(to+'0')) {
				return level;
			}
			
			for(int d=0; d<4; d++) {
				int nr = r+dR[d];
				int nc = c+dC[d];
				
				if(map[nr][nc]=='x' || visited[nr][nc]) continue;
				
				visited[nr][nc] = true;
				que.offer(new Point(nr, nc, level+1));
			}
		}//while
		
		//다음 칸에 못감
		return -1;
	}//bfs
	
	private static boolean np(){
		int i=N-1;
		while(true) {
			if(i<=0) return false;
			if(seq[i-1] < seq[i]) break;
			i--;
		}
		
		int j=N-1;
		while(true) {
			if(seq[i-1] < seq[j]) break;
			j--;
		}
		swap(i-1,j);
		
		j = N-1;
		while(i<j) {
			swap(i,j);
			i++;
			j--;
		}
		
		return true;
	}//np
	
	static private void swap(int i, int j) {
		int temp = seq[i];
		seq[i] = seq[j];
		seq[j] = temp;
	}
	
	static class Point{
		int r, c;
		int level;
		Point(int r, int c, int level){
			this.r = r;
			this.c = c;
			this.level = level;
		}
	}//Point

}
