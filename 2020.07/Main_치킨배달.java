import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * 백준 15686 치킨배달
 */
public class Main_치킨배달 {
	
	static class Point{
		int r, c;
		Point(int r, int c){
			this.r = r;
			this.c = c;
		}
	}
	static int N, M, map[][], MIN;
	static ArrayList<Point> houses, chickens;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());	//맵크기: ~50
		M = Integer.parseInt(tk.nextToken());	//살릴 치킨집 수: ~13
		map = new int[N+2][N+2];
		houses = new ArrayList<Point>();
		chickens = new ArrayList<Point>();
		
		for(int i=0; i<N+2; i++) {
			Arrays.fill(map[i], -1);
			if(i==0 || i==N+1) continue;
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				int el = Integer.parseInt(tk.nextToken());
				map[i][j] = el;
				if(el == 1) {
					houses.add(new Point(i,j));		//집 리스트
				}else if(el == 2) {
					chickens.add(new Point(i,j));	//치킨집 리스트
				}
			}
		}//input
		
		visited = new boolean[chickens.size()];
		MIN = 9999999;
		//치킨집 M개 조합
		combination(0, 0, chickens.size());
		
		//output
		System.out.println(MIN);
		
		
	}//main
	
	public static boolean visited[];
	private static void combination(int idx, int cnt, int len) {
		//치킨집 M개 골랐을때 도시의 치킨거리 찾기
		if(cnt == M) {
			MIN = Math.min(MIN, chickenDist());
		}
		
		if(cnt > len) return;
		
		for(int i=idx; i<len; i++) {
			visited[i] = true;
			combination(i+1, cnt+1, len);
			visited[i] = false;
		}
	}//comb
	
	//치킨거리구하기
	private static int chickenDist() {
		int sum=0;
		for(int i=0, size = houses.size(); i<size; i++) {
			int min = 999999;
			for(int j=0, msize = chickens.size(); j<msize; j++) {
				if(visited[j]) {
					Point h = houses.get(i);
					Point ch = chickens.get(j);
					int dist = Math.abs(h.r-ch.r)+Math.abs(h.c-ch.c);
					min = Math.min(min, dist);	//치킨거리
				}
			}
			sum += min;	//도시의 치킨 거리
		}
		return sum;
	}

}
