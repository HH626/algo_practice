import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * SWEA 1868 [D4] 파핑파핑 지뢰찾기
 */
public class Solution_파핑파핑지뢰찾기 {
	static int N;
	static char map[][];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			N = Integer.parseInt(br.readLine());
			map = new char[N][N];
			
			for(int i=0; i<N; i++) {
				String line = br.readLine();
				for(int j=0; j<N; j++) {
					map[i][j] = line.charAt(j);
				}
			}///input
			
			//1. 숫자로 다 표시하기
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					if(map[i][j]=='.') {
						map[i][j] = (char)(check(i,j)+'0');
					}
				}
			}
			
			int cnt=0;
			//2. 0인 칸 없애기
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					if(map[i][j]=='0') {
						bfs(i,j);
						cnt++;
					}
				}
			}
			
			//3. 나머지 칸 세기
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					if(map[i][j]>'0' && map[i][j]<'9') {
						cnt++;
					}
				}
			}
			
			
			///output
			System.out.println("#"+tc+" "+cnt);
			
			
		}//tc
	}//main

	static int[] dR = {-1,-1,0,1,1,1,0,-1};
	static int[] dC = {0,1,1,1,0,-1,-1,-1};
	private static int check(int r, int c) {
		int cnt=0;
		for(int d=0; d<8; d++) {
			int nr = r+dR[d];
			int nc = c+dC[d];
			if(nr<0 || nr>=N || nc<0 || nc>=N) continue;
			if(map[nr][nc] == '*') {
				cnt++;
			}
		}
		
		return cnt;
	}//check
	
	private static void bfs(int r, int c) {
		Queue<int[]> que = new LinkedList<int[]>();
		boolean visited[][] = new boolean[N][N];
		
		que.offer(new int[] {r,c});
		visited[r][c] = true;
		map[r][c] = '.';
		
		while(!que.isEmpty()) {
			int[] a = que.poll();
			r = a[0];
			c = a[1];
			for(int d=0; d<8; d++) {
				int nr = r+dR[d];
				int nc = c+dC[d];
				if(nr<0 || nr>=N || nc<0 || nc>=N || visited[nr][nc]) continue;
				
				if(map[nr][nc] == '0') {
					que.offer(new int[] {nr, nc});
				}
				map[nr][nc] = '.';
				visited[r][c] = true;
			}
		}
		
	}//marking
	
}
