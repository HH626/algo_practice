import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;
/**
 * 백준 18809 Gaaaaaaaaaarden
 */
public class Main_garden {
	static class Point{
		int r,c,color,level;
		Point(int r, int c, int color, int level){
			this.r = r;
			this.c = c;
			this.color = color;
			this.level = level;
		}
		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + ", color=" + color + ", level=" + level + "]";
		}
		
	}
	public static int R, C, Green, Red, map[][], N, MAX;
	public static ArrayList<Point> spotList;
	public static int flowerMap[][][], chosenSpot[];
	public final static int GREEN = 0, RED=1, Flower=2;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());		//~50
		C = Integer.parseInt(tk.nextToken());		//~50
		Green = Integer.parseInt(tk.nextToken());	//~5
		Red = Integer.parseInt(tk.nextToken());		//~5
		int total = Green + Red;
		
		spotList = new ArrayList<Point>();
		
		
		map = new int[R+2][C+2];
		for(int i=1; i<=R; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=C; j++) {
				int el = Integer.parseInt(tk.nextToken());
				map[i][j] = el;
				if(el == 2) {
					spotList.add(new Point(i,j,-1,1));
				}
			}
		}//input
		
		
		N = spotList.size();	//배양액 뿌릴수 있는 장소 크기
		chosenSpot = new int[N];
		for(int i=N-1, cnt=0; cnt<total; i--, cnt++) {
			chosenSpot[i] = 1;
		}
		
		
		MAX=0;
		//1.배양액 뿌릴곳 선정 : chosenSpot[n]==1인 곳이 배양액 뿌릴 곳;
		do {
			//2.어느 배양액 뿌릴지 정하기  초록:chosenSpot[n]==-2, 빨강:chosenSpot[n]==-1
			choose(0, Green, Red);
			
		}while(np(N,chosenSpot));
		
		
		
		//output
		System.out.println(MAX);
		
	}//main
	
	private static void choose(int idx, int green, int red) {
		if(green<0 || red<0) {
			return;
		}
		
		if(idx>=N) {
			if(green==0 && red == 0) {
				//3.뿌리기
//				System.out.println(Arrays.toString(chosenSpot));
				flowerMap = new int[R+2][C+2][3];
				int result = spread();
//				System.out.println(result);
				MAX = Math.max(MAX, result);
			}
			return;
		}
		
		if(chosenSpot[idx] == 1) { //배양액 뿌려야 할 곳이면
			//초록
			chosenSpot[idx] =-2;
			choose(idx+1, green-1, red);
			
			//빨강
			chosenSpot[idx] = -1;
			choose(idx+1, green, red-1);
			
			chosenSpot[idx] = 1;
		}else {	//뿌릴곳이 아니면
			choose(idx+1, green, red);
		}
	}

	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static int spread() {
		Queue<Point> que = new LinkedList<Point>();
		int flowerCnt = 0;
		
		for(int i=0; i<N; i++) {
			if(chosenSpot[i]<0) {
				Point p = spotList.get(i);
				p.color = chosenSpot[i]+2;
				flowerMap[p.r][p.c][p.color]=1;
				que.offer(p);
			}
		}
		
		
		while(!que.isEmpty()) {
			Point p = que.poll();
			
			//꽃이 된 자리면
			if(flowerMap[p.r][p.c][Flower]==1) continue; 
			
			int theOthercolor = (p.color+1)%2;
			for(int d=0; d<4; d++) {
				int nr = p.r + dR[d];
				int nc = p.c + dC[d];
				//호수일때
				if(map[nr][nc]==0) continue;
				
				//꽃일때
				if(flowerMap[nr][nc][Flower]==1) continue; 
				
				//같은색 배양액 이미 차있을때
				if(flowerMap[nr][nc][p.color] !=0) continue;
				
				flowerMap[nr][nc][p.color] = p.level+1;
				//꽃 필수 있을때
				if(flowerMap[nr][nc][p.color] == flowerMap[nr][nc][theOthercolor]) {
					flowerMap[nr][nc][Flower]=1;
					flowerCnt++;
				}else{
					que.offer(new Point(nr,nc,p.color,p.level+1));
				}
			}//for
		}//while
		
		return flowerCnt;
	}//spread
	

	private static boolean np(int N, int[] arr) {
		
		int i = N-1;
		while(i > 0 && arr[i-1] >= arr[i]) {
			i--;
		}
			
		if(i == 0) return false;	//
			
		int j = N-1;
		while(arr[i-1] >= arr[j]) {
			j--;
		}
			
		swap(i-1 , j, arr);
			
		j = N-1;
		while(i < j) {
			swap(i++ , j--, arr);
		}
	    
		return true;
	}
	
	private static void swap(int i, int j, int[] a) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}//swap

}
