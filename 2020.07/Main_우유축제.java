import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * 백준 14720 우유 축제
 * 
 */
public class Main_우유축제 {

	static int N;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		N = Integer.parseInt(br.readLine());
		
		int arr[] = new int[N];
		tk = new StringTokenizer(br.readLine());
		for(int i=0;i<N; i++) {
			arr[i] = Integer.parseInt(tk.nextToken());
		}//input
		
		int milk = 0;
		int cnt=0;
		for(int i=0; i<N ;i++) {
			if(arr[i]==milk) {
				cnt++;
				milk = (milk+1)%3;
			}
		}
		
		
		System.out.println(cnt);
	}//main
	

}
