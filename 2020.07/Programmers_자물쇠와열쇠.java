/**
 * Programmers 자물쇠와 열쇠
 * 2020 KAKAO BLIND RECRUITMENT
 */
public class Programmers_자물쇠와열쇠 {

	public static void main(String[] args) {
//		int[][] key = {{0,0,1},{0,1,1},{0,1,0}};
//		int[][] lock = {{1,1,1},{1,1,0},{1,0,1}};
		int[][] key = {{0,0,0},{1,0,0},{0,1,1}};
		int[][] lock = {{1,1,1},{1,1,0},{1,0,1}};
		
		Solution s = new Solution();
		System.out.println(s.solution(key, lock));
	}

}

class Solution {
    public boolean solution(int[][] key, int[][] lock) {
        boolean answer = true;
        
        //4번 회전동안
        for(int i=0; i<4; i++) {
	        //1. key의 (0,0)을 평행이동
	        answer = move(key, lock);
	        if(answer) return answer;
	        //3. key회전
	        key = rotate(key);
        }
        return answer;
    }//solution
    
    public int[][] rotate(int[][] key) {
    	int K = key.length;
    	int tmp[][] = new int[K][K];
    	
    	for(int j=0; j<K; j++) {
    		int[] a = new int[K];
    		for(int i=0; i<K; i++) {
    			a[i] = key[i][j]; 
    		}
    		tmp[K-1-j] = a;
    	}
    	
    	return tmp;
    }//rotate
    
    public boolean move(int[][] key, int[][] lock) {
    	//키를 이동시키면서 확인
    	//키가 자물쇠의 빈칸을 다 메우면 true, 못메우면 false 리턴
    	
    	int L = lock[0].length;
    	int K = key[0].length;
        for(int rowOffset = -K+1; rowOffset<L; rowOffset++) {
	        for(int colOffset=-K+1; colOffset<L ; colOffset++) {
	        	//2.자물쇠 구멍에 키가 일치하는지 확인
	        	 boolean result = match(key, lock, rowOffset,colOffset);
	        	 if(result) return true;
	        }
        }
        
        return false;
    }//move
    
    public boolean match(int[][] key, int[][] lock, int rowOffset, int colOffset) {
    	//키가 자물쇠의 빈칸을 다 메우면 true, 못메우면 false 리턴
    	int L = lock[0].length;
    	int K = key[0].length;
    	
    	for(int i=0; i<L ; i++) {
    		for(int j=0; j<L; j++ ) {
    			
    			int r = i-rowOffset;
    			int c = j-colOffset;
    			
    			//1) key랑 자물쇠가 안겹치는 부분
    			if(r<0 || r>=K || c<0 || c>=K) {
    				if(lock[i][j] == 0) return false;
    			}else{ //2) key랑 자물쇠가 겹치는 부분
    				if(lock[i][j] + key[i-rowOffset][j-colOffset] == 0
    					|| lock[i][j] + key[i-rowOffset][j-colOffset] == 2) return false;
    			}
    			
    		}
    	}
        return true;
    }//match
}