import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;
/**
 * SWEA 4014 [모의] 활주로 건설
 */
public class Solution_활주로건설 {
	static int N,X, map[][];
	static boolean visited[][], row[], col[];
	static final int UP = 0, DOWN = 1, LEFT = 2, RIGHT=3;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());		//맵크기: 6~20
			X = Integer.parseInt(tk.nextToken());		//경사로 길이: 2~4
			
			map = new int[N][N];
			visited = new boolean[N][N];
			row = new boolean[N];
			col = new boolean[N];
			Arrays.fill(row, true);
			Arrays.fill(col, true);
			
			for(int i=0; i<N; i++) {
				tk= new StringTokenizer(br.readLine());
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(tk.nextToken());
				}
			}//input
			
			////1. <->방향 확인
			checkLow();
			
			visited = new boolean[N][N];
			////2.위아래 방향 확인
			checkCol();
			
			////3.가능한 길 세기
			int cnt=0;
			for(int i=0; i<N; i++) {
				if(row[i]) cnt++;
				if(col[i]) cnt++;
			}

			//output
			System.out.println("#"+tc+" "+cnt);
		}//tc
	}//main
	
	private static void checkLow() {		
		for(int i=0; i<N; i++) {
			for(int j=1; j<N; j++) {
				//1.차이가 1보다 클때
				if(Math.abs(map[i][j-1]-map[i][j]) > 1) {
					row[i] = false;
				}
				//2. 오르막
				else if(map[i][j-1]+1 == map[i][j]) {
					int result = slope(i,j-1,LEFT);
					if(result<0) row[i] = false;
				}
				//3. 내리막
				else if(map[i][j-1]-1 == map[i][j]) {
					int result = slope(i,j,RIGHT);
					if(result<0) row[i] = false;
					else j = j+X-1;
				}
				//4. 놓을 수 없음
				if(row[i] == false) break;
			}
		}
	}
	
	private static void checkCol() {		
		for(int j=0; j<N; j++) {
			for(int i=1; i<N; i++) {
				//1.차이가 1보다 클때
				if(Math.abs(map[i-1][j]-map[i][j]) > 1) {
					col[j] = false;
				}
				//2. 오르막
				else if(map[i-1][j]+1 == map[i][j]) {
					int result = slope(i-1,j,UP);
					if(result<0) col[j] = false;
				}
				//3. 내리막
				else if(map[i-1][j]-1 == map[i][j]) {
					int result = slope(i,j,DOWN);
					if(result<0) col[j] = false;
					else i = i+X-1;
				}
				//4. 놓을 수 없음
				if(col[j] == false) break;
			}
		}
	}
	
	static int[] dR = {-1,1,0,0}; //상하좌우
	static int[] dC = {0,0,-1,1};
	//해당 자리부터 x만큼 떨어져 있는 자리까지 길이 평평한지 확인
	//평평하지 않거나 중간에 경사로가 있으면 -1
	//놓을 수 있으면 경사로 길이 리턴
	private static int slope(int r, int c, int d) {
		//경사로 놓을 수 있는지 확인
		for(int x=0; x<X; x++) {
			int nr = r+dR[d]*x;
			int nc = c+dC[d]*x;
			if(nr<0 || nr>=N || nc<0 || nc>=N || visited[nr][nc]) {
				return -1;
			}
			if(map[r][c] != map[nr][nc]) return -1;	
		}
		//경사로 놓을 수 있다면 visited true;
		for(int x=0; x<X; x++) {
			int nr = r+dR[d]*x;
			int nc = c+dC[d]*x;
			visited[nr][nc] = true;
		}
		
		return X;
	}//slope

}
