/**
 * Programmers 문자열 압축
 * 2020 KAKAO BLIND RECRUITMENT
 */
public class Programmers_문자열압축 {

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.solution("aabbaccc"));
		System.out.println(s.solution("ababcdcdababcdcd"));
		System.out.println(s.solution("abcabcdede"));
		System.out.println(s.solution("abcabcabcabcdededededede"));
		System.out.println(s.solution("xababcdcdababcdcd"));
		System.out.println(s.solution("a"));
	}

}

class Solution {
    public int solution(String s) {
    	int N = s.length();
        int answer = N;
        //자를 길이 정하기
        for(int n=1; n<=N/2; n++) {
        	StringBuilder sb = new StringBuilder();
        	
        	//단어 자르기
        	int i=0;
        	while(i<N/n) {
        		String piece = s.substring(i*n,(i+1)*n);
        		
        		//비교하기
        		int cnt=1;
        		while(i+cnt+1<=N/n) {
        			String comp = s.substring((i+cnt)*n,(i+cnt+1)*n);
        			if(!comp.equals(piece)) break;
        			cnt++;
        		}
        		
        		//같은 단어가 있으면
        		if(cnt>=2) {
        			sb.append(cnt).append(piece);
        			i += cnt-1;
        		}else {	//없으면
        			sb.append(piece);
        		}
        		i++;
        	}//while
        	
        	//나머지 알파벳 append
        	sb.append(s.substring(i*n,N));
        	answer = Math.min(answer, sb.length());
        }
        
        
        return answer;
    }
}