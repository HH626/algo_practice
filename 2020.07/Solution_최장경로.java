import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;
/**
 * SWEA 2814 최장경로 D3
 */
public class Solution_최장경로 {

	public static int V, E, MAX;
	public static ArrayList<Integer>[] adjList;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			tk = new StringTokenizer(br.readLine());
			V = Integer.parseInt(tk.nextToken()); //~10
			E = Integer.parseInt(tk.nextToken()); //~20
			
			MAX=0;
			adjList = new ArrayList[V+1];
			visited = new boolean[V+1];
			for(int i=1; i<=V; i++) {
				adjList[i] = new ArrayList<Integer>();
			}
			
			for(int i=0; i<E; i++) {
				tk = new StringTokenizer(br.readLine());
				int x = Integer.parseInt(tk.nextToken());
				int y = Integer.parseInt(tk.nextToken());
				adjList[x].add(y);
				adjList[y].add(x);
			}
			
			for(int i=1; i<=V; i++) {
				visited[i] = true;
				dfs(i, 1);
				visited[i] = false;
			}
				
			//ouput
			System.out.println("#"+tc+" "+MAX);
		}//tc

	}//main

	static boolean[] visited;
	private static void dfs(int v, int cnt) {
		for (int i : adjList[v]) {
			if(visited[i]) continue;
			visited[i] = true;
			dfs(i, cnt+1);
			visited[i] = false;
		}
		
		if(cnt>MAX) {
			MAX = cnt;
		}
		
	}

}
