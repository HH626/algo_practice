import java.util.*;
import java.io.*;
/**
 * SWEA 1767 프로세서 연결하기
 * 
 * dfs -> 시간 초과
 * 백트래킹
 */
public class Solution_프로세서연결하기 {

	static int N, map[][], numOfCore, MAX, answer;
	static ArrayList<Point> corelist;
	static boolean visited[][], connected[];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			N = Integer.parseInt(br.readLine());	//map크기: 7~12
			
			map = new int[N+2][N+2];
			visited = new boolean[N+2][N+2];		//전선 놓은곳 true
			corelist = new ArrayList<Point>();		//core위치
			numOfCore = 0;							//core가 몇개인지: 1~12

			corelist.add(new Point(0,0));
			for(int i=1; i<=N; i++) {
				tk = new StringTokenizer(br.readLine());
				for(int j=1; j<=N; j++) {
					int el = Integer.parseInt(tk.nextToken());
					if(el==1) {
						numOfCore++;
						el = numOfCore;
						corelist.add(new Point(i,j));
					}
					map[i][j] = el;
				}
			}
			
			connected = new boolean[numOfCore+1];		//전원과 연결된 core 표시
			MAX = 0;								//core최대 연결 표시
			answer = 999999999;
			//in
			
			/////
			dfs(1,0,0);
			
			//out
			System.out.println("#"+tc+" "+answer);
		}//tc
	}//main
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static void dfs(int coreNo, int cnt, int sum) {
		////더 가도 cnt값이 MAX보다 작으면 return; ***이거 안하면 시간초과
		if(numOfCore-coreNo+1+cnt<MAX) {
			return;
		}
		
		////
		if(coreNo>numOfCore) {
			if(cnt>MAX) {
				MAX = cnt;
				answer = sum;
			}else if(cnt==MAX) {
				answer = Math.min(answer, sum);
			}
			return;
		}
		
		
		////
		Point p = corelist.get(coreNo);
		int r = p.r;
		int c = p.c;
		for(int d=0; d<4; d++) {
			int i=0;
			while(true) {
				i++;
				int nr = r+dR[d] * i;
				int nc = c+dC[d] * i;
				
				// 전원에 연결 될때
				if(nr<1 || nr>N || nc<1 || nc>N) {
					//전선 깔기
					int length = connect(true, r, c, d)-1;
					connected[coreNo] = true;
					dfs(coreNo+1, cnt+1, sum+length);
					
					//전선 없애기
					connect(false, r, c, d);
					break;
				}
				// 다른 전선이 이미있거나, 코어가 있을때
				if(visited[nr][nc] || map[nr][nc]>0) {
					connected[coreNo] = false;
					dfs(coreNo+1, cnt, sum);
					break;
				}
				
			}//while
			

		}
	}//dfs
	
	private static int connect(boolean flag, int r, int c, int d) {
		int i=0;
		while(true) {
			i++;
			int nr = r+dR[d] * i;
			int nc = c+dC[d] * i;
			if(nr<1 || nr>N || nc<1 || nc>N) {
				break;
			}
			visited[nr][nc] = flag;
		}
		return i;
	}//connect

	static class Point{
		int r,c;
		Point(int r, int c){
			this.r = r;
			this.c = c;
		}
	}

}
