import java.util.Arrays;
import java.util.Scanner;

/**
 * 백준 2750 수정렬하기
 * 
 * O(n^2) 정렬
 * - bubble sort
 * - 삽입 정렬
 * - 선택 정렬
 */
public class Main_수정렬하기 {
	static int N, arr[];
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		arr = new int[N];
		for(int i=0; i<N; i++) {
			arr[i] = sc.nextInt();
		}//input
		
		//1. insertion sort
		insertion();
		
		//2. selection sort
		selection();

		///3.bubble sort
		bubble();
		
		//output
		for (int a : arr) {
			System.out.println(a);
		}
	}//main
	
	private static void selection() {
		for(int i=0; i<N-1; i++) {
			int minIdx = i;
			for(int j=i+1; j<N; j++) {
				if(arr[j]<arr[minIdx]) minIdx = j;
			}
			swap(i,minIdx);
//			System.out.println(Arrays.toString(arr));
		}
	}
	
	private static void insertion() {
		for(int i=1; i<N; i++) {
			int j=i;
			int tmp = arr[i];
			
			//tmp보다 작은 값이 나올때 까지 arr[j]를 오른쪽으로 한칸씩 밀기
			while(j>0) {
				if(arr[j-1]<tmp) break;
				
				arr[j] = arr[j-1];
				j--;
			}
			arr[j] = tmp;
//			System.out.println(Arrays.toString(arr));
		}
	}

	private static void bubble() {
		for(int i=0; i<N; i++) {
			for(int j=1; j<N-i; j++) {
				if(arr[j-1]>arr[j]) swap(j-1,j);
//				System.out.println(Arrays.toString(arr));
			}
		}
	}//bubble
	
	private static void swap(int i, int j) {
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}

}
