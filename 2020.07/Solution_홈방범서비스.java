import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * SWEA 2117 [모의] 홈 방범 서비스
 */
public class Solution_홈방범서비스 {
	static class Point{
		int r,c;
		Point(int r, int c ){
			this.r = r;
			this.c = c;
		}
	}
	static int N, M, map[][], cost[][][], MAX;
	static int[] operCost;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		operCost = new int[22];
		for(int i=1; i<22; i++) {
			operCost[i] = i*i+(i-1)*(i-1);
		}		
		int T = Integer.parseInt(br.readLine());
		////tc
		for(int tc = 1; tc<=T ;tc++) {
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());
			M = Integer.parseInt(tk.nextToken());
			
			MAX = 0;
			map = new int[N][N];
			cost = new int[N][N][N+2];
			
			for(int i=0; i<N; i++) {
				tk = new StringTokenizer(br.readLine());
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(tk.nextToken());
				}
			}//input
			
			//1.모든칸 돌면서
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					//2.K가 1~N일때 가격 구하기
					bfs(i,j);
				}
			}
			
			//ouput
			System.out.println("#"+tc+" "+MAX);
		}//tc
		
	}//main
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static void bfs(int r, int c) {
		Queue<Point> que = new LinkedList<Point>();
		boolean[][] visited = new boolean[N][N];
		int level=1, sum =0;
		
		visited[r][c] = true;
		que.offer(new Point(r,c));
		
		while(!que.isEmpty() && level<N+2) {
			int levelSize = que.size();
			
			while(levelSize-- > 0) {
				Point p = que.poll();
				//지금 범위에 집 있으면  sum++;
				if(map[p.r][p.c]==1) {
					sum++;
				}
				
				//다음 범위에 있을 범위 큐에 넣기
				for(int d=0; d<4; d++) {
					int nr = p.r+dR[d];
					int nc = p.c+dC[d];
					if(nr<0 || nr>=N || nc<0 || nc>=N || visited[nr][nc]) continue; //범위 초과 || 이미 방문
				
					visited[nr][nc] = true;
					que.offer(new Point(nr,nc));
				}
				
			}//level
			
			cost[r][c][level]=sum;
			//손해 안볼때
			if(sum*M >= operCost[level]) {
				MAX = Math.max(MAX, sum);
			}
			
			level++;
		}//que
	}//bfs

}
