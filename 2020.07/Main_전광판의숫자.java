import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * 백준 16519 전광판의 숫자
 */
public class Main_전광판의숫자 {

	static int N, number[][], arr[];
	static char map[][];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		map = new char[7][];
		for(int i=0; i<7; i++) {
			map[i] = br.readLine().toCharArray();
		}
		
		N = map[0].length/6;
		arr = new int[N];
		////1.숫자판 만들기
		makeNumberCard();
		
		////2.input 숫자 알아내기
		inputNumber();
		
		////3.input 다음 순서 숫자 찾기
		if(np()) {		
			////4.전광판 표시하기
			int answer[][];
			answer = outputNumber();
			
			for(int i=0; i<7; i++) {
				for(int j=0; j<6*N; j++) {
					System.out.print(answer[i][j]);
				}
				System.out.println();
			}
		////3.1. 다음 숫자가 없음	
		}else {
			System.out.println("The End");
		}
		
	}//main
	
	private static int[] inputNumber() {
		int n=0;
		while(n<N) {
			int cnt = 0;
			for(int i=0; i<7; i++) {
				for(int j=0; j<6; j++) {
					if(map[i][j+n*6] == '1') cnt++;
				}
			}
			
			arr[n] = getNumber(cnt);
			n++;
		}//while
		
		return arr;
	}//inputNum
	
	private static int[][] outputNumber() {
		int answer[][] = new int[7][N*6];
		int n=0;
		while(n<N) {
			int num = arr[n];
			int[] tmp = number[num];
			for(int i=0; i<tmp.length; i++) {
				int idx = tmp[i];
				int r = idx/6;
				int c = idx%6;
				answer[r][c+n*6] = 1;
			}
			n++;
		}//while
		
		return answer;
	}//outNum
	
	private static boolean np() {
		int i = N-1;
		while(i>0 && arr[i-1] >= arr[i]) {
			i--;
		}
		
		if(i<=0) return false;
		
		int j=N-1;
		while(arr[i-1] >= arr[j]) {
			j--;
		}
		
		swap(i-1, j);
		
		j = N-1;
		while(i<j) {
			swap(i,j);
			i++;
			j--;
		}
		
		return true;
	}
	
	private static void swap(int i, int j) {
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}
	
	//켜져있는 전구 수를 보고 어떤 숫자인지 리턴
	private static int getNumber(int n) {
		switch(n) {
		case 10:
			return 0;
		case 6:
			return 1;
		case 14:
			return 2;
		case 9:
			return 3;
		case 11:
			return 4;
		case 13:
			return 5;
		case 12:
			return 6;
		case 8:
			return 7;
		case 16:
			return 8;
		case 15:
			return 9;
		}
		return -1;
	}
	
	//number[n][cnt] = n이라는 숫자에 켜져있는 전구, cnt: 켜져있는 전구수
	private static void makeNumberCard() {
		number = new int[10][];
		number[0] = new int[] {8,9,13,16,19,22,25,28,32,33};				//10
		number[1] = new int[] {9,14,15,21,27,33};							//6
		number[2] = new int[] {7,8,9,10,16,19,20,21,22,25,31,32,33,34};		//14
		number[3] = new int[] {7,8,9,16,21,28,31,32,33};					//9
		number[4] = new int[] {9,14,15,19,21,24,25,26,27,28,33};			//11
		number[5] = new int[] {7,8,9,10,13,19,20,21,28,31,34,38,39};		//13
		number[6] = new int[] {7,13,19,20,21,22,25,28,31,32,33,34};			//12
		number[7] = new int[] {7,8,9,10,16,21,27,33};						//8
		number[8] = new int[] {7,8,9,10,13,16,19,20,21,22,25,28,31,32,33,34};//16
		number[9] = new int[] {1,2,3,4,7,10,13,16,19,20,21,22,28,34,40};	//15
	}

}
