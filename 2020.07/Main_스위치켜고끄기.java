import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 1244 스위치 켜고 끄기
 */
public class Main_스위치켜고끄기 {
	static int N,M, light[], people[][];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		N = Integer.parseInt(br.readLine());	//스위치 수
		light = new int[N+1];					//스위치 배열
		
		tk = new StringTokenizer(br.readLine());
		for(int i=1; i<=N; i++) {
			light[i] = Integer.parseInt(tk.nextToken());
		}	
		
		M = Integer.parseInt(br.readLine());	//사람수
		people = new int[M+1][2];
		
		for(int i=1; i<=M; i++) {
			tk = new StringTokenizer(br.readLine());
			int gender = Integer.parseInt(tk.nextToken());
			int index = Integer.parseInt(tk.nextToken());
			
			
			/////
			go(gender, index);
		}	
		
		for(int i=1; i<=N; i++) {
			System.out.print(light[i]+" ");
			if(i%20==0) System.out.println();
		}
	}//main
	
	private static void go(int gender, int index) {
		if(gender==1) boy(index);
		else if(gender ==2) girl(index);
		
	}
	
	private static void boy(int index) {
		int cnt=1;
		int n = index;
		do{
			swit(n);
			cnt++;
			n = index*cnt;
		}while(n<=N);
	}
	private static void girl(int index) {
		int left=index, right=index;
		swit(index);
		while(left>1 &&right<N) {
			left--;
			right++;
			if(light[left]==light[right]) {
				swit(left);
				swit(right);
			}else {
				break;
			}
		}
	}
	
	private static void swit(int idx) {
		if(light[idx]==0) light[idx] = 1;
		else light[idx] = 0;
	}

}
