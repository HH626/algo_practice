import java.util.Scanner;

/**
 * 백준 14613 너의 티어는?
 */
public class Main_너의티어는 {

	static double WIN, LOSE, DRAW;
	static double[] tier;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		WIN = sc.nextDouble();
		LOSE = sc.nextDouble();
		DRAW = sc.nextDouble();
		
		tier = new double[5];
		
		for(int w=0; w<=20; w++) {
			for(int l=0; l<=20-w; l++) {
				int d=20-w-l;
				//WIN이 w개 LOSE가 l개 DRAW가 d개 나올 확률 구하기
				double prob = Math.pow(WIN, w) * Math.pow(LOSE,l) * Math.pow(DRAW, d);
				//WIN이 w개 LOSE가 l개 DRAW가 d개로 만들수 있는 순열 경우의수
				long comb = combination(20,w) * combination(20-w, l) * combination(20-w-l,d);
				calc(w-l, prob*comb);
			}
		}
		
		for(int i=0; i<5; i++) {
			System.out.printf("%.8f\n", tier[i]);
		}
		
	}//main

	//티어별로 확률 값 넣기
	private static void calc(int n, double p) {
		//n:이긴수 - 진수
		if(n<-10) {					//브론즈
			tier[0] += p;
		}else if(-10<=n && n<0) {	//실버
			tier[1]+= p;
		}else if(0<=n && n<10) {	//골드
			tier[2] +=  p;
		}else if(10<=n && n<20) {	//프래티넘	
			tier[3] +=  p;
		}else if(20<=n) {			//다이아
			tier[4] +=  p;
		}
		
	}
	
	//순서없는 수열 경우의 수 구하기
	private static long combination(int n, int r) {
		long result = 1;
		for(int i=n; i>r; i--) {
			result = result*i;
		}
		for(int i=1; i<=(n-r); i++) {
			result = result/i;
		}
		return result;
	}
}
