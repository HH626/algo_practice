import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 1937 욕심쟁이 판다
 * N: ~500
 * bfs -> 메모리초과
 * dfs -> 시간초과
 * memo -> 시간초과
 * DP(LIS) top-down
 */
public class Main_욕심쟁이판다 {
	static int N, map[][], MAX, dp[][];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		N = Integer.parseInt(br.readLine()); //~500
		map = new int[N+2][N+2];
		dp = new int[N+2][N+2];
		MAX = 0;
		
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}//input
		
		////
		int answer = 0;
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				answer = Math.max(answer, dfs(i,j));
			}
		}
		
		//output
		System.out.println(answer);
	}//main
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	
	private static int dfs(int r, int c) {
		
		if(dp[r][c] !=0) return dp[r][c];
		
		dp[r][c] = 1;
		for(int d=0; d<4; d++) {
			int nr = r+dR[d];
			int nc = c+dC[d];
			if(map[nr][nc]>map[r][c]) {
				dp[r][c] = Math.max(dp[r][c], dfs(nr,nc)+1);
			}
		}
		return dp[r][c];
	}//dfs


}
