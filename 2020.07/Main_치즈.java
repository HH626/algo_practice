import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 2638 치즈
 */
public class Main_치즈 {
	static int R, C, map[][];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());

		map = new int[R][C];
		
		for(int i=0; i<R; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=0; j<C; j++) {
				map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		
		boolean flag = true;
		int cnt=0;
		while(flag) {
			cnt++;
			//1. 녹일 치즈 찾기
			bfs();
			//2. 녹이기
			flag = melt();
		}
		
		//output
		System.out.println(cnt-1);
	}
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static void bfs() {
		Queue<Point> que = new LinkedList<Point>();
		boolean visited[][] = new boolean[R][C];
		
		que.offer(new Point(0,0));
		visited[0][0] = true;
		
		while(!que.isEmpty()) {
			Point p = que.poll();
			int r = p.r;
			int c = p.c;
			for(int d=0; d<4; d++) {
				int nr = r+dR[d];
				int nc = c+dC[d];
				if(nr<0 ||  nr>=R || nc<0|| nc>=C ||visited[nr][nc]) continue;
				
				if(map[nr][nc]>0) {
					map[nr][nc]++;
				}else {
					visited[nr][nc] = true;
					que.offer(new Point(nr,nc));
				}
			}
		}//while
	}//bfs
	
	private static boolean melt() {
		boolean flag = false;	//녹인 치즈가 있으면  true 없으면  false
		
		for(int i=0; i<R; i++) {
			for(int j=0; j<C; j++) {
				
				if(map[i][j]>2) {			//1)녹일 치즈
					map[i][j] = 0;
					flag = true;
				}else if(map[i][j]>0) {		//2)안녹일 치즈
					map[i][j] = 1;
				}
			}
		}
		
		return flag;
	}
	
	static class Point{
		int r, c;
		Point(int r, int c){
			this.r = r;
			this.c = c;
		}
	}

}
