import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 * SWEA 10204 [D5] 초밥식사
 * 
 * 시간초과
 */
public class Solution_초밥식사 {
	static class Point implements Comparable<Point>{
		int a, b;
		Point(int a, int b){
			this.a = a;
			this.b = b;
		}
		public int compareTo(Point o) {
			if(o.a==this.a) {
				return o.b-this.b;
			}
			return o.a-this.a;
		}
		@Override
		public String toString() {
			return "Point [a=" + a + ", b=" + b + "]";
		};
	}
	static int N;
	static LinkedList<Point> list;
	static Point[] arr;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			N = Integer.parseInt(br.readLine());
			list = new LinkedList<Point>();
			
			for(int i=0; i<N; i++) {
				tk = new StringTokenizer(br.readLine());
				int a = Integer.parseInt(tk.nextToken());
				int b = Integer.parseInt(tk.nextToken());
				list.add(new Point(a,b));
			}
			
			//////
			long sumA = 0, sumB = 0;
			
			while(!list.isEmpty()) {
				//////1. a 기준 정렬
				Collections.sort(list);
				Point pa = list.poll();
				sumA += pa.a;
				
				if(list.isEmpty()) break;
				
				/////2. b 기준 정렬
				Collections.sort(list,new Comparator<Point>() {
					@Override
					public int compare(Point o1, Point o2) {
						if(o2.b==o1.b) {
							return o2.a-o1.a;
						}
						return o2.b-o1.b;
					}
				});
				Point pb = list.poll();
				sumB += pb.b;
			
			}//while
			
			//output
			long answer = sumA-sumB;
			System.out.println("#"+tc+" "+answer);
		}//tc
	}

}
