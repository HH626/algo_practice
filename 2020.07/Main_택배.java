import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

/**
 * 백준 8980 택배
 */
public class Main_택배 {
	static class Point implements Comparable<Point>{
		int from, to;
		int box;
		Point(int from, int to , int box){
			this.from = from;
			this.to = to;
			this.box  = box;	
		}
		public int compareTo(Point o) {
			//도착지 기준으로 정렬
			return this.to == o.to? this.from -o.from : this.to-o.to;
		};
		@Override
		public String toString() {
			return "Point [from=" + from + ", to=" + to + ", box=" + box + "]";
		};
		
	}
	static int N, C, boxes[];
	static PriorityQueue<Point> pq;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken()); //마을 수: 2~2000 
		C = Integer.parseInt(tk.nextToken()); //최대 박스 수 : 1~10000
		
		boxes = new int[N+1];	//n번 마을에서 트럭이 싣고있는 박스 수;
		pq = new PriorityQueue<Point>();	

		int M = Integer.parseInt(br.readLine());
		for(int i =0; i<M ;i++) {
			tk = new StringTokenizer(br.readLine());
			int from = Integer.parseInt(tk.nextToken());
			int to = Integer.parseInt(tk.nextToken());
			int box = Integer.parseInt(tk.nextToken());
			pq.offer(new Point(from, to, box));
		}//input
		
		
		int answer = delivery();
		//
		System.out.println(answer);
		
	}//main
	private static int delivery() {
		int totalSum = 0;
		while(!pq.isEmpty()) {
			Point p = pq.poll();
			//해당 구간에서 최대로 들어있는 박스 개수
			int max =0;
			for(int i=p.from; i<p.to; i++) {
				max = Math.max(max,boxes[i]);
			}
			//실을수 있는 박스
			int newBox =0;
			if(p.box+max<=C) {
				newBox = p.box;
			}else {
				newBox = C-max;
			}
			//박스 
			for(int i=p.from; i<p.to; i++) {
				boxes[i] += newBox;
			}
			totalSum += newBox;
		}
		return totalSum;
	}//deli

}

