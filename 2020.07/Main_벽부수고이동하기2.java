import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 14442 벽 부수고 이동하기2
 */
public class Main_벽부수고이동하기2 {
	static int R, C, K;
	static char map[][];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		K = Integer.parseInt(tk.nextToken());			//부술수 있는 벽 최대 갯수
		
		map = new char[R+2][C+2];
		for(int i=0; i<R+2; i++) {
			if(i==0 || i==R+1) continue;
			String line = br.readLine();
			for(int j=1; j<=C; j++) {
				map[i][j] = line.charAt(j-1);
			}
		}//input
		
		////
		int answer = bfs();
		
		
		//output
		System.out.println(answer);
		
	}//main
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static int bfs() {
		Queue<Point> que = new LinkedList<Point>();
		boolean[][][] visited = new boolean[R+2][C+2][K+1];
		
		que.add(new Point(1,1,1,0));
		visited[1][1][0] = true;
		
		while(!que.isEmpty()) {
			Point p = que.poll();
			int r = p.r;
			int c = p.c;
			int level = p.level;
			int boom = p.boom;
			
			//도착
			if(r==R && c==C) {
				return level;
			}
			
			for(int d=0; d<4; d++) {
				int nr = r+dR[d];
				int nc = c+dC[d];
				
				if(nr<1 || nr>R || nc<1 || nc>C ||visited[nr][nc][boom]) continue;
				
				if(map[nr][nc] == '1' && boom<K) {	//1)벽인경우 부술수 있을때 부수고가기
					visited[nr][nc][boom+1] = true;
					que.offer(new Point(nr,nc, level+1, boom+1));
					
				}else if(map[nr][nc] == '0') {		//2)벽 아닌경우
					visited[nr][nc][boom] = true;
					que.offer(new Point(nr, nc, level+1, boom));
				}
			}
		}//while
		
		return -1;
		
		
	}//bfs
	
	static class Point{
		int r, c;
		int level;
		int boom;
		Point(int r, int c, int level, int boom){
			this.r = r;
			this.c = c;
			this.level = level;
			this.boom = boom;
		}
		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + ", level=" + level + ", boom=" + boom + "]";
		}
		
	}

}
