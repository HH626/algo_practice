import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 * SWEA 2382 [모의] 미생물 격리
 */
public class Solution_미생물격리 {
	static class Point{
		int no, r, c, num, dir;
		Point(int no, int r, int c, int num, int dir){
			this.no = no;
			this.r = r;
			this.c = c;
			this.num = num;
			this.dir = dir;
		}
		@Override
		public String toString() {
			return "P(num=" + num + ", dir=" + dir + ")";
		}
		
	}
	static int N,M,K;
	static LinkedList<Point> map[][];
	static Point[] group;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());
			M = Integer.parseInt(tk.nextToken());
			K = Integer.parseInt(tk.nextToken());
			
			group = new Point[K];
			map = new LinkedList[N][N];
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					map[i][j] = new LinkedList<Point>();
				}
			}
			
			for(int i=0; i<K; i++) {
				tk = new StringTokenizer(br.readLine());
				int r = Integer.parseInt(tk.nextToken());
				int c = Integer.parseInt(tk.nextToken());
				int num = Integer.parseInt(tk.nextToken());
				int dir = Integer.parseInt(tk.nextToken());
				//dir 상하좌우 순서를 상우하좌로 바꿈
				if(dir == 1) dir = 0;
				else if(dir == 4) dir = 1;
				group[i] = new Point(i,r,c,num,dir);
			}//input
			
			
			for(int i=0; i<M; i++) {
				//1.군집 이동
				move();
				//2.미생물 수, 방향 변경시키기
				change();
			}
			
			int answer = 0;
			for (Point p : group) {
				if(p==null) continue;
				answer += p.num;
			}
			//output
			System.out.println("#"+tc+" "+answer);
		}//tc
	}//main

	static int[] dR = {-1,0,1,0}; 
	static int[] dC = {0,1,0,-1}; 
	private static void move() {
		for(int i=0; i<K; i++) {
			Point p = group[i];
			if(p==null) continue;
			
			p.r = p.r+dR[p.dir];
			p.c = p.c + dC[p.dir];
			map[p.r][p.c].add(p);
		}
	}//move
	
	private static void change() {
		for(int i=0; i<K; i++) {
			Point p = group[i];
			if(p==null) continue;
			
			int r = p.r;
			int c = p.c;
			//1. 약품 영역
			if(r<1 || r>N-2 || c<1 || c>N-2) {
				p.num = p.num/2;
				p.dir = (p.dir+2)%4;
				//num이 0이면면 군집 없어짐
				if(p.num<1) group[i] = null; 
			}//1.
			
			//2. 여러 미생물 그룹 만남
			if(map[r][c].size()>1) {
				int bigGroup=-1;
				int sum=0;
				for (Point j : map[r][c]) {
					sum += j.num;
					if(bigGroup==-1) {
						bigGroup = j.no;
						continue;
					}
						
					if(j.num > group[bigGroup].num) {
						group[bigGroup]=null;
						bigGroup = j.no;
					}else {
						group[j.no] = null;
					}
				}
				group[bigGroup].num = sum;

			}//2.
			
			//해당 위치 map 초기화
			map[r][c].clear();
		}//for
	}//change

}
