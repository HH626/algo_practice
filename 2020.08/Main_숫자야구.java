import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 2503 숫자 야구
 */
public class Main_숫자야구 {

	static int N, answer, comp;
	static Elem[] arr;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		N = Integer.parseInt(br.readLine());
		answer = 0;
		comp = 111;
		
		arr = new Elem[N];
		for(int i=0; i<N; i++) {
			tk = new StringTokenizer(br.readLine());
			String num = tk.nextToken();
			int strike = Integer.parseInt(tk.nextToken());
			int ball = Integer.parseInt(tk.nextToken());
			arr[i] = new Elem(num, strike, ball);
		}//in
		
		//1.세자리 수 다 만들어 보기
		for(int i=1; i<=9; i++) {
			for(int j=1; j<=9; j++) {
				if(i==j) continue;
				for(int k=1; k<=9; k++) {
					if(i==k || j==k) continue;
					comp = i*100+j*10+k;
					//2.답인지 확인해 보기
					boolean flag = check();
					if(flag) {
						answer++;
					}
				}
			}
		}
		

		System.out.println(answer);
	}//main
	
	private static boolean check() {
		String str = Integer.toString(comp);
		for(int i=0; i<N; i++) {
			int strike=0, ball=0;
			Elem e = arr[i];
			
			//비교하면서 스트라이크 볼 구하기
			for(int j=0; j<3; j++) {
				char cur = e.num.charAt(j);
				for(int k=0; k<3; k++) {
					if(cur==str.charAt(k)) {
						if(j==k) {
							strike++;
						}else {
							ball++;
						}
					}
				}
			}
			//같지 않으면 break;
			if(e.strike != strike || e.ball != ball) return false;
		}
		
		return true;
	}
	
	static class Elem{
		String num;
		int strike, ball;
		Elem(String num, int strike, int ball){
			this.num = num;
			this.strike = strike;
			this.ball = ball;
		}
	}

}
