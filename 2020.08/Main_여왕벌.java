import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 10836 여왕벌
 */
public class Main_여왕벌 {
	static int M, N, map[][];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		M = Integer.parseInt(tk.nextToken());			//map 크기: 2~700
		N = Integer.parseInt(tk.nextToken());			//날짜 수: ~1,000,000
		map = new int[M][M];
		int[] arr = new int[2*M-1];
		
		for(int i=0; i<N; i++) {
			tk = new StringTokenizer(br.readLine());
			int num[] = new int[3];
			num[0] = Integer.parseInt(tk.nextToken());
			num[1] = num[0]+Integer.parseInt(tk.nextToken());
			num[2] = num[1]+Integer.parseInt(tk.nextToken());
			//input값으로 맵 채우는거
			for(int j=num[0]; j<num[1]; j++) {
				arr[j] += 1;
			}
			for(int j=num[1]; j<num[2]; j++) {
				arr[j] += 2;
			}
		}//in
		
		int idx=0;
		for(int i=M-1; i>0; i--) {
			map[i][0] = arr[idx];
			idx++;
		}
		for(int j=0; j<M; j++) {
			map[0][j] = arr[idx];
			idx++;
		}
		
		

		/////
		for(int i=1; i<M; i++) {
			for(int j=1; j<M; j++) {
				map[i][j] = map[i-1][j];
			}
		}
		
		
		//out
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<M; i++) {
			for(int j=0; j<M; j++) {
				sb.append(map[i][j]+1).append(" ");
			}
			sb.append("\n");
		}
		
		System.out.println(sb.toString());
		
	}//main

}
