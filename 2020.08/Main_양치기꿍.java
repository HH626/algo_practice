import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 3187 양치기 꿍
 */
public class Main_양치기꿍 {

	static int R, C, wolf, lamb;
	static char[][] map;
	static boolean[][] visited;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		
		map = new char[R][C];
		visited = new boolean[R][C];
		wolf = 0;
		lamb = 0;
		
		for(int i=0; i<R; i++) {
			String line = br.readLine();
			for(int j=0; j<C; j++) {
				map[i][j] = line.charAt(j);
			}
		}//in
		
		////1. 우리 아닌곳 찾기
		for(int i=0; i<R; i++) {
			for(int j=0; j<C; j++) {
				if(map[i][j] != '#' && !visited[i][j]) {
					bfs(i,j);
				}
			}
		}
		
		
		
		//out
		System.out.println(lamb+" "+wolf);
		
	}//main

	private static int[] dR = {-1,0,1,0};
	private static int[] dC = {0,1,0,-1};
	private static void bfs(int r, int c) {
		Queue<Point> que = new LinkedList<Point>();
		int wcnt =0, lcnt = 0;
		
		visited[r][c] = true;
		que.offer(new Point(r,c));
		
		////2. 울타리 안에 양이랑 늑대 수 구하기
		while(!que.isEmpty()) {
			Point p = que.poll();
			r = p.r;
			c = p.c;
			
			if(map[r][c]=='v') {
				wcnt++;
			}else if(map[r][c]=='k') {
				lcnt++;
			}
			
			for(int d=0; d<4; d++) {
				int nr = r + dR[d];
				int nc = c + dC[d];
				
				if(nr<0 || nr >=R || nc<0 || nc>=C || visited[nr][nc] || map[nr][nc]=='#') continue;
				
				visited[nr][nc] = true;
				que.offer(new Point(nr, nc));
			}
		}//while
		
		////3. 잡아 먹힌거 처리
		if(lcnt>wcnt) {
			lamb += lcnt;
		}else {
			wolf += wcnt;
		}
		
	}//bfs
	
	static class Point{
		int r, c;
		Point(int r, int c){
			this.r = r;
			this.c = c;
		}
	}
	
}
