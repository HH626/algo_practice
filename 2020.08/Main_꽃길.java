import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 14620 꽃길
 */
public class Main_꽃길 {
	static int N, map[][], MIN;
	static boolean visited[][];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		N = Integer.parseInt(br.readLine());
		map = new int[N][N];
		visited = new boolean[N][N];
		MIN = 999999999;
		
		for(int i=0; i<N; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}//in
		
		////
		dfs(0,0);
		
		
		//out
		System.out.println(MIN);
		
	}//main
	
	private static void dfs(int cnt, int sum) {
		if(cnt == 3) {
			MIN = Math.min(MIN, sum);
			return; 
		}
		
		for(int r=1; r<N-1; r++) {
			for(int c=1; c<N-1; c++) {
				if(check(r,c)) {//다른 꽃이랑 안겹치는지확인
					int cost = flower(true,r,c);
					dfs(cnt+1, sum+cost);
					flower(false,r,c);
				}
			}
		}
	}//dfs
	
	private static boolean check(int r, int c) {
		if(visited[r][c] || visited[r-1][c] || visited[r][c-1] || visited[r+1][c] || visited[r][c+1])
			return false;
		return true;
	}
	
	private static int flower(boolean flag, int r, int c) {
		visited[r][c] = flag;
		visited[r-1][c] = flag;
		visited[r][c-1] = flag;
		visited[r+1][c] = flag; 
		visited[r][c+1] = flag;
		return map[r][c]+ map[r-1][c]+ map[r][c-1] +map[r+1][c] +map[r][c+1] ;
	}

}
