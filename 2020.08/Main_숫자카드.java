import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 백준 2591 숫자카드
 * 
 * DP
 */
public class Main_숫자카드 {

	static int N, num[], dp[][];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line = br.readLine();
		N = line.length();
		num = new int[N];
		dp = new int[N][2];
		
		for(int i=0; i<N; i++) {
			num[i] = line.charAt(i)-'0';
		}//in

		////
		if(num[0]!=0) {
			dp[0][0] = 1;
			dp[0][1] = 0;
		}
		
		for(int i=1; i<N; i++) {
			//1)지금 숫자가 단독을 쓰일 때
			if(num[i] > 0) {
				dp[i][0] = dp[i-1][0] + dp[i-1][1];
			}else if(num[i] == 0){
				dp[i][0] = 0;
			}
			
			//2)지금 숫자가 두자리수에 포함 될때
			if(10<= num[i-1]*10+num[i] && num[i-1]*10+num[i]<=34) {
				dp[i][1] = dp[i-1][0];
			}else {
				dp[i][1] = 0;
			}
		}
		
		//out
		System.out.println(dp[N-1][0]+dp[N-1][1]);
	}
	

}
