import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 백준 3085 사탕게임
 */
public class Main_사탕게임 {
	static int N;
	static char map[][];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		
		map = new char[N][N];
		for(int i=0; i<N; i++) {
			String line = br.readLine();
			for(int j=0; j<N; j++) {
				map[i][j] = line.charAt(j);
			}
		}//in
		
		int max = 0;
		for(int i=0; i<N-1; i++) {
			for(int j=0; j<N; j++) {
				
				if(j != N-1) {
					//1.가로 변경
					change(i,j, i,j+1);
					
					//1-1.긴 사탕
					int a = numOfCandy(i, j, 1, 0);
					int b = numOfCandy(i, j+1, 1, 0);
					int c = numOfCandy(i, j, 0, 1);
					max = Math.max(max, longest(a,b,c));
					
					//1-2. 원상태 복원
					change(i,j, i,j+1);
				}
				
				//2.세로 변경
				change(i,j, i+1,j);
				
				//2-1.긴 사탕
				int a = numOfCandy(i, j, 0, 1);
				int b = numOfCandy(i+1, j, 0, 1);
				int c = numOfCandy(i, j, 1, 0);
				max = Math.max(max, longest(a,b,c));
				
				//2-2.상태복원
				change(i,j, i+1,j);
				
			}
		}
		
		//out
		System.out.println(max);
	}//main
	
	private static int longest(int a, int b, int c) {
		int tmp = Math.max(a, b);
		return Math.max(tmp,c);
	}

	
	private static int numOfCandy(int r, int c, int dr, int dc) {
		int nr = r;
		int nc = c;
		char color = map[r][c];
		
		int cnt = 1;
		
		while(true) {
			nr = nr+dr;
			nc = nc+dc;
			if(nr>=0 && nr<N && nc>=0 && nc<N && map[nr][nc]==color) {
				cnt++;
			}else {
				break;
			}
		}
		
		nr = r;
		nc = c;
		while(true) {
			nr = nr-dr;
			nc = nc-dc;
			if(nr>=0 && nr<N && nc>=0 && nc<N && map[nr][nc]==color) {
				cnt++;
			}else {
				break;
			}
		}
		
		return cnt;
	}//
	
	private static void change(int r1, int c1, int r2, int c2) {
		char tmp = map[r1][c1];
		map[r1][c1] = map[r2][c2];
		map[r2][c2] = tmp;
	}//change
	
	static class Point{
		int r, c;
		Point(int r, int c){
			this.r = r;
			this.c = c;
		}
	}

}
