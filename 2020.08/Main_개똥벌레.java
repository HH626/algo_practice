import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 3020 개똥벌레
 */
public class Main_개똥벌레 {

	static int N, H, bottom[], top[], obstacle[];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());	//가로길이
		H = Integer.parseInt(tk.nextToken());	//높이
		
		bottom = new int[H+1];					//bottom[n]: 길이 n의 석순개수
		top = new int[H+1];						//top[n]: 길이 n의 종유석 개수
		obstacle = new int[H+1];				//obstacle[n]: 위치n에 가로막고 있는 장애물 수(n:위부터 1~H)
	
		int min = 999999999;					
		int cnt = 0;							
		
		for(int i=0; i<N/2; i++) {
			bottom[Integer.parseInt(br.readLine())]++;
			top[Integer.parseInt(br.readLine())]++;
		}//in
		
		//높이가 h면 h-1...1인 곳도 막고 있는 것이므로 누적해서 저장해놓기
		for(int i=H-1; i>0; i--) {
			top[i] += top[i+1];
			bottom[i] += bottom[i+1];
		}
		
		for(int i=1; i<=H; i++) {
			obstacle[i] = top[i]+bottom[H-i+1];
			if(obstacle[i]<min) {
				min = obstacle[i];
				cnt=1;
			}else if(obstacle[i] == min) {
				cnt++;
			}
		}
		
		//out
		System.out.println(min+" "+cnt);
		
	}//main

}
