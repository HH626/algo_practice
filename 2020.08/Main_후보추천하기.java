import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * 백준 1713 후보 추천하기
 */
public class Main_후보추천하기 {

	static int N, M;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		N = Integer.parseInt(br.readLine());
		M = Integer.parseInt(br.readLine());
		
		
		HashMap<Integer,Point> hm = new HashMap<Integer,Point>();
		
		tk = new StringTokenizer(br.readLine());
		for(int i=0; i<M; i++) {
			int num = Integer.parseInt(tk.nextToken());
			if(hm.containsKey(num)) {	//1)이미 추천 받음
				//추천수 올리기
				hm.get(num).recom++;
			}else {						//2)새로 추천 받음
				if(hm.size()<N) {		//2-1)자리 비어있음
					//새로 추가
					hm.put(num, new Point(num, 1, i));
				}else{					//2-2)자리 꽉 참
					//추천수 작거나 오랜된 사진 버리기
					Point min = new Point(0,999999,999999);
					for (Integer a : hm.keySet()) {
						Point p = hm.get(a);
						if(p.recom<min.recom) {
							min = p;
						}else if(p.recom == min.recom){
							min = p.time<min.time? p: min;
						}
					}
					hm.remove(min.num);
					//새로 추가
					hm.put(num, new Point(num, 1, i));
				}
			}
		}
		
		int[] answer = new int[N];
		int idx=0;
		for (Integer a : hm.keySet()) {
			answer[idx] = a;
			idx++;
		}
		
		Arrays.sort(answer);
		for (int i : answer) {
			System.out.print(i+" ");
		}
		
	}//main
	
	static class Point{
		int num;
		int recom;
		int time;
		Point(int num, int recom, int time){
			this.num = num;
			this.recom = recom;
			this.time = time;
		}
	}

}
