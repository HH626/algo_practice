import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 * 백준 10282 해킹
 * 
 * 인접행렬 -> 메모리 초과 (256MB기준)
 * 인접리스트 -> 통과
 */
public class Main_해킹 {

	static int N, adjMap[][], dist[];
	static boolean visited[];
	static LinkedList<Point>[] adjList;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T ;tc++) {
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());				//컴퓨터 대수: ~10,000
			int D = Integer.parseInt(tk.nextToken());			//의존관계 수: ~100,000
			int start = Integer.parseInt(tk.nextToken());		//시작 컴퓨터 번호
			
			
			adjList = new LinkedList[N+1];
			for(int i=1; i<=N; i++) {
				adjList[i] = new LinkedList<Point>();
			}
			dist = new int[N+1];
			Arrays.fill(dist, 999999999);
			visited = new boolean[N+1];
			
			for(int i=0; i<D; i++) {
				tk = new StringTokenizer(br.readLine());
				int a = Integer.parseInt(tk.nextToken());		//
				int b = Integer.parseInt(tk.nextToken());		//
				int s = Integer.parseInt(tk.nextToken());		//
				adjList[b].add(new Point(a,s));
			}//in
			
			dijkstra(start);
			
			//out
			int max = 0, cnt=0;
			for(int i=1; i<=N; i++) {
				if(visited[i]) {
					cnt++;
					max = Math.max(max, dist[i]);
				}
			}
			
			System.out.println(cnt+" "+max);
		}//tc
	}//main
	
	private static void dijkstra(int start) {
		dist[start] = 0;

		boolean flag;
		int from = start;
		do{
			flag = false;
			visited[from] = true;
			//from에서 to까지 최짧은값 구하기
			for (Point p : adjList[from]) {
				int to = p.to;
				if(dist[from] + p.val < dist[to]) {
					dist[to] = dist[from] + p.val;
				}
			}
			
			//가장 작은 값 찾기
			int min = 999999999;
			for(int i=0; i<=N; i++) {
				if(visited[i]) continue;
				if(dist[i]<min) {
					from = i;
					min = dist[i];
					flag = true;
				}
			}
			
		}while(flag);

	}//dijkstra
	
	static class Point{
		int to, val;
		Point(int to, int val){
			this.to = to;
			this.val = val;
		}
	}

}
