import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 2178 미로탐색
 */
public class Main_미로탐색 {
	static int R,C,map[][];
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());			
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		
		map = new int[R+2][C+2];
		
		for(int i=1; i<=R; i++) {
			String line = br.readLine();
			for(int j=1; j<=C; j++) {
				map[i][j] = (int)(line.charAt(j-1)-'0');
			}
		}//input
		
		
		int answer = bfs();
		
		//out
		System.out.println(answer);
	}
	
	static int dR[] = {-1,0,1,0};
	static int dC[] = {0,1,0,-1};
	public static int bfs() {
		Queue<Point> que = new LinkedList<Point>();
		boolean visited[][] = new boolean[R+2][C+2];
			
		visited[1][1] = true;
		que.offer(new Point(1,1,1));
		
		while(!que.isEmpty()) {
			Point p = que.poll();
			int r = p.r;
			int c = p.c;
			int cnt = p.cnt;
			
			if(r==R && c==C) {
				return cnt;
			}
			
			for(int d=0; d<4; d++) {
				int nr = r+dR[d];
				int nc = c+dC[d];
				
				if(!visited[nr][nc] && map[nr][nc]==1) {
					visited[nr][nc] = true;
					que.offer(new Point(nr, nc, cnt+1));
				}
				
			}
		}//while
		
		return -1;
		
	}
	
	static class Point{
		int r, c;
		int cnt;
		Point(int r, int c, int cnt){
			this.r = r;
			this.c = c;
			this.cnt = cnt;
		}
	}

}
