import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.StringTokenizer;

/**
 * 백준 2493 탑
 */
public class Main_탑 {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		int N = Integer.parseInt(br.readLine());
		int tower[] = new int[N+1];
		int arr[] = new int[N+1];
		tk = new StringTokenizer(br.readLine());
		for(int i=1; i<=N; i++) {
			tower[i] = Integer.parseInt(tk.nextToken());
		}
		
		
		
		Stack<Integer> stack = new Stack<>();
		int high;
		for(int i=1; i<=N; i++) {			
			while(true) {
				if(stack.isEmpty()) {
					arr[i] = 0;
					break;
				}
				high = stack.peek();
				if(tower[high] > tower[i]) {
					arr[i] = high;
					break;
				}else {
					stack.pop();
				}
			}//while
			
			stack.add(i);
		}
		
		for(int i=1; i<=N; i++) {
			System.out.print(arr[i]+" ");
		}
	}//main

}
