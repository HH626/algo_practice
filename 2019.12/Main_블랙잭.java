import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 2798 블랙잭
 */
public class Main_블랙잭 {

	static int N, M, arr[], answer;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());
		M = Integer.parseInt(tk.nextToken());
		
		answer =0;
		arr = new int[N];
		visited = new boolean[N];
		tk = new StringTokenizer(br.readLine());
		for(int i=0; i<N; i++) {
			arr[i] = Integer.parseInt(tk.nextToken());
		}
		
		dfs(0,0,0);
		
		System.out.println(answer);
	}//main

	static boolean visited[];
	private static void dfs(int i, int c, int sum) {
		if(c==3) {
			if(answer<sum && sum<=M) {
				answer = sum;
			}
			return;
		}		
		if(i==N) return;
		
		visited[i] = true;
		dfs(i+1, c+1, sum+arr[i]);
		visited[i] = false;
		dfs(i+1, c, sum);
	}

}
