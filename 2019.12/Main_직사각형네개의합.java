import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 2669 직사각형 네개의 합집합의 면적 구하기
 */
public class Main_직사각형네개의합 {
	static int map[][];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		map = new int[101][101];
		
		int x1, y1, x2, y2;
		for(int i=0; i<4; i++) {
			tk = new StringTokenizer(br.readLine());
			x1 = Integer.parseInt(tk.nextToken());
			y1 = Integer.parseInt(tk.nextToken());
			x2 = Integer.parseInt(tk.nextToken());
			y2 = Integer.parseInt(tk.nextToken());
			for(int x=x1; x<x2; x++) {
				for(int y=y1; y<y2; y++) {
					map[y][x] = 1;
				}
			}
		}
		
		int sum=0;
		for(int i=1; i<=100; i++) {
			for(int j=1; j<=100; j++) {
				sum += map[i][j] ;
			}
		}
		
		System.out.println(sum);
	}//main

}
