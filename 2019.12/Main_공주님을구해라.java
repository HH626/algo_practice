import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 17836 공주님을 구해라
 */


public class Main_공주님을구해라 {

	private static class Point{
		int r, c, val;
		Point(int r, int c, int val){
			this.r = r;
			this.c = c;
			this.val = val;
		}
	}//class Point
	
	static int R, C, T;
	static int[][] map;
	static int result;
	public static void main(String[] args) throws NumberFormatException, IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		T = Integer.parseInt(tk.nextToken());
		map = new int[R+2][C+2];
		result = 99999999;
		
		Arrays.fill(map[0], 1);
		Arrays.fill(map[R+1], 1);
		for(int i=1; i<=R; i++) {
			tk = new StringTokenizer(br.readLine());
			Arrays.fill(map[i], 1);
			for(int j=1; j<=C; j++) {
				map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		
		
		////
		bfs(1,1);
		
		System.out.println(result>T?"Fail":result);
		
	}//main
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static void bfs(int r, int c) {
		Queue<Point> queue = new LinkedList<Point>();
		int[][] time = new int[R+2][C+2];
		for(int i=0; i<R+2; i++) {
			Arrays.fill(time[i], 99999999);
		}
		
		Point p;
		int curR, curC, nr, nc;
		
		queue.offer(new Point(r,c,0)); 
		while(!queue.isEmpty()) {
			p = queue.poll();
			curR = p.r;
			curC = p.c;
			if(p.val>T) {	//시간 초과
				return;
			}else if(curR==R && curC==C) {	//공주한테 도착하면
				result = Math.min(result, p.val);
				return;
			}else if(map[curR][curC]==2) {	//그람 찾음
				result = Math.min(result, R-curR + C-curC + p.val);
			}else {
				for(int d=0; d<4; d++) {
					nr = curR + dR[d];
					nc = curC + dC[d];
					if(map[nr][nc] != 1 && time[nr][nc]>p.val+1) {
						time[nr][nc] = p.val+1;
						queue.offer(new Point(nr, nc, p.val+1));
					}
					
				}
			}
		}//while
		
	}//bfs

}
