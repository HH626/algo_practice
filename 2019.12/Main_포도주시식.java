import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 백준 2156 포도주 시식
 * DP
 */
public class Main_포도주시식 {

	static int N, arr[]; 
	static int[][] D;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		arr = new int[N];
		for(int i=0; i<N; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		
		D = new int[N][3];
		D[0][0] = 0;
		D[0][1] = arr[0];
		D[0][2] = arr[0];
		
		if(N>1) {
			D[1][0] = arr[0];
			D[1][1] = arr[1];
			D[1][2] = arr[0]+arr[1];
	
			
			for(int i=2; i<N; i++) {
				D[i][0] = max(i-1);
				D[i][1] = D[i-1][0]+arr[i];
				D[i][2] = D[i-2][0]+arr[i-1]+arr[i];		
			}
		}
		
		System.out.println(max(N-1));
		

	}//main
	
	public static int max(int i) {
		return Math.max(Math.max(D[i][0], D[i][1]), D[i][2]);
	}

}
