import java.util.Scanner;

/**
 * 백준 2231 분해합
 */
public class Main_분해합 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		
		int m=0, cur, result =0;
		for(int i=1; i<N; i++) {

			cur = i;
			m = cur;
			while(cur>0) {
				m += cur%10;
				cur=cur/10;
			}
			m += cur;
			
			if(m==N) {
				result = i;
				break;
			}
		}
		
		System.out.println(result);
	}//main

}
