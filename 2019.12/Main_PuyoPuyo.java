import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 백준 11559 PuyoPuyo
 * 시뮬레이션
 */
public class Main_PuyoPuyo {

	static char[][] map;
	static final int R=12, C=6;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		map = new char[R+2][C+2];
		
		
		String line;
		for(int i=1; i<=R; i++) {
			line = br.readLine();
			for(int j=1; j<=C; j++) {
				map[i][j]=line.charAt(j-1);
			}
		}
		
		////
		boolean flag = true, isCombo=false;
		int combo=0;
		while(flag) {
			for(int i=1; i<=R; i++) {
				for(int j=1; j<=C; j++) {
					if(map[i][j] != '.') {
						if(bfs(i,j,map[i][j])) {
							isCombo=true;
							boom(i,j,map[i][j]);
						}
					}//if
				}
			}//for
			drop();
			if(isCombo) {
				 combo++;
				 isCombo=false;
			}else {
				flag = false;
			}
		}//while
		
		
		int result=combo;
		System.out.println(result);
	}//main
	
	private static void drop() {
		LinkedList<Character> tempList = new LinkedList<>();
		
		for(int j=1; j<=C; j++) {
			for(int i=R; i>0; i--) {
				if(map[i][j] != '.') {
					tempList.add(map[i][j]);
				}
			}
			for(int i=R; i>0; i--) {
				if(tempList.isEmpty()) {
					map[i][j] = '.';
				}else {
					map[i][j] = tempList.pollFirst();
				}
			}
		}
	}//drop

	private static void boom(int r, int c, int color) {
		Queue<int[]> queue = new LinkedList<>();
		int nR, nC, a[];
		
		map[r][c] = '.';
		queue.offer(new int[] {r,c});
		
		while(!queue.isEmpty()) {
			a = queue.poll();
			for(int d=0; d<4; d++) {
				nR = a[0]+dR[d];
				nC = a[1]+dC[d];
				
				if(map[nR][nC]==color) {
					map[nR][nC] = '.';
					queue.offer(new int[]{nR,nC});
				}
			}
		}//while
	}//boom

	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static boolean bfs(int r, int c, char color) {	//터질수 있는지 없는지 확인
		Queue<int[]> queue = new LinkedList<>();
		int nR, nC, a[], cnt=0;
		boolean visited[][] = new boolean[R+2][C+2];
		
		visited[r][c] = true;
		queue.offer(new int[] {r,c});
		cnt++;
		
		while(!queue.isEmpty()) {
			a = queue.poll();
			for(int d=0; d<4; d++) {
				nR = a[0]+dR[d];
				nC = a[1]+dC[d];
				
				if(map[nR][nC]==color && !visited[nR][nC]) {
					visited[nR][nC] = true;
					queue.offer(new int[]{nR,nC});
					cnt++;
				}
				if(cnt>=4) {
					return true;
				}
			}
		}//while
		return false;
	}//bfs

}
