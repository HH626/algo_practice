import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 2564 경비원
 */
public class Main_경비원 {

	static int R, C, N;
	static int dong;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		tk = new StringTokenizer(br.readLine());
		C = Integer.parseInt(tk.nextToken());
		R = Integer.parseInt(tk.nextToken());
		N =Integer.parseInt(br.readLine()) + 1;
		int dir, dist;
		int[] arr[] = new int[N][2];
		for(int i=0; i<N; i++) {
			tk = new StringTokenizer(br.readLine());
			dir = Integer.parseInt(tk.nextToken());
			dist = Integer.parseInt(tk.nextToken());
			arr[i] = new int[] {dir, dist};
		}
		
		
		dong = lineLoc(arr[N-1][0],arr[N-1][1]);	//동근이 위치
		
		int shop, d, sum=0;
		for(int i=0; i<N-1; i++) {
			shop = lineLoc(arr[i][0],arr[i][1]);		
			d = Math.abs(dong-shop);
			sum += Math.min(d, R+R+C+C-d);	//더 짧은쪽 거리 더함
		}
		
		System.out.println(sum);
	}//main
	
	private static int lineLoc(int dir, int dist) { //사각형 길을 일자 길로 변환했을때 위치 
		switch(dir) {
		case 4://동
			return R-dist;
		case 1://북
			return R+C-dist;
		case 3://서
			return R+C+dist;
		case 2://남
			return R+C+R+dist;
		}
		return 0;
	}//lineLoc

}
