import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * 백준 17780 새로운 게임
 */
class Mar{

	int r, c, dir;
	Mar(int r, int c, int dir){
		this.r = r;
		this.c = c;
		this.dir = dir;
	}
	@Override
	public String toString() {
		return "Mar [r=" + r + ", c=" + c + ", dir=" + dir + "]";
	}
}

public class Main_새로운게임 {
	static int N, K, colorMap[][];
	static List<Integer>[][] map;
	static Mar[] mars;
	static int[] dR= {0,0,0,-1,1};
	static int[] dC = {0,1,-1,0,0};
	public static void main(String[] args)  throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());
		K = Integer.parseInt(tk.nextToken());
		//init colormap
		colorMap = new int[N+2][N+2];
		for (int[] m : colorMap) {
			Arrays.fill(m, 2);
		}
		//init mar map
		map = new LinkedList[N+2][N+2];
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				map[i][j] = new LinkedList<Integer>();
			}
		}
		//init mar arr
		mars = new Mar[K+1];
		//input colormap
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				colorMap[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		//input mar
		int r, c, d;
		for(int i=1; i<=K; i++) {
			tk = new StringTokenizer(br.readLine());
			r = Integer.parseInt(tk.nextToken());
			c = Integer.parseInt(tk.nextToken());
			d = Integer.parseInt(tk.nextToken());
			mars[i] = new Mar(r,c,d);
			map[r][c].add(i);
		}

		////
		Mar curMar;
		int curr, curc, nr, nc;
		int cnt=0;
		boolean flag = true;
		while(flag) {
			cnt++;
			for(int i=1; i<=K; i++) {
				curMar = mars[i];
				curr = curMar.r;
				curc = curMar.c;
				if(map[curr][curc].get(0) != i) {continue;} //맨 아래에 있는 말이아니면
				
				nr = curr + dR[curMar.dir];
				nc = curc + dC[curMar.dir];

				switch(colorMap[nr][nc]) {
				case 0: //white
					white(curr, curc, nr, nc);
					break;
				case 1: //red
					red(curr, curc, nr, nc);
					break;
				case 2:	//blue
					int[] a = blue(i, curr, curc, curMar.dir);
					nr = a[0];
					nc = a[1];
					break;				
				}//switch
				if(map[nr][nc] != null && map[nr][nc].size()>=4) {
						flag= false;
				}
			}//for
			if(cnt>1000) {
				cnt = -1;
				flag = false;
			}
		}//while
	
		System.out.println(cnt);
	}//main
	
	public static void white(int r, int c, int nr, int nc) {
		List<Integer> temp = map[r][c];
		int no;
		for(int i=0, size = temp.size(); i<size; i++) {
			no = temp.get(i);
			map[nr][nc].add(no);
			mars[no].r = nr;
			mars[no].c = nc;
		}
		map[r][c]=new LinkedList<Integer>();
	}//white;
	
	public static void red(int r, int c, int nr, int nc) {
		List<Integer> temp = map[r][c];
		int no;
		for(int i= temp.size()-1; i>=0; i--) {
			no = temp.get(i);
			map[nr][nc].add(no);
			mars[no].r = nr;
			mars[no].c = nc;
		}
		map[r][c]=new LinkedList<Integer>();
		//map[r][c] = null;
	}//red
	
	public static int[] blue(int no, int r, int c, int dir) {
		switch(dir) {
		case 1://right
			mars[no].dir = 2;
			break;
		case 2://left
			mars[no].dir = 1;
			break;
		case 3://up
			mars[no].dir = 4;
			break;
		case 4://down
			mars[no].dir = 3;
			break;
		}
		
		Mar curMar = mars[no];
		int nr = curMar.r + dR[curMar.dir];
		int nc = curMar.c + dC[curMar.dir];
		
		if(colorMap[nr][nc]==2) {
		}else if (colorMap[nr][nc]==1){
			red(r, c, nr, nc);
		}else if(colorMap[nr][nc]==0) {
			white(r, c, nr, nc);
		}
		
		return new int[]{nr, nc};
	}//blue;
	
	
}
