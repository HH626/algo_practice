import java.util.Scanner;
import java.util.Stack;
/**
 * 백준 15649 N과M(1)
 * 백준 15650 N과M(2)
 */
public class Main_N과M {
	static int N, M;
	static boolean visited[];
	static int arr[];
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt(); //N까지 자연수
		M = sc.nextInt(); //M개
		
		visited = new boolean[N+1];
		arr = new int[M+1];
		per(0);
		
		com(1,0);
		
	}//main
	
	public static void per(int cnt) {
		if(cnt==M) {
			for(int i=0; i<M; i++) {
				System.out.print(arr[i]+" ");
			}
			System.out.println();
			return;
		}
		if(cnt>M) return;
		
		for(int i=1; i<=N; i++) {
			if(visited[i]==false) {
				visited[i] = true;
				arr[cnt]=i;
				per(cnt+1);
				visited[i] = false;
			}	
		}
		
	}//순열 N과M(1)
	
	static Stack<Integer> stack = new Stack<Integer>();
	public static void com(int n, int cnt) {
		if(cnt==M) {
			for(int i=0; i<M; i++) {
				System.out.print(stack.get(i)+" ");
			}
			System.out.println();
			return;
		}
		if(cnt>M) return;
		if(n>N) return;
		
		stack.push(n);
		com(n+1, cnt+1);
		stack.pop();
		com(n+1, cnt);
		
	}//조합 N과M(2)

}
