import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * 백준 17837 새로운 게임2
 */
class Mar{

	int r, c, dir;
	Mar(int r, int c, int dir){
		this.r = r;
		this.c = c;
		this.dir = dir;
	}
	@Override
	public String toString() {
		return "Mar [r=" + r + ", c=" + c + ", dir=" + dir + "]";
	}
}

public class Main_새로운게임2 {
	static int N, K, colorMap[][];
	static List<Integer>[][] map;
	static Mar[] mars;
	static int[] dR= {0,0,0,-1,1};
	static int[] dC = {0,1,-1,0,0};
	public static void main(String[] args)  throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());
		K = Integer.parseInt(tk.nextToken());
		//init colormap
		colorMap = new int[N+2][N+2];
		for (int[] m : colorMap) {
			Arrays.fill(m, 2);
		}
		//init mar map
		map = new LinkedList[N+2][N+2];
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				map[i][j] = new LinkedList<Integer>();
			}
		}
		//init mar arr
		mars = new Mar[K+1];
		//input colormap
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				colorMap[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		//input mar
		int r, c, d;
		for(int i=1; i<=K; i++) {
			tk = new StringTokenizer(br.readLine());
			r = Integer.parseInt(tk.nextToken());
			c = Integer.parseInt(tk.nextToken());
			d = Integer.parseInt(tk.nextToken());
			mars[i] = new Mar(r,c,d);
			map[r][c].add(i);
		}

		////
		Mar curMar;
		int curr, curc, nr, nc;
		int cnt=0, bottom=0;
		boolean flag = true;
		while(flag) {
			cnt++;
			for(int i=1; i<=K; i++) {
				curMar = mars[i];
				curr = curMar.r;
				curc = curMar.c;
				
				bottom = 0;
				for(int j=0, size = map[curr][curc].size(); j<size; j++) {	//지금움직이려는 말이 밑에서 부터 몇번째에 있는지 찾기
					if(map[curr][curc].get(j) == i) {
						bottom = j;
					}
				}
				
				nr = curr + dR[curMar.dir];
				nc = curc + dC[curMar.dir];

				switch(colorMap[nr][nc]) {
				case 0: //white
					white(bottom, curr, curc, nr, nc);
					break;
				case 1: //red
					red(bottom, curr, curc, nr, nc);
					break;
				case 2:	//blue
					int[] a = blue(bottom, i, curr, curc, curMar.dir);
					nr = a[0];
					nc = a[1];
					break;				
				}//switch
				if(map[nr][nc] != null && map[nr][nc].size()>=4) {
						flag= false;
				}
			}//for
			if(cnt>1000) {
				cnt = -1;
				flag = false;
			}
		}//while
	
		System.out.println(cnt);
	}//main
	
	public static void white(int bottom, int r, int c, int nr, int nc) {
		List<Integer> temp = map[r][c];
		int no;
		for(int i=bottom, size = temp.size(); i<size; i++) {
			no = temp.get(bottom);
			map[nr][nc].add(no);
			mars[no].r = nr;
			mars[no].c = nc;
			map[r][c].remove(bottom);
		}
	}//white;
	
	public static void red(int bottom, int r, int c, int nr, int nc) {
		List<Integer> temp = map[r][c];
		int no;
		for(int i= temp.size()-1; i>=bottom; i--) {
			no = temp.get(i);
			map[nr][nc].add(no);
			mars[no].r = nr;
			mars[no].c = nc;
			map[r][c].remove(i);
		}
	}//red
	
	public static int[] blue(int bottom, int no, int r, int c, int dir) {
		switch(dir) {
		case 1://right
			mars[no].dir = 2;
			break;
		case 2://left
			mars[no].dir = 1;
			break;
		case 3://up
			mars[no].dir = 4;
			break;
		case 4://down
			mars[no].dir = 3;
			break;
		}
		
		Mar curMar = mars[no];
		int nr = curMar.r + dR[curMar.dir];
		int nc = curMar.c + dC[curMar.dir];
		
		if(colorMap[nr][nc]==2) {
		}else if (colorMap[nr][nc]==1){
			red(bottom, r, c, nr, nc);
		}else if(colorMap[nr][nc]==0) {
			white(bottom, r, c, nr, nc);
		}
		
		return new int[]{nr, nc};
	}//blue;
	
	
}
