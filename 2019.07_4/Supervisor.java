import java.util.Scanner;

/**
 * 백준 13458 시험감독
 *
 */
public class Supervisor {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int N; //응시장 수
		int[] A;//각 시험장의 응시자주
		int B; //총감독이 감시할 수 있는 응시자수
		int C; //부감독이 감시할 수 있는 응시자수
		
		N = sc.nextInt();
		A = new int[N];
		for(int i=0; i<N; i++) {
			A[i] = sc.nextInt();
		}
		B= sc.nextInt();
		C= sc.nextInt();
		
		long cnt =0;
		int result, mod;
		for(int i=0; i<N; i++) {
		//총감독
			if(A[i]>B) {
				A[i] = A[i]-B;
				cnt++;
				//부감독
				result = A[i]/C;
				mod = A[i]%C;
				cnt += result;
				if(mod==0) {}
				else {cnt++;}
			}else {
				cnt++;
			}
		}
		
		System.out.println(cnt);
		
		
		
	}// main

}
