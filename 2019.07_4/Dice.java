/**
 * 백준 14499 주사위굴리기
 */


import java.util.Scanner;

public class Dice {
	
	public static int TOP=1, NORTH=2, EAST=3, WEST=4, SOUTH=5, BOTTOM=6;
	public static int[] dice = new int[7];

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int[][] map;
		int[] direc;
		//input
		int N = sc.nextInt();
		int M = sc.nextInt();
		int r = sc.nextInt();
		int c = sc.nextInt();
		int K = sc.nextInt();
		
		map = new int[N][M];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				map[i][j]=sc.nextInt();
			}
		}
		
		direc = new int[K];
		for (int i = 0; i < K; i++) {
			direc[i] = sc.nextInt();
		}		
		//--------------------------------------
		
		int n;
		int curR = r, curC=c;
		//디랙션데로 이동
		for (int i = 0; i < K; i++) {
			n = direc[i];
			// 주사위굴림
			switch (n) {
			case 1: // 동으로 이동
				throwDice(EAST);
				curC = curC+1;
				break;
			case 2: // 서로이동
				throwDice(WEST);
				curC = curC-1;
				break;
			case 3: // 북으로 이동
				throwDice(NORTH);
				curR = curR -1;
				break;
			case 4: // 남으로 이동
				throwDice(SOUTH);
				curR = curR +1;
				break;
			}

			// 숫자변경
			if (map[curR][curC] == 0) {
				map[curR][curC] = dice[BOTTOM];
			} else {
				dice[BOTTOM] = map[curR][curC];
				map[curR][curC] = 0;
			}
			
			
			//output
			System.out.println(dice[TOP]);

		}		

	}//main
	
	public static void throwDice(int n) {
		int tmp = dice[BOTTOM];
		dice[BOTTOM] = dice[n];
		dice[n] = dice[TOP];
		dice[TOP] = dice[7-n];
		dice[7-n] = tmp;
	}

}
