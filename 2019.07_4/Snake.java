import java.util.ArrayList;
import java.util.Scanner;

/**
 * 백준 3190 뱀
 *
 */
public class Snake {

	public static int[][] map;
	
	public static ArrayList<Integer> rQ = new ArrayList<>();
	public static ArrayList<Integer> cQ = new ArrayList<>();
	
	public static void enq(int r, int c) {
		rQ.add(r);
		cQ.add(c);
		map[r][c] =1;
	}
	public static void dq() {
		int r, c;
		r = rQ.remove(0);
		c = cQ.remove(0);
		map[r][c] = 0;
	}

	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int N = sc.nextInt();//보드크기
		int K = sc.nextInt();//사과개수
		map = new int[N+2][N+2];
		for(int i=0; i<N+2; i++) { //map테두리 1로 초기화
			map[0][i] = 1;
			map[N+1][i] = 1;
			map[i][0] = 1;
			map[i][N+1] = 1;
		}
		
		int r, c;
		for(int i=0; i<K; i++) {  //사과 위치 4로 표시
			r = sc.nextInt();
			c = sc.nextInt();
			map[r][c] = 4;
		}
		
		int L = sc.nextInt();
		int[] s_arr = new int[L];
		String[] d_arr = new String[L];
		for(int i=0; i<L; i++) {
			s_arr[i] = sc.nextInt();
			d_arr[i] = sc.next();
		}
		
		
		int[][] dir = {{0,1},{1,0},{0,-1},{-1,0}};//우, 하, 좌, 상
		int i=0, sec=0, a=0;
		int curR=1, curC=1;
		enq(curR,curC);
		while(true) {
			//지시따라 움직이기
			if(sec == s_arr[i]) {
				if(d_arr[i].equals("L")) {
					if(a==0) {a=3;}
					else {a--;}
				}else if(d_arr[i].equals("D")) {
					a = (a+1)%4;
				}
				if(i<L-1) {i++;}
				
			}	
			
			
			//다음 위치
			curR = curR+dir[a][0];
			curC = curC+dir[a][1];
			
			//벽 이나 자기 몸에 부딪힘
			if(map[curR][curC] == 1) {
				sec++;
				break;
			}else if (map[curR][curC] == 4) { //사과 먹었는지 아닌지 확인
				enq(curR, curC);
			}else {
				enq(curR, curC);
				dq();
			}
			
			//초 +1
			sec++;		
			
		}
		
		System.out.println(sec);

	}//main
	

}
