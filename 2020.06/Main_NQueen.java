import java.util.Arrays;
import java.util.Scanner;

/*
 * 백준 1663 Nqueen
 */
public class Main_NQueen {

	static int N, answer;
	static boolean board[][];
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		board = new boolean[N][N];
		answer = 0;
		
		putQueen(0,0);
		
		System.out.println(answer);

	}
	private static void putQueen(int r, int cnt) {
		if(cnt==N) {
			answer++;
			return;
		}
		if(r>N-1) return;
		
		for(int j=0; j<N; j++) {
			boolean flag = check(r,j);
			if(flag) {
				board[r][j] = true;
				putQueen(r+1, cnt+1);
				board[r][j] = false;
			}
		}
		
	}
	private static boolean check(int r, int c) {
		for(int i=0; i<N; i++) {
			//가로 세로 라인에 다른 퀸 있으면
			if(board[r][i] || board[i][c]) return false;
			//대각선에  다른 퀸 있으면
			if(r-i>=0 && c-i>=0) {
				if(board[r-i][c-i]) return false;
			}
			if(r-i>=0 && c+i<N) {
				if(board[r-i][c+i]) return false;
			}
		}
		
		return true;
		
	}
	


}
