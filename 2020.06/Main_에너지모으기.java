import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.StringTokenizer;
/*
 * 백준 16198 에너지 모으기
 */
public class Main_에너지모으기 {

	static int max;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		int N = Integer.parseInt(br.readLine());		//구슬 갯수
		tk = new StringTokenizer(br.readLine());
		
		LinkedList<Integer> list = new LinkedList<>();	//구슬 w값 list
		for(int i=0; i<N; i++) {
			list.add(Integer.parseInt(tk.nextToken()));
		}		
		
		max = Integer.MIN_VALUE;
		go(N, list, 0);
		System.out.println(max);

	}

	private static void go(int N, LinkedList<Integer> list, int energy) {
		if(N<=2) {
			if(energy>max) {
				max = energy;
			}
			return ;
		}
		
		for(int i=1; i<N-1 ; i++) {
			int w = list.get(i);
			//구슬 하나 제거
			list.remove(i);
			go(N-1, list, energy+list.get(i-1)*list.get(i));
			//제거한 구슬 돌려 놓기
			list.add(i, w);
		}
	}


}

