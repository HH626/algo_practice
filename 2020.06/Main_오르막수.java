import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
/**
 * 백준 11057 오르막수
 */
public class Main_오르막수 {

	static int N;
	static long D[][], total[];
	public static void main(String[] args) throws NumberFormatException, IOException {
		//D[n번째 자리수][숫자]
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		N = Integer.parseInt(br.readLine());
		D = new long[N+1][10];
		total = new long[N+1];
		
		Arrays.fill(D[0], 1);
		total[0]=10;
		for(int i=1; i<=N; i++) {
			D[i][0] = total[i] = total[i-1]%10007;
			for(int j=1; j<10; j++) {
				D[i][j] = (D[i][j-1]+10007-D[i-1][j-1])%10007;
				total[i] += D[i][j];
			}
		}


		System.out.println(total[N-1]%10007);
	}

}


