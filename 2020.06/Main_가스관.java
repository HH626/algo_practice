import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
/**
 * 백준 2931 가스관
 */
public class Main_가스관 {
	static class Point{
		int r, c, d;
		char pipe;
		Point(){};
		Point(int r, int c, int d, char pipe){
			this.r = r;
			this.c = c;
			this.d = d;
			this.pipe = pipe;
		}
	}
	static int R, C;
	static char[][] map;
	static boolean[][] visited;
	final static int UP=0, RIGHT=1, DOWN=2, LEFT=3;
	static Point Moscow, Zagreb;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		map = new char[R+2][C+2];
		visited = new boolean[R+2][C+2];
		Moscow = new Point();
		Zagreb = new Point();
		 
		
		for(int i=1; i<=R; i++) {
			String line = br.readLine();
			for(int j=1; j<=C; j++) {
				char el = line.charAt(j-1);
				map[i][j] = el;
				if(el=='M') {
					Moscow = new Point(i,j,-1,' ');
				}else if(el=='Z') {
					Zagreb = new Point(i,j,-1,' ');
				}
			}
		}//input
		
		Point a = Moscow;
		Point b = Zagreb;
		for(int d=0; d<4; d++) {
			int r = Moscow.r+dR[d];
			int c = Moscow.c+dC[d];
			if(map[r][c] == '.' || map[r][c] =='\u0000' || map[r][c]=='M' || map[r][c]=='Z') continue;
			a = flow(Moscow.r,Moscow.c,d);
//			Moscow = flow(Moscow.r,Moscow.c,d);
			break;
		}
		for(int d=0; d<4; d++) {
			int r = Zagreb.r+dR[d];
			int c = Zagreb.c+dC[d];
			if(map[r][c] == '.' || map[r][c] =='\u0000' || map[r][c]=='M' || map[r][c]=='Z') continue;
			b = flow(Zagreb.r,Zagreb.c,d);
//			Zagreb = flow(Zagreb.r,Zagreb.c,d);
			break;
		}
		
		Point answer = newPipe(a,b);
		System.out.println(answer.r+" "+ answer.c +" "+ answer.pipe);

	}//main
	
	private static int selectDir(Point a, Point b) {
		if(b.r-a.r == -1) {
			return UP;
		}else if(b.r-a.r == 1) {
			return DOWN;
		}else{
			if(b.c-a.c == -1) {
				return LEFT;
			}else if(b.c-a.c == 1) {
				return RIGHT;
			}
		}
		return -1;
	}//selectDir
	
	/*끊긴 파이프가 어느위치 , 어느모양인지 찾기*/
	private static Point newPipe(Point mosc, Point zag) {
		int r=0,c=0;
		int ad = mosc.d, bd=zag.d;
		char pipe=' ';
		boolean flag = true;
		
		//파이프 놓을 위치 찾기
		if(mosc.r!=zag.r || mosc.c!=zag.c) {
			if(mosc.r==Moscow.r && mosc.c == Moscow.c) {
				r = zag.r;
				c = zag.c;
				ad = selectDir(mosc, zag); //모스크바 방향찾기
			}else if(zag.r==Zagreb.r && zag.c==Zagreb.c) {
				r=mosc.r;
				c=mosc.c;
				bd = selectDir(zag, mosc); //자그라브 방향찾기
			}
		}else {
			r=mosc.r;
			c=mosc.c;
		}
		
		//파이프 모양찾기
		for(int d=0; d<4; d++) {
			int nr = r+dR[d];
			int nc = c+dC[d];
			if(map[nr][nc]=='.' || map[nr][nc] == '\u0000' || map[nr][nc] == 'M' || map[nr][nc] == 'Z') {
				flag = false;
				break;
			}
			
		}
		if(flag) return new Point(r,c,-1,'+');
		
		if((ad == UP && bd == DOWN)|| (ad == DOWN && bd == UP)) {
			pipe = '|';
		}else if((ad == RIGHT && bd == LEFT )|| (ad == LEFT && bd == RIGHT)) {
			pipe = '-';
		}else if((ad == UP && bd == LEFT)|| (ad == LEFT && bd == UP)) {
			pipe = '1';
		}else if((ad == DOWN && bd == LEFT)|| (ad == LEFT && bd == DOWN)) {
			pipe = '2'; //L
		}else if((ad == DOWN && bd == RIGHT)|| (ad == RIGHT && bd == DOWN)) {
			pipe = '3'; //J
		}else if((ad == UP && bd == RIGHT)|| (ad == RIGHT && bd == UP)) {
			pipe = '4'; //ㄱ
		}else {
			pipe = '+';
		}
		
//		System.out.println(mosc.r+" "+mosc.c+" "+pipe );
		return new Point(r,c,-1,pipe);
	}



	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	/*가스관 끊긴데까지 따라가 보기*/
	private static Point flow(int r,int c, int d) {
		boolean flag = false;
		while(!flag) {
		r = r+dR[d];
		c = c+dC[d];
		char el = map[r][c];
			switch(d) {
			case UP: //위
				if(el=='|') {
					d=UP;
				}else if(el=='1') {
					d=RIGHT;
				}else if(el=='4') {
					d=LEFT;
				}else if(el=='+') {
					d=UP;
				}else if(el=='.') {
					flag = true;
				}
				break;
			case RIGHT: //오
				if(el=='-') {
					d=RIGHT;
				}else if(el=='3') {
					d=UP;
				}else if(el=='4') {
					d=DOWN;
				}else if(el=='+') {
					d=RIGHT;
				}else if(el=='.') {
					flag = true;
				}
				break;
			case DOWN: //아래
				if(el=='|') {
					d=DOWN;
				}else if(el=='2') {
					d=RIGHT;
				}else if(el=='3') {
					d=LEFT;
				}else if(el=='+') {
					d=DOWN;
				}else if(el=='.') {
					flag = true;
				}
				break;
			case LEFT: //왼
				if(el=='-') {
					d=LEFT;
				}else if(el=='1') {
					d=DOWN;
				}else if(el=='2') {
					d=UP;
				}else if(el=='+') {
					d=LEFT;
				}else if(el=='.') {
					flag = true;
				}
				break;
			}//switch
		}//while
		
//		System.out.println("r"+r+" c"+c+" d"+d);
		return new Point(r,c,d,' ');
	}//flow

}
