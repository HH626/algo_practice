import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

/*
 * 백준 2580 스도쿠
 */
public class Main_스도쿠 {

	static int[][] map;
	static boolean[][] garo, sero, nemo;
	static ArrayList<int[]> empty;
	static int N;
	static boolean findAns;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		map = new int[10][10];
		garo = new boolean[10][10]; //garo[n][m] n번째 row에 m이라는 수가 있으면 true
		sero = new boolean[10][10]; //sero[n][m] n번째 col에 m이라는 수가 있으면 true
		nemo = new boolean[10][10]; //nemo[n][m] n영역에 m이라는 수가 있으면 true
		empty = new ArrayList<>();	//빈칸의 r,c값
		
		for(int i=1; i<10; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<10; j++) {
				int el = Integer.parseInt(tk.nextToken());
				map[i][j] = el;
				if(el==0) {
					empty.add(new int[] {i,j});
				}else {
					garo[i][el] = true;
					sero[j][el] = true;
					setNemo(i,j,el,true);
				}
			}
		}//input
		
		N = empty.size();	//빈칸 수
		
		go(0);
		
		//output
		for(int i=1; i<10; i++) {
			for(int j=1; j<10; j++) {
				System.out.print(map[i][j]+" ");
			}
			System.out.println();
		}//


	}//main
	private static void go(int index) {
		if(findAns) return;
		if(index>=N) {
			findAns = true;
			return;
		}
		
		int r = empty.get(index)[0];
		int c = empty.get(index)[1];
			
		for(int num=1; num<10; num++) {
			if(garo[r][num] || sero[c][num] || getNemo(r,c,num)) continue; //하나라도 이미 사용된 수이면
			// 시도할 수 있는 수이면
			garo[r][num] =true;
			sero[c][num] = true;
			setNemo(r, c, num, true);
			map[r][c] = num;
			go(index+1);
			if(findAns) return;
			garo[r][num] = false;
			sero[c][num] = false;
			setNemo(r, c, num, false);
		}

	}
	
	private static void setNemo(int i, int j, int el, boolean b) {
//		1 2 3
//		4 5 6
//		7 8 9
		//1
		if(i<=3 && j<=3) {
			nemo[1][el] = b;
		//2
		}else if(i<=3 && j>3 && j<=6) {
			nemo[2][el] = b;
		//3
		}else if(i<=3 && j>6) {
			nemo[3][el] = b;
		//4
		}else if(i>3 && i<=6 && j<=3) {
			nemo[4][el] = b;
		//5
		}else if(i>3 && i<=6 && j>3 && j<=6) {
			nemo[5][el] = b;
		//6
		}else if(i>3 && i<=6 && j>6) {
			nemo[6][el] = b;
		//7
		}else if(i>6 && j<=3) {
			nemo[7][el] = b;
		//8
		}else if(i>6 && j>3 && j<=6) {
			nemo[8][el] = b;
		//9
		}else if(i>6 && j>6) {
			nemo[9][el] = b;
		}

	}//setNemo
	private static boolean getNemo(int i, int j, int el) {
//		1 2 3
//		4 5 6
//		7 8 9
		//1
		if(i<=3 && j<=3) {
			return nemo[1][el];
		//2
		}else if(i<=3 && j>3 && j<=6) {
			return nemo[2][el] ;
		//3
		}else if(i<=3 && j>6) {
			return nemo[3][el] ;
		//4
		}else if(i>3 && i<=6 && j<=3) {
			return nemo[4][el] ;
		//5
		}else if(i>3 && i<=6 && j>3 && j<=6) {
			return nemo[5][el] ;
		//6
		}else if(i>3 && i<=6 && j>6) {
			return nemo[6][el] ;
		//7
		}else if(i>6 && j<=3) {
			return nemo[7][el] ;
		//8
		}else if(i>6 && j>3 && j<=6) {
			return nemo[8][el] ;
		//9
		}else if(i>6 && j>6) {
			return nemo[9][el] ;
		}
		return false;

	}//getNemo

}
