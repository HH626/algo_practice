import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
/**
 * 백준 18428 감시피하기
 */
public class Main_감시피하기 {

	static int N;
	static char[][] map;
	static ArrayList<int[]> teacher;
	static boolean answer;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		N = Integer.parseInt(br.readLine());
		
		map = new char [N+2][N+2];
		teacher = new ArrayList<>();
		
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				char el = tk.nextToken().charAt(0);
				map[i][j] = el;
				if(el=='T') {
					teacher.add(new int[] {i,j});
				}
			}
		}

		putOb(1,1,0);
		System.out.println(answer? "YES":"NO");
	}//main

	/*장애물 3개 놓고 학생이 감시 피할수 있는지 확인*/
	private static void putOb(int r, int c, int cnt) {
		if(answer) return;
		if(cnt>=3) {
			answer = studentHide();
			return;
		}
		
		for(int i=r; i<=N; i++) {
			if(i>r) {c=1;}
			for(int j=c; j<=N; j++) {
				if(map[i][j]=='X') {
					map[i][j] = 'O';
					putOb(i,j,cnt+1);
					if(answer) return;
					map[i][j] = 'X';
				}
			}
		}
		
	}

	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	/*감시로 부터 피했으면(학생이 안걸리면) true, 걸리면 false 리턴 */
	private static boolean studentHide() {	
		int i=0, len=teacher.size();
		
		while(i<len) {
		int r = teacher.get(i)[0];
		int c = teacher.get(i)[1];
			for(int d=0; d<4; d++) {
				int cnt = 1;
				while(true) {
					int nr = r+dR[d]*cnt;
					int nc = c+dC[d]*cnt;
					if(map[nr][nc]=='O' || map[nr][nc]=='\u0000') break;
					if(map[nr][nc]=='S') {
						return false;
					}
					cnt++;
				}//while
			}//for
			i++;
		}//while
		return true;
	}

}
