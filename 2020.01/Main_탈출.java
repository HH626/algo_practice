import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 3055 탈출
 */
public class Main_탈출 {

	static int R, C;
	static char map[][];
	static Queue<int[]> go, water;
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	static boolean visited[][];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		go = new LinkedList<int[]>();
		water = new LinkedList<int[]>();
		
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		map = new char[R+2][C+2];
		visited = new boolean[R+2][C+2];
		
		String line; 
		char ch;
		for(int i=1; i<=R; i++) {
			line = br.readLine();
			for(int j=1; j<=C; j++) {
				ch = line.charAt(j-1);
				map[i][j] =ch;
				if(ch=='S') {
					visited[i][j] = true;
					go.offer(new int[] {i,j});
				}else if(ch=='*') {
					water.offer(new int[] {i,j});
				}
			}
		}
		
		boolean flag = false;
		int count=0;
		while(!go.isEmpty() && flag==false) {
		//물 
		int a[], r, c, nr, nc;
		int size = water.size();
		while(size-->0) {
			a = water.poll();
			r=a[0];
			c=a[1];
			for(int d=0; d<4; d++) {
				nr = r+dR[d];
				nc = c+dC[d];
				if(map[nr][nc]=='.') {
					water.offer(new int[] {nr, nc});
					map[nr][nc]='*';
				}
			}
		}//water
		
		//고슴도치 움직임
		size = go.size();
		while(size-->0) {
			
			a = go.poll();
			r=a[0];
			c=a[1];
			for(int d=0; d<4; d++) {
				nr = r+dR[d];
				nc = c+dC[d];
				if(map[nr][nc]=='.' && !visited[nr][nc]) {
					visited[nr][nc] = true;
					go.offer(new int[] {nr, nc});
				}else if(map[nr][nc]=='D') {
					flag = true;
					break;
				}
				
			}
			if(flag) break;
		}//go 
		
		count++;
		
		}//while
		
		System.out.println(flag==true? count: "KAKTUS");
	
	}//main

}
