import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 1726 로봇
 * bfs
 */
public class Main_로봇 {
	static int R, C, map[][];
	static int startR, startC, startDir, endR, endC, endDir;
	static final int E=1, W=2, S=3, N=4; 
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		map = new int[R+2][C+2];
		visited = new boolean[R+2][C+2][4];
		for(int i=0; i<R+2; i++) {
			Arrays.fill(map[i], 1);
			if(i==0 || i==R+1) continue;
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=C; j++) {
				map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		
		tk = new StringTokenizer(br.readLine());
		startR = Integer.parseInt(tk.nextToken()); 
		startC = Integer.parseInt(tk.nextToken()); 
		startDir = Integer.parseInt(tk.nextToken()); 
		startDir = changeDir(startDir);
		
		tk = new StringTokenizer(br.readLine());
		endR = Integer.parseInt(tk.nextToken()); 
		endC = Integer.parseInt(tk.nextToken()); 
		endDir = Integer.parseInt(tk.nextToken()); 
		endDir = changeDir(endDir);
		
		
		/////
		map[startR][startC] = 2;
		bfs(startR, startC, startDir, 2);
		
		System.out.println(map[endR][endC]-2);
	}//main
	
	
	public static int[] dR = {-1,0,1,0}; //북, 동, 남, 서
	public static int[] dC = {0,1,0,-1};
	public static boolean visited[][][];
	public static void bfs(int r, int c, int dir, int count) {
//		PriorityQueue<int[]> queue = new PriorityQueue(new Comparator<int[]>() {
//			public int compare(int[] o1, int[] o2) {
//				return o1[3]-o2[3];
//			}
//		});
		
		Queue<int[]> queue = new LinkedList<int[]>();
		int nr, nc, ndir;
		
		queue.offer(new int[] {r,c,dir,count});
		while(!queue.isEmpty()) {
			int p[] = queue.poll();
			r=p[0];
			c=p[1];
			dir = p[2];
			count = p[3];
			
			
			if(r==endR && c==endC && dir == endDir) {
					map[r][c] = count;
					break;
			}
			
			for(int d=0; d<4; d++){
				ndir = (d+dir)%4;
				nr = r +dR[ndir];
				nc = c +dC[ndir];
				
				switch(d) {
				case 0: //지금 방향 그대로
					for(int i=1; i<=3; i++) {
						nr = r +dR[ndir]*i;
						nc = c +dC[ndir]*i;
//						if(nr>R || nr<1 ||nc>C || nc<1) break;
						if(map[nr][nc]==1) break;
						if(map[nr][nc]==0 || !visited[nr][nc][dir]) {
							visited[nr][nc][dir]=true;
							queue.offer(new int[] {nr, nc, ndir, count+1});
							map[nr][nc] = count+1;
						}
					}
					break;
					
				case 1: //지금방향 오른쪽
				case 3: //지금방향 왼쪽
					if(!visited[r][c][ndir]) {
						visited[r][c][ndir]=true;
						queue.offer(new int[] {r, c, ndir, count+1});
					}
					break;
					
				case 2: //지금방향 반대쪽
					if(!visited[r][c][ndir]) {
						visited[r][c][ndir]=true;
						queue.offer(new int[] {r, c, ndir, count+2});
					}
					break;
				}//switch
				
				
				
			}//for
		}//while
	}//bfs
	

	
	private static int changeDir(int dir) {
		switch(dir) {
		case 1: //동
			dir = 1;
			break;
		case 2: //서
			dir = 3;
			break;
		case 3: //남
			dir = 2;
			break;
		case 4: //북
			dir = 0;
			break;
		
		}
		
		return dir;
		
	}// changeDir
	
	
}










//public static int[] dR = {-1,0,1,0}; //북, 동, 남, 서
//public static int[] dC = {0,1,0,-1};
//public static void bfs(int r, int c, int dir, int count) {
//	Queue<int[]> queue = new LinkedList<>();
//	int nr, nc, ndir;
//	
//	queue.offer(new int[] {r,c,dir,count});
//	while(!queue.isEmpty()) {
//		int p[] = queue.poll();
//		r=p[0];
//		c=p[1];
//		dir = p[2];
//		count = p[3];
//		
//		if(map[r][c]==0 || map[r][c] > count) {
//		
//			///go();
//			for(int i=1; i<=3; i++) {
//				nr = r +dR[dir]*i;
//				nc = c +dC[dir]*i;
//				if(map[nr][nc]!=1) {
//					queue.offer(new int[] {nr, nc, dir, count+1});
//				}
//			}
//			///turn right;
//			ndir = (dir+1)%4;
//			queue.offer(new int[] {r, c, ndir, count+1});
//			
//			///turn left;
//			ndir = (dir+3)%4;
//			queue.offer(new int[] {r, c, ndir, count+1});
//		
//		}
//
//	}//while
//}//bfs





