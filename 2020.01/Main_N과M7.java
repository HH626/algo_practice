import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * ���� 15656 N��M(7)
 */
public class Main_N��M7 {

	static int N, M, arr[], arr2[];
	static StringBuilder sb;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		sb = new StringBuilder();
		
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());
		M = Integer.parseInt(tk.nextToken());
		
		arr = new int[N];
		arr2 = new int[M];
		tk = new StringTokenizer(br.readLine());
		for(int i=0; i<N; i++) {
			arr[i] = Integer.parseInt(tk.nextToken());
		}
		
		Arrays.sort(arr);
		
		for(int i=0; i<N; i++) {
			arr2[0] = arr[i];
			dfs(1);
		}
		System.out.println(sb);
	}//main
	private static void dfs(int c) {
		if(c==M) {
			for(int i=0; i<c; i++) {
				sb.append(arr2[i]).append(" ");
			}
			sb.append("\n");
			return;
		}
		for(int i=0; i<N; i++) {
			arr2[c] = arr[i];
			dfs(c+1);
		}
	}

}
