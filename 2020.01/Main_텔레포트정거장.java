import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;
/**
 * 백준 18232 텔레포트 정거장
 */
public class Main_텔레포트정거장 {
	static int N,M,S,E;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());	//길 총길이
		M = Integer.parseInt(tk.nextToken());	//텔레포트 갯수
		LinkedList<Integer>[] arr = new LinkedList[N+1];
		for(int i=1; i<=N; i++) {
			arr[i] = new LinkedList<>();
		}
		boolean[] visited = new boolean[N+1];
		
		tk = new StringTokenizer(br.readLine());
		S = Integer.parseInt(tk.nextToken());	//시작점
		E = Integer.parseInt(tk.nextToken());	//도착점
		
		int x, y;
		for(int i=0; i<M; i++) {
			tk = new StringTokenizer(br.readLine());
			x = Integer.parseInt(tk.nextToken());
			y = Integer.parseInt(tk.nextToken());
			arr[x].add(y);
			arr[y].add(x);
		}
		
		int n,cnt=-1;
		Queue<Integer> queue = new LinkedList<>();
		
		queue.offer(S);
		n = queue.size();
		while(!queue.isEmpty()) {
			cnt++;
			while(n-->0) {
				x = queue.poll();
				if(x==E) {
					System.out.println(cnt);
					queue.clear();
					break;
				}
				
				if(x-1>0 && !visited[x-1]) {
					queue.offer(x-1);
					visited[x-1] = true;
				}
				if(x+1<=N && !visited[x+1]) {
					queue.offer(x+1);
					visited[x+1] = true;
				}
				for(int i=0; i<arr[x].size(); i++) {
					y = arr[x].get(i);
					if(y>0 && !visited[y]) {// 텔레포트 연결
						queue.offer(y);
						visited[y] = true;
					}else if(y==0){
						break;
					}
				}
			
			}
			n = queue.size();
		}
		
	}// main

}
