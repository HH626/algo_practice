import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/*
 * 백준 1938 통나무 옮기기
 * dfs로 하면 터짐
 */
public class Main_통나무옮기기 {
	static class Point{
		int r, c;
		int dir;
		int cnt;
		
		Point(int r, int c, int dir, int cnt){
			this.r = r;
			this.c = c;
			this.dir = dir;//세로1 가로2
			this.cnt = cnt;
		}
	}
	
	static int N;
	static char originmap[][];
	static int map[][];
	static boolean visited[][][];
	static int answer;
	static Point start, end;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		N = Integer.parseInt(br.readLine());
		originmap = new char[N+2][N+2];
		map = new int[N+2][N+2];
		visited = new boolean[N+2][N+2][3];
		answer = Integer.MAX_VALUE;
		
		String line;
		for(int i=0; i<N+2; i++) {
			Arrays.fill(originmap[i],'1');
			if(i==0 || i==N+1) continue;
			line = br.readLine();
			for(int j=1; j<=N; j++) {
				originmap[i][j] = line.charAt(j-1);
			}
		}//for
		
		start=null;
		end=null;
		for(int i=0; i<N+2; i++) {
			Arrays.fill(map[i],1);
			if(i==0 || i==N+1) continue;
			for(int j=1; j<N+2; j++) {
				if(originmap[i][j]=='0' || originmap[i][j]=='B' || originmap[i][j]=='E') {
					map[i][j] = 0;
				}else if(originmap[i][j]=='1') {
					map[i][j] = 1;
				}
				
				if(start==null && originmap[i][j]=='B') {
					if(originmap[i+1][j]=='B') {
						start = new Point(i+1,j,1,0);
					}else if(originmap[i][j+1]=='B') {
						start = new Point(i,j+1,2,0);
					}
				}
				if(end==null && originmap[i][j]=='E') {
					if(originmap[i+1][j]=='E') {
						end = new Point(i+1,j,1,-1);
					}else if(originmap[i][j+1]=='E') {
						end = new Point(i,j+1,2,-1);
					}
				}
			}
		}//for
		
		
		go(start);
		System.out.println(answer==Integer.MAX_VALUE? 0: answer);
		
	}//main
	
	
	static Queue<Point> queue = new LinkedList<Main.Point>();
	public static void go(Point start) {
		Point p;
		int r, c, dir, cnt;
		queue.offer(start);
		
		while(!queue.isEmpty()) {
			p = queue.poll();
			r = p.r;
			c = p.c;
			cnt = p.cnt;
			dir = p.dir;
			
			if(r==end.r && c==end.c && dir==end.dir) {
				answer = cnt;
				return;
			}
			
			
			if(dir==1) {//세로모양일때
				switch('U') {
				case 'U':
					if(!visited[r-1][c][dir] && map[r-2][c]==0) {
						visited[r-1][c][dir] = true;
						queue.offer(new Point(r-1,c,dir,cnt+1));
					}
				case 'D':
					if(!visited[r+1][c][dir] && map[r+2][c]==0) {
						visited[r+1][c][dir] = true;
						queue.offer(new Point(r+1,c,dir,cnt+1));
					}
				case 'L':
					if(!visited[r][c-1][dir] && map[r-1][c-1]+map[r][c-1]+map[r+1][c-1]==0) {
						visited[r][c-1][dir] = true;
						queue.offer(new Point(r,c-1,dir,cnt+1));
					}
				case 'R':
					if(!visited[r][c+1][dir] && map[r-1][c+1]+map[r][c+1]+map[r+1][c+1]==0) {
						visited[r][c+1][dir] = true;
						queue.offer(new Point(r,c+1,dir,cnt+1));
					}
				case 'T':
					if(!visited[r][c][2]
							&& map[r-1][c-1]+map[r][c-1]+map[r+1][c-1]+map[r-1][c+1]+map[r][c+1]+map[r+1][c+1]==0) {
						visited[r][c][2] = true;
						queue.offer(new Point(r,c,2,cnt+1));
					}
				}//switch
			}else if(dir==2) {
				switch('U') {
				case 'U':
					if(!visited[r-1][c][dir] && map[r-1][c-1]+map[r-1][c]+map[r-1][c+1]==0) {
						visited[r-1][c][dir] = true;
						queue.offer(new Point(r-1,c,dir,cnt+1));
					}
				case 'D':
					if(!visited[r+1][c][dir] && map[r+1][c-1]+map[r+1][c]+map[r+1][c+1]==0) {
						visited[r+1][c][dir] = true;
						queue.offer(new Point(r+1,c,dir,cnt+1));
					}
				case 'L':
					if(!visited[r][c-1][dir] && map[r][c-2]==0) {
						visited[r][c-1][dir] = true;
						queue.offer(new Point(r,c-1,dir,cnt+1));
					}
				case 'R':
					if(!visited[r][c+1][dir] && map[r][c+2]==0) {
						visited[r][c+1][dir] = true;
						queue.offer(new Point(r,c+1,dir,cnt+1));
					}
				case 'T':
					if(!visited[r][c][1] 
							&& map[r-1][c-1]+map[r-1][c]+map[r-1][c+1]+map[r+1][c-1]+map[r+1][c]+map[r+1][c+1]==0) {
						visited[r][c][1]  = true;
						queue.offer(new Point(r,c,1,cnt+1));
					}
				}//switch
			}
			
			
		}//while
		
	}//go
	

}
