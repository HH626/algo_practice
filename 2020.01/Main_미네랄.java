import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 2933 미네랄
 */
public class Main_미네랄 {
	static int R,C,N,arr[];
	static char[][] map;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		map = new char[R+2][C+2];
		
		String line;
		for (int i = R; i >0 ; i--) {
			line = br.readLine();
			for (int j = 1; j <= C; j++) {
				map[i][j] = line.charAt(j-1);
			}
		}
		
		N = Integer.parseInt(br.readLine());
		arr = new int[N];
		tk = new StringTokenizer(br.readLine());
		for(int i=0; i<N; i++) {
			arr[i] = Integer.parseInt(tk.nextToken());
		}
		
		int dir=-1;
		for(int i=0; i<N; i++) {
			//1.막대던지기
			int height = arr[i];
			dir= dir*-1;
			throwStick(dir, height);
			//2.땅에 붙어있는 미네랄 표시
			bfs();
			//3.공중에 있는 미네랄 바닥으로 내리기
			drop();
			//4.x로 다시 정리
			cover();
		}
		
		
		//output
		for(int i=R; i>0; i--) {
			for(int j=1; j<=C; j++) {
				System.out.print(map[i][j]);
			}
			System.out.println();
		}
		
	}//main

	static void throwStick(int dir, int height) {
		switch(dir) {
		case 1: //왼
			for(int j=1; j<=C; j++ ) {
				if(map[height][j]=='x') {
					map[height][j] = '.';
					break;
				}
			}
			break;
		case -1: //오
			for(int j=C; j>0; j--) {
				if(map[height][j]=='x') {
					map[height][j] = '.';
					break;
				}
			}
			break;
		}
	}
	
	static int[] dirR = {-1,0,1,0};
	static int[] dirC = {0,1,0,-1};
	static void bfs() {	//바닥에 붙어있는지 확인 (붙어있으면 'o'로 표시)
		int r, c, curR, curC;
		Queue<Integer> queueR = new LinkedList<Integer>();
		Queue<Integer> queueC = new LinkedList<Integer>();
		
		for(int j=1; j<=C; j++) {
			if(map[1][j]=='o'|| map[1][j]=='.') continue;
			queueR.offer(1);
			queueC.offer(j);
			while(!queueR.isEmpty()) {
				r = queueR.poll();
				c = queueC.poll();
				for(int d=0; d<4; d++) {
					curR = r+dirR[d];
					curC = c+dirC[d];
					if(map[curR][curC]=='x') {
						map[curR][curC]='o';
						queueR.offer(curR);
						queueC.offer(curC);						
					}
					
				}
				
			}//while
		}//for
		
	}//bfs
	
	static void drop() { //미네랄 바닥으로 내리기 (두개이상의 클러스터가 동시에 떨어지는 경우 없음)
		int cnt=0, min=100;
		
		//몇칸 내릴지 찾기
		for(int j=1; j<=C; j++) {
			cnt=0;
			for(int i=1; i<=R; i++) {
				if(map[i][j]=='.') {
					cnt++;
				}else if(map[i][j]=='o') {
					cnt=0;
				}else if(map[i][j]=='x') {
					if(min>cnt) {
						min = cnt;
					}
					break;
				}
			}
		}//for
		
		//내릴 클러스터 찾아 내리기
		for(int j=1; j<=C; j++) {
			for(int i=1; i<=R; i++) {
				if(map[i][j]=='x') {
					map[i-min][j]='o';
					map[i][j]='.';
				}
			}
		}//for
		
		
	}//drop
	
	static void cover() {
		for(int i=1; i<=R; i++) {
			for(int j=1; j<=C;j++) {
				if(map[i][j] == 'o') {
					map[i][j] = 'x';
				}
			}
		}
	}//cover
	
}
