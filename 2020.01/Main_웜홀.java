import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * 백준 1865 웜홀
 * 벨만포드
 */
public class Main_웜홀 {
	static class Edge{
		int s, e, t;
		Edge(){};
		Edge(int s, int e, int t){
			this.s = s;
			this.e = e;
			this.t = t;
		}
	}

	static int N,M,W;
	static List<Edge> list;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());
			M = Integer.parseInt(tk.nextToken());
			W = Integer.parseInt(tk.nextToken());
			list = new LinkedList<Main.Edge>();
			
			int s, e, t;
			//도로
			for(int i=0; i<M; i++) {
				tk = new StringTokenizer(br.readLine());
				s = Integer.parseInt(tk.nextToken());
				e = Integer.parseInt(tk.nextToken());
				t = Integer.parseInt(tk.nextToken());
				list.add(new Edge(s,e,t));
				list.add(new Edge(e,s,t));
			}
			//웜홀
			for(int i=0; i<W; i++ ) {
				tk = new StringTokenizer(br.readLine());
				s = Integer.parseInt(tk.nextToken());
				e = Integer.parseInt(tk.nextToken());
				t = Integer.parseInt(tk.nextToken());
				list.add(new Edge(s,e,-t));
			}
			
			String answer = "NO";
			for(int i=1; i<=2; i++) {
				if(bellman(i)) {
					answer = "YES";
					break;
				}
			}
			
			System.out.println(answer);
			
		}//tc
	}//main
	private static boolean bellman(int start) {
		int[] dest = new int[N+1];
		Arrays.fill(dest, Integer.MAX_VALUE);
		dest[start] = 0;
		
		//간선 수만큼
		for(int i=1, size = list.size(); i<=size; i++) {
			for (Edge edge : list) {
				if(dest[edge.s]==Integer.MAX_VALUE) continue; // start에서 s로 갈 수 없으면 컨티뉴
				
				//갈 수 있는 지점이면 s에 들러서 e를 가는게 빠른지 start에서 바로e에 가는게 빠른지 비교
				if(dest[edge.s]+edge.t < dest[edge.e]) {
					dest[edge.e] = dest[edge.s]+edge.t;
					if(i==size) {return true;}
				}
			}
		}
		
		return false;
		
	}//bellman

}
