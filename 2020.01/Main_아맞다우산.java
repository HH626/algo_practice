import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 17244 아 맞다 우산
 */
public class Main_아맞다우산 {
	static class Point{
		int r, c, time;
		Point(int r, int c, int time){
			this.r= r;
			this.c= c;
			this.time = time;
		}
	}
	static int R,C,N,MIN;
	static char map[][], seq[];
	static Point start;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		C = Integer.parseInt(tk.nextToken());
		R = Integer.parseInt(tk.nextToken());
		
		map = new char[R][C];
		seq = new char[6];			//물건 찾는 순서,  n번째에 seq[n]찾기, n은 1부터
		N=0;						//찾을 물건 개수
		
		for(int i=0; i<R; i++){
			String line = br.readLine();
			for(int j=0; j<C; j++) {
				char ct = line.charAt(j);
				if(ct=='X') {
					N++;
					ct = (char)('0'+ N);
					seq[N] = ct;
				}else if(ct=='S') {
					start = new Point(i,j,0);
				}
				map[i][j] = ct;
			}
		}//input
		
		MIN = 9999999;
		//1.물건 순서 정하기
		do{
			//2.정한 순서데로 가장 빨리 가지고 나오기
			int result = bfs();
			MIN = Math.min(MIN, result);
			
		}while(np(seq,N));
		
		
		//output
		System.out.println(MIN);
		
	}//main
	
	static int[] dR = {-1,0,1,0};
	static int[] dC = {0,1,0,-1};
	private static int bfs() {
		Queue<Point> que = new LinkedList<Point>();
		boolean visited[][] = new boolean[R][C];
		
		char flag = N==0? '#':'E';
		int idx=1;
		que.offer(start);
		visited[start.r][start.c] = true;

		while(!que.isEmpty()) {
			Point p = que.poll();
			int r = p.r;
			int c = p.c;
			int time = p.time;
			
			//찾으려는 물건 일때
			if(idx<=N) {
				if(map[r][c] == seq[idx]) {
					idx++;
					visited = new boolean[R][C];
					que.clear();
					//마지막 물건
					if(idx>N) {
						flag = '#';
					}
				}
			}
			else {
				if(map[r][c] == 'E') return time;
			}
			
			for(int d=0; d<4; d++) {
				int nr = r+dR[d];
				int nc = c+dC[d];
				
				if(map[nr][nc]=='#' || visited[nr][nc] || map[nr][nc]==flag) continue;
				
				visited[nr][nc] = true;
				que.offer(new Point(nr, nc, time+1));
			}
			
		}//while
		
		return -1;
		
	}//bfs
	
	private static boolean np(char[] arr, int N) {
		int i=N;
		
		while(true) {
			if(i<=1) return false;
			if(i-1>0 && arr[i-1]<arr[i]) break;
			i--;
		}
		
		int j=N;
		while(true) {
			if(arr[i-1]<arr[j]) {
				swap(i-1,j,arr);
				break;
			}else {
				j--;
			}
		}
		
		j=N;
		while(true) {
			if(i>=j) break;
			swap(i,j,arr);
			i++;
			j--;
		}
		
		return true;
		
	}//np
	
	private static void swap(int i, int j, char[] arr){
		char tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}

}
