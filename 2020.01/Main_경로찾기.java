import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 11403 경로찾기
 */
public class Main_경로찾기 {

	static int N;
	static int[][] adj, map;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		N = Integer.parseInt(br.readLine());
		adj = new int[N+1][N+1];
		map = new int[N+1][N+1];
		
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				adj[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		
		//
		Queue<Integer> queue = new LinkedList<Integer>();
		int s;
		for(int i=1; i<=N; i++) {
			boolean visited[] = new boolean[N+1];
			queue.offer(i);
			while(!queue.isEmpty()) {
				s = queue.poll();
				for(int j=1; j<=N; j++) {
					if(!visited[j] && adj[s][j]==1) {
						map[i][j] = 1;
						visited[j] = true;
						queue.offer(j);
					}
				}
			}//while
		}
		
		//output
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				System.out.print(map[i][j] + " ");
			}
			System.out.println();
		}
		
		
	}//main


}
