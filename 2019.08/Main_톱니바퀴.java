package aug4;
/**
 * 백준 14891 톱니바퀴
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_톱니바퀴 {

	public static int[][] gear;
	public static boolean[] samePole= new boolean[4];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		String line;
		//톱니바퀴 4개 세팅
		gear = new int[5][8]; //톱니바퀴
		for (int i = 1; i <= 4; i++) {
			line = br.readLine();
			for (int j = 0; j < 8; j++) {
				gear[i][j] = Integer.parseInt(line.charAt(j) + "");
			}
		}
		//돌릴 톱니와 방향
		int K = Integer.parseInt(br.readLine());
		int gearNo, ro;
		for (int i = 0; i < K; i++) {
			line = br.readLine();
			tk = new StringTokenizer(line);
			gearNo = Integer.parseInt(tk.nextToken()); //돌릴 톱니바퀴 번호
			ro = Integer.parseInt(tk.nextToken());		//돌릴 방향 
			go(gearNo, ro); //톱니 돌리기
		}

		int result=0;
		for(int i=1; i<=4; i++) {
			if(gear[i][0]==1) {
				result += Math.pow(2, i-1);
			}
		}
		System.out.println(result);
	}// main

	public static void go(int gearNo, int ro) {
		int noL = gearNo-1, noR = gearNo; 
		int roL =ro, roR = ro;
		
		check(); // 붙어있는 극이 같은지 다른지 확인
		
		rotate(gear[gearNo],ro);
		while(true) {// 왼쪽 톱니바퀴
			if(noL<1 || samePole[noL]) { // 톱니가 없으면  or 같은 극이면
				break;
			}else {
				roL = roL*(-1);
				rotate(gear[noL],roL);
				noL--;
			}			
		}
		while(true) {// 오른쪽 톱니바퀴
			if(noR>3 || samePole[noR]) { // 톱니가 없으면  or 같은 극이면
				break;
			}else {
				roR = roR*(-1);
				rotate(gear[noR+1],roR);
				noR++;
			}			
		}
		
		
	}
	
	
	public static void check() {
		
		for(int i=1; i<4; i++) {
			if(gear[i][2]==gear[i+1][6]) { //붙어있는 극이 같으면
				samePole[i]=true;
			}else {
				samePole[i]=false;
			}
		}
	}//check
	
	public static void rotate(int[] arr, int d) { //톱니 돌리기 1시계, -1반시계
		int temp;
		if (d == 1) {// 시계방향
			temp = arr[7];
			for (int i = 7; i > 0; i--) {
				arr[i] = arr[i - 1];
			}
			arr[0] = temp;
		} else if (d == -1) {
			// 반시계방향
			temp = arr[0];
			for (int i = 0; i < 7; i++) {
				arr[i] = arr[i + 1];
			}
			arr[7] = temp;
		}
	}

}
