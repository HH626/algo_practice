package aug2;
import java.util.Scanner;
/**
 * 백준 1012 유기농 배추
 *
 */
public class baechu {

	static int[][] map;
	static boolean[][] visited;
	static int count;
	static int N,M;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int t = 1; t<=T; t++) {
			count=0;
			//input
			M= sc.nextInt();
			N= sc.nextInt();
			map = new int[N][M];
			visited = new boolean[N][M];
			int K= sc.nextInt();
			int x,y;
			for(int i=0; i<K; i++) {
				x = sc.nextInt();
				y = sc.nextInt();
				map[y][x] = 1;
			}
			
			//
			for(int r=0; r<N; r++) {
				for(int c=0; c<M; c++) {
					if(map[r][c]==0) {continue;}  //배추가 없거나
					else if(visited[r][c]==true) {continue;} //dfs방문한 적이 있는
					else { //곳이 아니라면 dfs
						dfs(r,c);
						count++;
					}
				}
			}
			System.out.println(count);
			//
			
		}//tc			
	}//main
	
	static int[] dirR = {-1,0,1,0};//상우하좌
	static int[] dirC = {0,1,0,-1};
	public static void dfs(int r, int c) {
		if(map[r][c]==0 || visited[r][c] == true) {
			return;
		}
		
		visited[r][c]=true;
		int nextR, nextC;
		for(int i=0; i<4; i++) {
			nextR = r+dirR[i];
			nextC = c+dirC[i];
			if(nextR>=0 && nextR<N && nextC>=0 && nextC<M) {
				dfs(nextR, nextC);				
			}
		}
		return;
	}


}
