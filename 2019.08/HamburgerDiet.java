package aug3;
/**
 * swea 5215 햄버거 다이어트
 */
import java.util.Scanner;

public class HamburgerDiet {

	public static int[] point;
	public static int[] kal;
	public static int N,L, MAXpoint = Integer.MIN_VALUE;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int t=1; t<=T; t++) {
			MAXpoint = Integer.MIN_VALUE;
			
			N=sc.nextInt();
			L=sc.nextInt();
			
			point = new int[N];
			kal = new int[N];
			for(int i=0; i<N; i++) {
				point[i] = sc.nextInt();
				kal[i] = sc.nextInt();
			}
			
			dfs(0,0,0);
			
			System.out.println("#"+t+" "+MAXpoint);
			
		}//tc
		
	}//main
	public static void dfs(int i,int pointSum, int kalSum) {
		if(kalSum>L ) {
			return;
		}
		
		if(i==N) {
			if(pointSum>MAXpoint) {
				MAXpoint = pointSum;
			}
			return;
		}
		
		dfs(i+1,pointSum,kalSum);
		dfs(i+1,pointSum+point[i],kalSum+kal[i]);
		
	}//dfs

}
