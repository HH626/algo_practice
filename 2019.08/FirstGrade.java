package aug3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 5557 1학년
 *
 */
public class FirstGrade {

	static long result=0;
	static int N;
	static int[] numbers;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		//input
		N = Integer.parseInt(br.readLine());
		numbers = new int[N];
		String line = br.readLine();
		tk = new StringTokenizer(line," ");
		for(int i=0; i<N; i++) {
			numbers[i] = Integer.parseInt(tk.nextToken());
		}
		
		//
		bfs(0,0);
		
		//output
		System.out.println(result);
	}//main
	
	public static void bfs(int n, int total) {
		if(total < 0 || total >20) { //중간에 값이 음수거나 20보다 크면 끝
			return;
		}
		
		if(N-1==n) { 
			if(numbers[N-1]==total) { //등식이 성립하면
				result++;
			}
			return;
		}
		
		 bfs(n+1, total + numbers[n]);
		 bfs(n+1, total - numbers[n]);
	}

}
