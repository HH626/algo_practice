package aug3;

import java.util.Scanner;

/**
 * 백준 1018 체스판 다시 칠하기
 *
 */
public class Chess {

	public static char[][] arr;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int R = sc.nextInt();
		int C = sc.nextInt();
		arr = new char[R][C];
		for(int r=0; r<R; r++) {
			String line = sc.next();
			for(int c=0; c<C; c++) {
				arr[r][c] = line.charAt(c);
			}
		}
		
		//
		int MIN = Integer.MAX_VALUE;
		int a, b;
		for(int r=0; r<=R-8; r++) {
			for(int c=0; c<=C-8; c++) {
				a = white(r,c);
				b = black(r,c);
				if(MIN>a) {
					MIN=a;
				}
				if(MIN>b) {
					MIN=b;
				}
			}
		}
		
		
		System.out.println(MIN);

	}//main
	
	public static int white(int r, int c) {
		int count=0;
		for(int i=r; i<r+8; i++) {
			for(int j=c; j<c+8; j++) {
				if((i+j)%2==0) { //짝수
					if(arr[i][j] != 'B') {count++;}
				}
				else {//홀수
					if(arr[i][j] != 'W') {count++;}
				}
			}
		}
		return count;
	}//white
	
	public static int black(int r, int c) {
		int count=0;
		for(int i=r; i<r+8; i++) {
			for(int j=c; j<c+8; j++) {
				if((i+j)%2==0) { //짝수
					if(arr[i][j] != 'W') {count++;}
				}
				else {//홀수
					if(arr[i][j] != 'B') {count++;}
				}
			}
		}
		return count;
	}//black

}
