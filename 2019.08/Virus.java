package aug2;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * 백중 2606 바이러스
 * dfs,bfs
 */
public class Virus {
	public static boolean[] visited;
	public static int N;
	public static LinkedList<Integer>[] list; 
	public static int count;
	public static LinkedList<Integer> queue = new LinkedList<>();
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		count =0;
		//input
		N = sc.nextInt();//컴퓨터수
		list = new LinkedList[N+1];
		for(int i=0; i<=N; i++) {
			list[i] = new LinkedList<Integer>();		
		}
		visited = new boolean[N+1];
		int K = sc.nextInt();//연결쌍수	
		int x,y;
		for(int i=0; i<K; i++) {
			x= sc.nextInt();
			y= sc.nextInt();
			list[x].add(y);
		}
		//
		visited[1]=true;
		bfs(1);
		
		//output
		System.out.println(count);
		
		
	}//main
	
	
	public static void bfs(int num) {		

		for(int i=0, size = list[num].size(); i<size; i++) {
			int k = list[num].get(i);
			if(visited[k]==false) {//방문한 적이 없다면 큐에 넣기
				queue.add(k);
				count++;
				visited[k]=true;
			}
		}
		if(queue.isEmpty()) return;
		bfs(queue.remove());
	}
	


}
