package aug2;
/**
 * 백준 14503 로봇청소기
 *
 */
import java.util.Scanner;

public class RobotVacuum {

	public static int d;
	public static int[][] map;
	public static int count=0;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		int M = sc.nextInt();
		int r = sc.nextInt();
		int c = sc.nextInt();
		d = sc.nextInt();
		map = new int[N][M];
		for(int i=0; i<N; i++) {
			for(int j=0; j<M; j++) {
				map[i][j] = sc.nextInt();
			}
		}
		
		map[r][c]=2;//청소함
		count++;
		while(true) {
		
			int i=0;
			for(i=0; i<4; i++) { //네방향 돌면서 확인
				turn();
				if(checkRoom(r,c,d) == 0) {					
					r=r+dirR[d];
					c=c+dirC[d];
					map[r][c]=2;//청소함
					count++;
					break;
				}
			}
			
			if(i==4) {
				int back = (d+2)%4;
				if(checkRoom(r,c,back)==1) {break;}
				else {
					r=r+dirR[back];
					c=c+dirC[back];
				} 
			}
			
			
		}

		System.out.println(count);
		
		
		
	}//main
	

	
	static int[] dirR= {-1, 0, 1, 0};
	static int[] dirC= {0, 1, 0, -1};
	
	public static int checkRoom(int r, int c, int dir) {
		int frontR, frontC;
		frontR = r+dirR[dir];
		frontC = c+dirC[dir];
		
		return map[frontR][frontC];
	}

	public static void turn() {

		if(d==0) {d = 3;}
		else {d--;}

	}

}
