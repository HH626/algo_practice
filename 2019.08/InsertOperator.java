package aug2;


import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * 백준 14888 연산자 끼워넣기
 *
 */
public class InsertOperator {

	public static int[] operator = new int[4];
	public static LinkedList<Integer> result = new LinkedList<Integer>();
	public static int c=-1;
	public static int[] nums;
	public static int N;
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N =sc.nextInt();
		nums = new int[N];
		for(int i=0; i<N; i++) {
			nums[i] = sc.nextInt();
		}
		for(int i=0; i<4; i++) {
			operator[i] = sc.nextInt();
		}
		
		for(int i=0; i<4; i++) {
			bfs(i,1,nums[0]);			
		}
		//bfs(0,0,0);
		Collections.sort(result);
		//System.out.println(result.toString());
		System.out.println(result.getLast());
		System.out.println(result.getFirst());
		
	}//main
	
	public static void bfs(int op, int n, int total) {
		if(n==N) {
			result.add(total);
			return;
		}
		
		if(operator[op]==0) {return;}
		switch(op) {
		case 0:
			total = total+nums[n];
			//operator[0]--;
			break;
		case 1:
			total = total-nums[n];
			//operator[1]--;
			break;
		case 2:
			total = total*nums[n];
			//operator[2]--;
			break;
		case 3:
			total = total/nums[n];
			//operator[3]--;
			break;
		}
		
		for(int i=0; i<4; i++) {
			operator[op]--;
			bfs(i,n+1,total);	
			operator[op]++;
		}
	}

}
