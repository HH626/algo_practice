package aug3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 14889 스타트와 링크
 *
 */
public class StartAndLink {

	static int N;
	static int[][] map;
	static int MIN = Integer.MAX_VALUE;
	static boolean[] list;
	public static void main(String[] args) throws NumberFormatException, IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		MIN = Integer.MAX_VALUE;
		
		//input
		N = Integer.parseInt(br.readLine());
		map = new int[N+1][N+1];
		list = new boolean[N+1];
		
		String line;
		for(int i=1; i<=N; i++) {
			line = br.readLine();
			tk = new StringTokenizer(line);
			for(int j=1; j<=N; j++) {	
				map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		
		//
		bfs(0,0);
		
		System.out.println(MIN);
		
	}//main
	
	public static void bfs(int num, int count) {
		if(count == N/2) { //한팀 다 뽑았으면
			int[] starts = new int[N/2+1];
			int[] links = new int[N/2+1];
			int s=0, l=0;
			for(int i=1; i<=N; i++) {
				if(list[i]) {
					starts[++s]=i;
				}else {
					links[++l]=i;
				}
			}
			
			int sub = Math.abs(ability(starts)-ability(links)); //팀별 능력치 차이
			if(MIN>sub) {
				MIN = sub;
			}
					
		}
		
		for(int i=num+1; i<=N; i++) {
			list[i] = true;
			bfs(i,count+1);
			list[i] = false;
		}
	}
	
	public static int ability(int[] arr) {
		int sum=0;
		for(int i=1; i<=N/2; i++) {
			for(int j=1; j<=N/2; j++) {
				sum += map[arr[i]][arr[j]]; 
			}
		}
		
		return sum;
	}

}
