import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
/**
 * 백준 16987	계란으로 계란치기
 */
public class Main_16987_계란으로계란치기 {
	
	static int N, MAX;
	static int[] arrS, arrW;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		MAX = -1;
		N = Integer.parseInt(br.readLine());
		arrS = new int[N];
		arrW = new int[N];
		for(int i=0; i<N; i++) {
			tk = new StringTokenizer(br.readLine());
			arrS[i] = Integer.parseInt(tk.nextToken());
			arrW[i] = Integer.parseInt(tk.nextToken());
		}
		
		baam(0, 0);

		System.out.println(MAX);
		
	}//main
	private static void baam(int cur, int egg) {
		if(cur==N) {
			if(egg>MAX) {
				MAX = egg;
			}
			return;
		}
		
		if(arrS[cur]<=0 ) {//손에든게 깨져있음
			baam(cur+1, egg);
		}else if(egg==N-1){	// 나빼고 다 깨져있음
			baam(cur+1, egg);	
		}else {
			for(int i=0; i<N; i++) {
				if(arrS[i]<=0 || i==cur) continue;	//지금보는게 깨져있거나 든 계란 차례면 스킵
				//계란깨기
				arrS[i] = arrS[i]-arrW[cur];
				arrS[cur] =  arrS[cur]-arrW[i];
				
				if(arrS[i]<=0 || arrS[cur]<=0) {//깨지면
					if(arrS[i] <=0 && arrS[cur]<=0) {
						baam(cur+1, egg+2);
					}else if(arrS[i] <=0) {
						baam(cur+1, egg+1);
					}else if(arrS[cur]<=0) {
						baam(cur+1, egg+1);
					}
				}else {//안깨지면
					baam(cur+1, egg);
				}
				
				arrS[i] = arrS[i]+arrW[cur];
				arrS[cur] = arrS[cur]+arrW[i];
			}//for
			
					
		}
		
	}//baam
}
