import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 정올 2577 회전초밥
 *
 */
public class Main_2577_회전초밥 {

	static int N, D, K, C;
	static int[] dishes, sushi;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());	//접시수
		D = Integer.parseInt(tk.nextToken());	//초밥 가짓수
		K = Integer.parseInt(tk.nextToken());	//연속접시
		C = Integer.parseInt(tk.nextToken());	//쿠폰 스시
		
		dishes = new int[N];		//접시
		sushi = new int[D+1];		//스시종류
		
		int s;
		for(int i=0; i<N; i++) {
			s = Integer.parseInt(br.readLine());
			dishes[i] = s;
		}
		
		///
		
		int cnt=0, MAX=-1;
		boolean flag = true;
		boolean coupon=false;
		for(int j=0; j<K; j++) {
			System.out.println("dishes[j] "+dishes[j]);
			sushi[dishes[j]]++;
			cnt++;
			if(sushi[dishes[j]] > 1) {	//중복되면
				if(MAX<cnt-1) {
					flag = false;
					MAX = cnt-1;
					cnt=1;
				}
			}
		}
		
		System.out.println(MAX);
		
		for(int i=K; i<N; i++) {
			sushi[dishes[i]]++;
			sushi[dishes[i-K]]--;
			flag = true;
			cnt=0;
			for(int j=i-K+1; j<=i; j++) {
				cnt++;
				if(dishes[j] == C) { //쿠폰이 포항되어있으면
					coupon = true;
				}
				if(sushi[dishes[j]] > 1) {	//중복되면
					coupon = false;
					if(MAX<cnt) {
						flag = false;
						MAX = cnt;
					}
					break;
				}
			}
			
			if(flag) {//중복안됨
				if(coupon) {//쿠폰이 있으면
					MAX = K;
				}else {	//쿠폰미포함
					MAX = K+1;
					break;
				}
			}
		}
		
		//output
		System.out.println(MAX);
		

		
	}//main

}
