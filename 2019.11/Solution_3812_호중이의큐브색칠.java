import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 *	swea 3812 D4 호중이의 큐브색칠
 */
public class Solution_3812_호중이의큐브색칠 {

	static int X,Y,Z,A,B,C,N;
	static long[] arr, xarr,yarr,zarr;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		int T = Integer.parseInt(br.readLine());
		for(int t=1; t<=T; t++) {
			tk = new StringTokenizer(br.readLine());
			X = Integer.parseInt(tk.nextToken());
			Y = Integer.parseInt(tk.nextToken());
			Z = Integer.parseInt(tk.nextToken());
			A = Integer.parseInt(tk.nextToken());
			B = Integer.parseInt(tk.nextToken());
			C = Integer.parseInt(tk.nextToken());
			N = Integer.parseInt(tk.nextToken());
			
			arr = new long[N];
			
			
			//modarr
			xarr = new long[N];		//(X-A)%N 했을때 값0~N-1의 갯수 
			yarr = new long[N];		//(Y-B)%N 했을때 값0~N-1의 갯수 
			zarr = new long[N];		//(Z-C)%N 했을때 값0~N-1의 갯수 
			
			makeModArr(X-A-1, xarr);
			makeModArr(Y-B-1, yarr);
			makeModArr(Z-C-1, zarr);

			makeModArr(A, xarr);
			xarr[0]--;
			makeModArr(B, yarr);
			yarr[0]--;
			makeModArr(C, zarr);
			zarr[0]--;
			
			color();
			
			StringBuilder sb = new StringBuilder();
			sb.append("#").append(t);
			for (long n : arr) {
				sb.append(" ").append(n);
			}
			System.out.println(sb);
			
		}//tc
	}
	private static void makeModArr(int num, long[] a) {
		int quotient = num/N;
		int remainder = num%N;
		for(int i=0; i<=remainder; i++) {
			a[i] += quotient+1;
		}
		for(int i=remainder+1; i<N; i++) {
			a[i] += quotient;
		}
	}
	private static void color() {
		
		int sum;
		long num;
//		for(int modx=0; modx<N; modx++) {
//			for(int mody=0; mody<N; mody++) {
//				for(int modz=0; modz<N; modz++) {
//					sum = modx + mody + modz;
//					num = xarr[modx] * yarr[mody] * zarr[modz];
//					arr[sum%N] = arr[sum%N] + num;
//				}
//			}
//		}
		
		long[] aarr = new long[N];	
		for(int modx=0; modx<N; modx++) {
			for(int mody=0; mody<N; mody++) {
				sum = modx + mody;
				num = xarr[modx] * yarr[mody];
				aarr[sum%N] = aarr[sum%N] + num;
			}
		}
		for(int moda=0; moda<N; moda++) {
			for(int modz=0; modz<N; modz++) {
				sum =  moda + modz;
				num = aarr[moda] * zarr[modz];
				arr[sum%N] = arr[sum%N] + num;
			}
		}
		

	}//color

}
