import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * 백준 17070 파이프옮기기
 * dfs
 */
public class Main_17070_파이프옮기기 {

	static int N, map[][], cnt;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		
		cnt=0;
		N = Integer.parseInt(br.readLine());
		map = new int[N+2][N+2];
		for (int[] a : map) {
			Arrays.fill(a, 1);
		}
		visited = new boolean[N+2][N+2];
		
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		
		//
		visited[1][1]=true;
		visited[1][2]=true;
		dfs(1,2,0);
		
		System.out.println(cnt);
		
	}//main
	
	static boolean visited[][];
	static int[] dirR = {0,1,1};	// 옆, 대각,아래
	static int[] dirC = {1,1,0};
	private static void dfs(int r, int c, int state) {
		if(r==N && c==N) {
			cnt++;
			return;
		}

		int nextR, nextC;
		
		switch(state) {
			
		case 0:
		case 1:
			//옆
			nextR = r;
			nextC = c+1;
			if(map[nextR][nextC] == 0) {
				visited[nextR][nextC] = true;
				dfs(nextR, nextC, 0);
				visited[nextR][nextC] = false;
				
			}
		case 2:
			//대각
			boolean flag = true;
			for(int d=0; d<3; d++) {
				nextR = r+dirR[d];
				nextC = c+dirC[d];
				if(map[nextR][nextC] != 0) {
					flag = false;
				}
			}
			if(flag) {
				nextR = r+1;
				nextC = c+1;
				if(map[nextR][nextC] == 0) {
					visited[nextR][nextC] = true;
					dfs(nextR, nextC, 1);
					visited[nextR][nextC] = false;
					
				}
			}
			if(state == 0) break;
		case 9:
			//아래
			nextR = r+1;
			nextC = c;
			if(map[nextR][nextC] == 0) {
				visited[nextR][nextC] = true;
				dfs(nextR, nextC, 2);
				visited[nextR][nextC] = false;
				
			}
			break;
		}
		

		
	}//dfs

}
