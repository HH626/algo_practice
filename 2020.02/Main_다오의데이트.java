import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 백준 18188 다오의 데이트
 * BFS
 */
public class Main_다오의데이트 {
	static class Point{
		int r, c;
		int idx;
		String path;
		Point(int r, int c, int idx, String path){
			this.r = r;
			this.c = c;
			this.idx = idx;
			this.path = path;
		}
		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + ", idx=" + idx + ", path=" + path + "]";
		}
		
	}
	static int R, C, N;
	static char map[][];
	static String marid[][];
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());		
		map = new char[R+2][C+2];
		
		String line;
		int startR =0, startC=0;
		for(int i=0; i<=R+1; i++) {
			Arrays.fill(map[i], '@');
			if(i==0 || i==R+1) continue;
			line = br.readLine();
			for(int j=1; j<=C; j++) {
				map[i][j] = line.charAt(j-1);
				if(map[i][j]=='D') {
					startR = i;
					startC = j;
					map[i][j] ='.';
				}
			}
		}
	
		N = Integer.parseInt(br.readLine());
		marid = new String[N+1][2]; //마리드의 명령 저장
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			marid[i][0] = tk.nextToken();
			marid[i][1] = tk.nextToken();
		}
		
		
		///////
		String answer = move(startR, startC);
		
		
		//output
		if(answer==null) {
			System.out.println("NO");
		}else {
			System.out.println("YES");
			System.out.println(answer);
		}
	
	
	}//main
	
	static int[] dR= {-1,0,1,0};
	static int[] dC= {0,1,0,-1};
	public static String move(int r, int c) {
		Queue<Point> que = new LinkedList<Point>();
		Point p;
		int nr, nc, d, idx;
		String path;
		
		que.offer(new Point(r,c,1,""));
		
		while(!que.isEmpty()) {
			p = que.poll();
			r = p.r;
			c = p.c;
			idx = p.idx;
			path = p.path;
			for(int i=0;i<2; i++) {
				d = transToNum(marid[idx][i]); //마리드 명령
				nr = r +dR[d];
				nc = c +dC[d];
				if(map[nr][nc]=='Z') { //디즈니 만남
					return path+marid[idx][i];
				}
				if(map[nr][nc]=='.' && idx<N) { //빈칸이고 다오가 움직일수 있는 횟수를 넘지 않음
					que.offer(new Point(nr,nc,idx+1,path+marid[idx][i]+""));
				}
			}//for
			
		}//while
		
		return null;
	}//move
	
	
	//**마리드 명령을 숫자로 변경하는 메서드*/
	public static int transToNum(String str) { 
		switch(str) {
		case "W":
			return 0;
		case "D":
			return 1; 
		case "S":
			return 2;
		case "A":
			return 3;
		}
		return -1;
	}

}
