import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 백준 2174 로봇시뮬레이션
 * 
 * x,y좌표를 r,c좌표로 바꿈
 *    N            S  
 *  W   E   =>   W   E
 *    S            N
 * 시계방향(R) => 반시계방향(L)       
 */
public class Main_로봇시뮬레이션 {
	static class Point{
		int r, c;
		int dir;
		Point(int r, int c, int dir){
			this.r = r;
			this.c = c;
			this.dir = dir;
		}
	}
	static int R, C, N, M, answer;
	static int map[][];
	static Point[] robot;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		tk = new StringTokenizer(br.readLine());
		C = Integer.parseInt(tk.nextToken());
		R = Integer.parseInt(tk.nextToken());
		map = new int[R+2][C+2];
		for(int i=0; i<R+2; i++) {
			for(int j=0; j<C+2; j++) {
				if(j==0 || j==C+1 || i==0 || i==R+1) map[i][j] = -1;
			}
		}
		
		tk = new StringTokenizer(br.readLine());
		N = Integer.parseInt(tk.nextToken());	//로봇수
		robot = new Point[N+1];
		M = Integer.parseInt(tk.nextToken());	//명령수
		for(int i=1; i<=N; i++) {
			tk = new StringTokenizer(br.readLine());
			int c = Integer.parseInt(tk.nextToken());
			int r = Integer.parseInt(tk.nextToken());
			int dir = StrToNum(tk.nextToken());
			robot[i] = new Point(r,c,dir);	//로봇 번호에따라 어느 위치에 있는지 저장
			map[r][c] = i;					//map에 어느 로봇이 위치해 있는지 표시
		}
		
		/////
		
		int no=0;
		for(int i=0; i<M; i++) {//명령하나씩 수행
			tk = new StringTokenizer(br.readLine());
			no = Integer.parseInt(tk.nextToken());
			answer = move(no,tk.nextToken(),Integer.parseInt(tk.nextToken()));	//로봇번호, 명령, 명령반복횟수
			if(answer!=0) break;
		}
		
		//output
		if(answer>0) {//다른 로봇과 부딪
			System.out.printf("Robot %d crashes into robot %d",no,answer);
		}else if(answer<0) {//벽에 부딪
			System.out.printf("Robot %d crashes into the wall",no);
		}else {//잘감
			System.out.println("OK");
		}
		
	}//main
	static int[] dR= {-1,0,1,0};
	static int[] dC= {0,1,0,-1};
	private static int move(int no, String m, int irr) {
		int r, c, d, nr, nc;
		
		r = robot[no].r;
		c = robot[no].c;
		d = robot[no].dir;	
		
		switch(m) {
		case "F":	//앞으로 한칸
			map[r][c] = 0;
			for(int i=0; i<irr; i++) {
				nr = r+dR[d];
				nc = c+dC[d];
				if(map[nr][nc]!=0) {
					return map[nr][nc];
				}
				r = nr;
				c = nc;
			}
			robot[no].r = r;
			robot[no].c = c;
			map[r][c] = no;
			break;
		case "R":	//왼쪽으로 회전(r,c좌표 기준)
			robot[no].dir = (100+d-irr)%4;
			break;
		case "L":	//오른쪽으로 회전(r,c좌표 기준)
			robot[no].dir = (d+irr)%4;
			break;
		}//switch
		
		return 0;

	}
	private static int StrToNum(String s) {
		// xy좌표를 rc로 바꿈 -> N,S이 바뀜
		switch(s) {
		case "S":
			return 0;
		case "E":
			return 1;
		case "N":
			return 2;
		case "W":
			return 3;		
		}
		return -1;
	}

}
