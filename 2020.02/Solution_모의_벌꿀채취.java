import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * swea 2115 모의 벌꿀채취
 * 완전탐색
 * 	1.부분집합
 * 	2.조합
 */
public class Solution_모의_벌꿀채취 {
	static int T, N, M, C;
	static int[][] map;
	static int[][] honey;
	static int max, answer;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		T = Integer.parseInt(br.readLine());
		for(int t=1; t<=T; t++) {
			tk = new StringTokenizer(br.readLine());
			N = Integer.parseInt(tk.nextToken());	
			M = Integer.parseInt(tk.nextToken());	//연속된 벌통갯수
			C = Integer.parseInt(tk.nextToken());	//벌꿀 채취 최대
			
			map = new int[N][N];
			honey = new int[N][N];
			answer = 0;
			max = 0;
			
			for(int i=0; i<N; i++) {
				tk = new StringTokenizer(br.readLine());
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(tk.nextToken());
				}
			}
			
			
			/////////
			
			//1.벌꿀 값 최대 구하기(부분집합)
			maxProfit();
			//2.일꾼 2명의 벌통 자리 구하기(조합)
			comb();
			
			
						
			//output
			System.out.println("#"+t+" "+(answer));
			
			
		}//tc
	}//main
	private static void maxProfit() {
		for(int i=0; i<N; i++) {
			for(int j=0; j<N-M+1; j++) {
				max = 0;
				makeSubset(i,j,0,0,0);
				honey[i][j] = max;
			}
		}
	}//maxProfit
	
	private static void makeSubset(int i, int j, int cnt, int sum, int profit) {
		
		if(sum>C) return;		
		if(cnt==M) {
			if(profit>max) {
				max = profit;
			}
			return;
		}
		
		//선택
		makeSubset(i,j+1,cnt+1,sum+map[i][j],profit+(map[i][j]*map[i][j]));
		//비선택
		makeSubset(i,j+1,cnt+1,sum, profit);
	}//makeSubset
	
	private static void comb() {
		for(int i=0; i<N; i++) {
			for(int j=0; j<N-M+1; j++) {
				//1.같은 라인에 2개
				if(j+M<N-M+1) {
					for(int j2 = j+M ; j2<N-M+1; j2++) {
						answer = Math.max(answer, honey[i][j]+honey[i][j2]);
					}
				}
				
				//2. 다른 라인에
				if(i<N-1) {
					for(int i2=i+1; i2<N; i2++) {
						for(int j2=0; j2<N-M+1; j2++) {
							answer = Math.max(answer, honey[i][j]+honey[i2][j2]);
						}
					}
				}
			}//for
		}
		
	}//comb

}
