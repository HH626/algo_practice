import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * swea 5650 모의 핀볼게임
 */
public class Solution_모의_핀볼게임 {

	static int T, N, answer;
	static int[][] map;
	static List<Integer>[] hole;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk;
		T = Integer.parseInt(br.readLine());
		for(int t=1; t<=T; t++) {
			//init
			answer =0;
			hole = new LinkedList[11];
			for(int i=6; i<=10; i++) {
				hole[i] = new LinkedList<Integer>();
			}
			//input
			N = Integer.parseInt(br.readLine());
			map = new int[N+2][N+2];
			for(int i=0; i<N+2; i++) {
				Arrays.fill(map[i], 5);
				if(i==0 || i==N+1) continue;
				tk = new StringTokenizer(br.readLine());
				for(int j=1; j<=N; j++) {
					map[i][j] = Integer.parseInt(tk.nextToken());
					if(map[i][j]>=6 && map[i][j]<=10) {
						hole[map[i][j]].add(i);
						hole[map[i][j]].add(j);
					}
				}
			}
			
			//////
			int point;
			//공 시작점 하나씩 놓아보기
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					if(map[i][j] == 0) {
						map[i][j] = 100;
						point = go(i, j);//공 굴리기
						if(point>answer) {
							answer = point;
						}
						map[i][j] =0;
					}
				}
			}
			
			
			////
			System.out.println("#"+t+" "+answer);
			
		}//tc
	}//main
	
	public static int[] dR = {0,1,0,-1};
	public static int[] dC = {1,0,-1,0};
	private static int go(int i, int j) {
		int r, c, nr, nc, d;
		int max = 0;
		
		//4방향으로 굴리기
		for(int dir=0; dir<4; dir++) {
			int cnt=0;
			r = i;
			c = j;
			d = dir;
			
			while(true) {
				nr = r+dR[d];
				nc = c+dC[d];
				//공이 시작점에 오거나 블랙홀에 빠진 경우
				if(map[nr][nc]==100 || map[nr][nc]==-1) {
					break;
				}
				
				
				int cur = map[nr][nc];
				if(map[nr][nc]==0) { //1.아무것도 없는 경우
					r = nr;
					c = nc;
				}else if(cur>=1 && cur<=4) { //2. 삼각형 블록 있는 경우
					cnt++;
					r = nr;
					c = nc;
					if(d == cur%4) {
						d = (cur-1);
					}else if(d == (cur+1)%4) {
						d = (d+1)%4;
					}else {
						d = (d+2)%4;
					} 
					
				}else if(cur == 5) {	//3. 네모블록 있는 경우, 벽인 경우
					cnt++;
					r = nr;
					c = nc;
					d = (d+2)%4;
				}else { //	4.웜홀이 있는 경우
					if(hole[cur].get(0)==nr) {
						r = hole[cur].get(2);
					}else if(hole[cur].get(2)==nr) {
						r = hole[cur].get(0);
					}
					
					if(hole[cur].get(1)==nc) {
						c = hole[cur].get(3);
					}else if(hole[cur].get(3)==nc) {
						c = hole[cur].get(1);
					}
				}//
				
			}//while
			
			if(cnt>max) { //4방향으로 굴린것 중 가장 큰 포인트 리턴
				max = cnt;
			}
		}//for
		return max;
	}//go

}
