import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 백준 8911 거북이
 */
public class Main_거북이 {

	static int T, answer;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		T = Integer.parseInt(br.readLine());
		for(int t=1; t<=T; t++) {
			String line = br.readLine();
			answer = move(line);
			//output
			System.out.println(answer);
		}//tc
	}//main
	
	public static int[] dY = {1,0,-1,0};
	public static int[] dX = {0,1,0,-1};
	public static int move(String str) {
		int x=0, y=0;
		int maxX=0, minX=0, maxY=0, minY=0;
		int dir=0;
		char c;
		
		for(int i=0, len = str.length(); i<len; i++) {
			c = str.charAt(i);
			switch(c) {
			case 'F':	//앞으로 한칸
				y = y+dY[dir];
				x = x+dX[dir];
				maxX = Math.max(maxX, x);
				maxY = Math.max(maxY, y);
				minX = Math.min(minX, x);
				minY = Math.min(minY, y);
				break;
			case 'B':	//뒤로 한칸
				y = y-dY[dir];
				x = x-dX[dir];
				maxX = Math.max(maxX, x);
				maxY = Math.max(maxY, y);
				minX = Math.min(minX, x);
				minY = Math.min(minY, y);
				break;
			case 'L':	//왼쪽 회전
				dir = (dir+4-1)%4;
				break;
			case 'R':	//오른쪽 회전
				dir = (dir+1)%4;
				break;
			}//switch
		}//for
		
		return (maxY-minY)*(maxX-minX);
	}//move

}
